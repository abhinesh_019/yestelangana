import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment';

@Injectable()


export class GeneraltravelsService {

  constructor(private http: HttpClient) { }

  locationspost(data) {
    return   this.http.post(environment.apiUrl +"/travelsLocations",data);

 
    }

    locationsget(){
       return this.http.get(environment.apiUrl +"/travelsLocations");
                   }
                   
                   areaapi(id){
    
                    return this.http.get(environment.apiUrl +"/travelsAreas/"+id);
                  }

                  areapost(id,data){
                    return   this.http.post(environment.apiUrl +"/travelsAreas/"+id,data);
                   }
 }

import { TestBed, inject } from '@angular/core/testing';

import { GeneralindoorService } from './generalindoor.service';

describe('GeneralindoorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeneralindoorService]
    });
  });

  it('should be created', inject([GeneralindoorService], (service: GeneralindoorService) => {
    expect(service).toBeTruthy();
  }));
});

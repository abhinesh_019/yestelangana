import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralanimalsComponent } from './generalanimals.component';

describe('GeneralanimalsComponent', () => {
  let component: GeneralanimalsComponent;
  let fixture: ComponentFixture<GeneralanimalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralanimalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralanimalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

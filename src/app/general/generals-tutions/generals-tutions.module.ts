import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
 import { GeneralsTutionsComponent } from './generals-tutions.component';
 import{GeneralTutionsService}from'./general-tutions.service'


const routes:Routes=[{path:'generalsTutions',component:GeneralsTutionsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
     GeneralsTutionsComponent
  ],
  providers: [ GeneralTutionsService],
})
 
export class GeneralsTutionsModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneraloutdoorComponent } from './generaloutdoor.component';

describe('GeneraloutdoorComponent', () => {
  let component: GeneraloutdoorComponent;
  let fixture: ComponentFixture<GeneraloutdoorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneraloutdoorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneraloutdoorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

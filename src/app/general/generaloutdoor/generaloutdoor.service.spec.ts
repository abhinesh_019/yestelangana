import { TestBed, inject } from '@angular/core/testing';

import { GeneraloutdoorService } from './generaloutdoor.service';

describe('GeneraloutdoorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeneraloutdoorService]
    });
  });

  it('should be created', inject([GeneraloutdoorService], (service: GeneraloutdoorService) => {
    expect(service).toBeTruthy();
  }));
});

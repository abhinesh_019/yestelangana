import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralrestaurantsComponent } from './generalrestaurants.component';

describe('GeneralrestaurantsComponent', () => {
  let component: GeneralrestaurantsComponent;
  let fixture: ComponentFixture<GeneralrestaurantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralrestaurantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralrestaurantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { TestBed, inject } from '@angular/core/testing';

import { GeneralmarriagebureauService } from './generalmarriagebureau.service';

describe('GeneralmarriagebureauService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeneralmarriagebureauService]
    });
  });

  it('should be created', inject([GeneralmarriagebureauService], (service: GeneralmarriagebureauService) => {
    expect(service).toBeTruthy();
  }));
});

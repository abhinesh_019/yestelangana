import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
 import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { GeneralComponent } from './general.component';
import { GeneralfurnituresModule } from '../general-furnitures/generalfurnitures.module';
import { FurnituresService } from '../general-furnitures/generalfurnitures.service';
import { GeneralbabycaresModule } from '../generalbabycares/generalbabycares.module';
import { GeneralsTutionsModule } from '../generals-tutions/generals-tutions.module';
import { BeautyParlourModule } from '../beauty-parlour/beauty-parlour.module';
import { HostelsModule } from '../general-hostels/hostels.module';
import { GeneraltravelsModule } from '../generaltravels/generaltravels.module';
import { GeneralGymModule } from '../general-gym/general-gym.module';
import { GeneralanimalsModule } from '../generalanimals/generalanimals.module';
import { GeneralplantsModule } from '../generalplants/generalplants.module';
import { GeneralfunctionhallModule } from '../general-functions-hall/generalfunctionhall.module';
import { GeneralmusicModule } from '../generalmusic/generalmusic.module';
import { GeneralrealestateModule } from '../generalrealestate/generalrealestate.module';
import { GeneraldanceModule } from '../generaldance/generaldance.module';
import { GeneralrestaurantsModule } from '../generalrestaurants/generalrestaurants.module';
import { GeneralmarriagebureauModule } from '../generalmarriagebureau/generalmarriagebureau.module';
import { GeneralswmmingpoolsModule } from '../generalswimmingpools/generalswmmingpools.module';
import { GeneralfightsModule } from '../generalfights/generalfights.module';
import { GeneralindoorModule } from '../generalindoor/generalindoor.module';
import { GeneraloutdoorModule } from '../generaloutdoor/generaloutdoor.module';
import { GeneraldrivingschoolModule } from '../generaldrivingschool/generaldrivingschool.module';
import { GeneralfireworksModule } from '../generalfireworks/generalfireworks.module';
import { GeneralpackersMoversModule } from '../generalpackers-movers/generalpackers-movers.module';
import { GeneralProfessionalInstituionsModule } from '../general-professional-institutions/general-professional-instituions.module';
import { MainmenuModule } from '../mainmenus/mainmenu.module';
 


const routes: Routes = [
  {
    path: '', component: GeneralComponent, children:
      [
        { path: 'generalfurnitures', loadChildren: 'app/general/general-furnitures/generalfurnitures.module#GeneralfurnituresModule'},
        { path: 'generalbabycares', loadChildren: 'app/general/generalbabycares/generalbabycares.module#GeneralbabycaresModule'},
        { path: 'generalsTutions', loadChildren: 'app/general/generals-tutions/generals-tutions.module#GeneralsTutionsModule'},
        { path: 'generalsbeautyparlour',loadChildren: 'app/general/beauty-parlour/beauty-parlour.module#BeautyParlourModule'},
        { path: 'generalshostels',loadChildren: 'app/general/general-hostels/hostels.module#HostelsModule'},
        { path: 'generalsgym',loadChildren: 'app/general/general-gym/general-gym.module#GeneralGymModule'},
        { path: 'generalstravels',loadChildren: 'app/general/generaltravels/generaltravels.module#GeneraltravelsModule'},
        { path: 'generalsAnimals',loadChildren: 'app/general/generalanimals/generalanimals.module#GeneralanimalsModule'},
        { path: 'generalsPlants',loadChildren: 'app/general/generalplants/generalplants.module#GeneralplantsModule'},
        { path: 'generalsFunctionHall',loadChildren: 'app/general/general-functions-hall/generalfunctionhall.module#GeneralfunctionhallModule'},
        { path: 'generalsmusic',loadChildren: 'app/general/generalmusic/generalmusic.module#GeneralmusicModule'},
        { path: 'generalsrealestate',loadChildren: 'app/general/generalrealestate/generalrealestate.module#GeneralrealestateModule'},
        { path: 'generaldances',loadChildren: 'app/general/generaldance/generaldance.module#GeneraldanceModule'},
        { path: 'generalsrestaurants',loadChildren: 'app/general/generalrestaurants/generalrestaurants.module#GeneralrestaurantsModule'},
        { path: 'generalmarriageBureau',loadChildren: 'app/general/generalmarriagebureau/generalmarriagebureau.module#GeneralmarriagebureauModule'},
        { path: 'generalswimmings',loadChildren: 'app/general/generalswimmingpools/generalswmmingpools.module#GeneralswmmingpoolsModule'},
        { path: 'generalsfights',loadChildren: 'app/general/generalfights/generalfights.module#GeneralfightsModule'},
        { path: 'generalindoorgames',loadChildren: 'app/general/generalindoor/generalindoor.module#GeneralindoorModule'},
        { path: 'generaloutdoorgames',loadChildren: 'app/general/generaloutdoor/generaloutdoor.module#GeneraloutdoorModule'},
        { path: 'generalDrivingSchools',loadChildren: 'app/general/generaldrivingschool/generaldrivingschool.module#GeneraldrivingschoolModule'},
        { path: 'generalFireWorks',loadChildren: 'app/general/generalfireworks/generalfireworks.module#GeneralfireworksModule'},
        { path: 'generalPackersMovers',loadChildren: 'app/general/generalpackers-movers/generalpackers-movers.module#GeneralpackersMoversModule'},
        { path: 'generalProfessionalInstituions',loadChildren: 'app/general/general-professional-institutions/general-professional-instituions.module#GeneralProfessionalInstituionsModule'},
        { path: 'mainmenu',loadChildren: 'app/general/mainmenus/mainmenu.module#MainmenuModule'},

     ]
  }]
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    SharedModule,
    GeneralfurnituresModule,
    GeneralbabycaresModule,
    GeneralsTutionsModule,
    BeautyParlourModule,
    HostelsModule,
    GeneraltravelsModule,
    GeneralGymModule,
    GeneralanimalsModule,
    GeneralplantsModule,
    GeneralfunctionhallModule,
    GeneralmusicModule,
    GeneralrealestateModule,
    GeneraldanceModule,
    GeneralrestaurantsModule,
    GeneralmarriagebureauModule,
    GeneralswmmingpoolsModule,
    GeneralfightsModule,
    GeneralindoorModule,
    GeneraloutdoorModule,
    GeneraldrivingschoolModule,
    GeneralfireworksModule,
    GeneralpackersMoversModule,
    GeneralProfessionalInstituionsModule,
    MainmenuModule
   ],
  declarations: [
    GeneralComponent
  ],
  providers: [FurnituresService],
})
 
export class GeneralModule { }

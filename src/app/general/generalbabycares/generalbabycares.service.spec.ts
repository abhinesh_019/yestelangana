import { TestBed, inject } from '@angular/core/testing';

import { GeneralbabycaresService } from './generalbabycares.service';

describe('GeneralbabycaresService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeneralbabycaresService]
    });
  });

  it('should be created', inject([GeneralbabycaresService], (service: GeneralbabycaresService) => {
    expect(service).toBeTruthy();
  }));
});

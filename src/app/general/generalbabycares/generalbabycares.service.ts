import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment';


@Injectable()
export class GeneralbabycaresService {

  constructor(private http: HttpClient) { }

  locationspost(data){
    return   this.http.post(environment.apiUrl +"/babycarelocations",data);
   }
   locationapi( ){
    return   this.http.get(environment.apiUrl +"/babycarelocations");
   }
   areapost(id,data){
    return   this.http.post(environment.apiUrl +"/babycareArea/"+id,data);

   }

   areaapi(id){
    return   this.http.get(environment.apiUrl +"/babycareArea/"+id);
   }

   areapostusers(id,data){
    return   this.http.post(environment.apiUrl +"/babycares/"+id,data);
   }
   
 

  babycaresAddsOnea(id){

    return this.http.get(environment.apiUrl +"/babycaresAddsOneGeta/"+id);
  }
  babycaresAddsTwoa(id){
    
    return this.http.get(environment.apiUrl +"/babycaresAddsTwoGeta/"+id);
  }
  babycaresAddsThreea(id){
    
    return this.http.get(environment.apiUrl +"/babycaresAddsThreeGeta/"+id);
  }
  babycaresAddsFoura(id){
    
    return this.http.get(environment.apiUrl +"/babycaresAddsFourGeta/"+id);
  }
   
  babycaresAddsOnel(id){
    
    return this.http.get(environment.apiUrl +"/babycaresAddsOneLocGet/"+id);
  }
  babycaresAddsTwol(id){
    
    return this.http.get(environment.apiUrl +"/babycaresAddsTwoLocGet/"+id);
  }
  babycaresAddsThreel(id){
    
    return this.http.get(environment.apiUrl +"/babycaresAddsThreeGetLoc/"+id);
  }
  babycaresAddsFourl(id){
    
    return this.http.get(environment.apiUrl +"/babycaresAddsFourLocGet/"+id);
  }

  babycaresAddsFivel(id){
    
    return this.http.get(environment.apiUrl +"/babycaresAddsFiveLocGet/"+id);
  }
   
  postAddsOne(id,data){
    return   this.http.post(environment.apiUrl +"/babycaresAddsOnePtsa/"+id,data);
   }
   postAddsTwo(id,data){
    return   this.http.post(environment.apiUrl +"/babycaresAddsTwoPtsa/"+id,data);
   }
   postAddsThree(id,data){
    return   this.http.post(environment.apiUrl +"/babycaresAddsThreePtsa/"+id,data);
   }
   postAddsFour(id,data){
    return   this.http.post(environment.apiUrl +"/babycaresAddsFourPtsa/"+id,data);
   }
   


  postAddsOnepl(id,data){
    return   this.http.post(environment.apiUrl +"/babycaresAddsOneLocPts/"+id,data);
   }
   postAddsTwopl(id,data){
    return   this.http.post(environment.apiUrl +"/babycaresAddsTwoLocPts/"+id,data);
   }
   postAddsThreepl(id,data){
    return   this.http.post(environment.apiUrl +"/babycaresAddsThreePtsLoc/"+id,data);
   }
   postAddsFourpl(id,data){
    return   this.http.post(environment.apiUrl +"/babycaresAddsFourLocPts/"+id,data);
   }
   postAddsFivepl(id,data){
    return   this.http.post(environment.apiUrl +"/babycaresAddsFiveLocPts/"+id,data);
   }
   


   babycaresAddsOneaDelete(id){

    return this.http.delete(environment.apiUrl +"/babycaresAddsOneDeletea/"+id);
  }
 
  babycaresAddsTwoaDelete(id){

    return this.http.delete(environment.apiUrl +"/babycaresAddsTwoDeletea/"+id);
  }
  babycaresAddsThreeaDelete(id){

    return this.http.delete(environment.apiUrl +"/babycaresAddsThreeDeletea/"+id);
  }
  babycaresAddsFouraDelete(id){

    return this.http.delete(environment.apiUrl +"/babycaresAddsFourDeletea/"+id);
  }


   babycaresAddsOneLocDelete(id){

    return this.http.delete(environment.apiUrl +"/babycaresAddsOneLocDelete/"+id);
  }

  babycaresAddsTwoLocDelete(id){

    return this.http.delete(environment.apiUrl +"/babycaresAddsTwoLocDelete/"+id);
  }
  babycaresAddsThreeLocDelete(id){

    return this.http.delete(environment.apiUrl +"/babycaresAddsThreeDeleteLoc/"+id);
  }
  babycaresAddsFourLocDelete(id){

    return this.http.delete(environment.apiUrl +"/babycaresAddsFourLocDelete/"+id);
  }
  babycaresAddsFiveLocDelete(id){

    return this.http.delete(environment.apiUrl +"/babycaresAddsFiveLocDelete/"+id);
  }
   
}


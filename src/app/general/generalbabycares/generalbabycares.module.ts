import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
 import { GeneralbabycaresComponent } from './generalbabycares.component';
import { GeneralbabycaresService } from './generalbabycares.service';



const routes:Routes=[{path:'generalbabycares',component:GeneralbabycaresComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    GeneralbabycaresComponent,
  ],
  providers: [GeneralbabycaresService],
})
 
 
export class GeneralbabycaresModule { }

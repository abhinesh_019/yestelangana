import { Component, OnInit } from '@angular/core';
import { GeneralbabycaresService } from './generalbabycares.service';

@Component({
  selector: 'app-generalbabycares',
  templateUrl: './generalbabycares.component.html',
  styleUrls: ['./generalbabycares.component.css']
})
export class GeneralbabycaresComponent implements OnInit {
public babycareslocations:any
public locationName:any
public its:any
public locationget:any
public areasAll:any
public selectedAreas:any
public locationsget:any
public selectAreaId:any
public selectArea:any

 public imageAddsOne:any
public babycaresAddsOnes:any 
public babycaresAddsFours:any
public imageAddsFour:any
public imageAddsThree:any
public imageAddsTwo:any
public babycaresAddsThrees:any
public babycaresAddsTwos:any


public babycaresAddsFivesLoc:any
public imageAddsFiveLoc:any
public imageAddsFourLoc:any
public babycaresAddsFoursLoc:any
public imageAddsThreeLoc:any
public babycaresAddsThreeLoc:any
public imageAddsTwoLoc:any
public babycaresAddsTwoLoc:any
public imageAddsOneLoc:any
public babycaresAddsOneLoc:any
public babycaresAddsOneLocl:any
  constructor(private babycares:GeneralbabycaresService) { }

  locationsdata={
    "locations":""
  }

  areas={
    "area":""
  }


  bcmain={
  "name":"",
  "centername":"",
  "description":"",
  "infants":"",
  "toddler":"",
  "preschool":"",
  "schoolage":"",
  "descriptionone":"",
    "descriptiontwo" :"",
    "descriptionthree" :"",
 
  "day1":"",
  "day2":"",
  "day3":"",
  "day4":"",
  "day5":"",
  "day6":"",
  "day7":"",
  "overalltimes":"",


  "hno":"",
  "area":"",
  "landmark":"",
  "place":"",
 "city":"",
"distict":"",
 "state":"",
   "pincode":"",



   "officeno":"",
   "mobileno":"",
   "whatsupno":"",
   "emailid":"",
   
       "refone":"",
       "reftwo":"",
       "refthree":"",
  
"images1":"",
"images2":"",
"images3":"",
"images4":"",
"images5":"",
"images6":""

}
areaAddsOne={
  "addsOneLink":"",
  "addsOneImg":"",
}
areaAddsTwo={
  "addsTwoLink":"",
  "addsTwoImg":"",
}
areaAddsThree={
  "addsThreeLink":"",
  "addsThreeImg":"",
}
areaAddsFour={
  "addsFourLink":"",
  "addsFourImg":"",
}
 

areaAddsOneLoc={
  "addsOneLinkLoc":"",
  "addsOneImgLoc":"",
}
areaAddsTwoLoc={
  "addsTwoLinkLoc":"",
  "addsTwoImgLoc":"",
}
areaAddsThreeLoc={
  "addsThreeLinkLoc":"",
  "addsThreeImgLoc":"",
}
areaAddsFourLoc={
  "addsFourLinkLoc":"",
  "addsFourImgLoc":"",
}
areaAddsFiveLoc={
  "addsFiveLinkLoc":"",
  "addsFiveImgLoc":"",
}
 


public formData: any = new FormData();


  ngOnInit() {
    this.getlocations()
   
  }
  postlocations(){
 
    this.babycares.locationspost(this.locationsdata).subscribe((res)=>{
      this.babycareslocations=res
       
    })  
  this.locationsdata.locations=""
  }


    // locations*****************
getlocations(){
  this.babycares.locationapi().subscribe((res)=>{
    this.locationsget=res;
    
    var id =this.locationsget[0];
     this.locations(id)
     
  })}



    locations(get?){
      this.locationName=get.locations
      this.its=get._id
      this.locationget=get.locations 
      this.allAreas()
      this.babycaressareasAddsOneLoc()
      this.babycaressareasAddsTwoLoc()
      this.babycaressareasAddsThreeLoc()
      this.babycaressareasAddsFourLoc()
      this.babycaressareasAddsFiveLoc()
    }

    allAreas(){
     
      this.babycares.areaapi(this.its).subscribe((res)=>{
        this.areasAll=res
        var id =this.areasAll[0];
        
        this.selectedAre(id) 
      })
     
    }

selectedAre(result){
  this.selectAreaId=result._id
  this.selectArea=result.area
 
  this.babycaressareasAddsOnea()
  this.babycaressareasAddsTwoa()
  this.babycaressareasAddsThreea()
  this.babycaressareasAddsFoura()
}


    postarea(){
      this.babycares.areapost(this.its,this.areas).subscribe((res)=>{
         this.areas.area=""
      }) 
    }


 //  ************************main users details *********************

 handleFileInput($event:any,img) {
  let imageFile= File = $event.target.files[0];
  this.formData.append(img, imageFile);
   
}


    
postUsers(){
 
  this.formData.append('name',this.bcmain.name);
  this.formData.append('centername',this.bcmain.centername);
  this.formData.append('description',this.bcmain.description);
  this.formData.append('infants',this.bcmain.infants);
   this.formData.append('toddler',this.bcmain.toddler);
  this.formData.append('preschool',this.bcmain.preschool);
  this.formData.append('schoolage',this.bcmain.schoolage);
  this.formData.append('descriptionone',this.bcmain.descriptionone);
  this.formData.append('descriptiontwo',this.bcmain.descriptiontwo);
  this.formData.append('descriptionthree',this.bcmain.descriptionthree);
  this.formData.append('day1',this.bcmain.day1);
  this.formData.append('day2',this.bcmain.day2);
  this.formData.append('day3',this.bcmain.day3);
  this.formData.append('day4',this.bcmain.day4);
  this.formData.append('day5',this.bcmain.day5);
  this.formData.append('day6',this.bcmain.day6);
  this.formData.append('day7',this.bcmain.day7);
  this.formData.append('overalltimes ',this.bcmain.overalltimes);
  this.formData.append('hno',this.bcmain.hno);
  this.formData.append('area',this.bcmain.area);
  this.formData.append('landmark',this.bcmain.landmark);
  this.formData.append('place',this.bcmain.place);
  this.formData.append('city',this.bcmain.city);
  this.formData.append('distict',this.bcmain.distict);
  this.formData.append('state',this.bcmain.state);
  this.formData.append('pincode',this.bcmain.pincode);
  this.formData.append('officeno',this.bcmain.officeno);
  this.formData.append('mobileno',this.bcmain.mobileno);
  this.formData.append('whatsupno',this.bcmain.whatsupno);
  this.formData.append('emailid',this.bcmain.emailid);
  this.formData.append('refone',this.bcmain.refone);
  this.formData.append('reftwo',this.bcmain.reftwo);
  this.formData.append('refthree',this.bcmain.refthree);

this.bcmain.name=""
this.bcmain.centername=""
this.bcmain.description="",
this.bcmain.infants="",
this.bcmain.toddler="",
this.bcmain.preschool="",
this.bcmain.schoolage="",
this.bcmain.descriptionone="",
this.bcmain.descriptiontwo="",
this.bcmain.descriptionthree="",
 
this.bcmain.day1="",
this.bcmain.day2="",
this.bcmain.day3="",
this.bcmain.day4="",
this.bcmain.day5="",
this.bcmain.day6="",
this.bcmain.day7="",
this.bcmain.overalltimes="",
 
this.bcmain.hno="",
this.bcmain.area="",
this.bcmain.landmark="",
this.bcmain.city="",


this.bcmain.distict="",
this.bcmain.state="",
this.bcmain.pincode="",
this.bcmain.officeno="",
this.bcmain.mobileno="",
this.bcmain.whatsupno="",
this.bcmain.emailid="",
this.bcmain.refone,
this.bcmain.reftwo,
this.bcmain.refthree,

this.bcmain.images1="",
this.bcmain.images2="",
this.bcmain.images3="",
this.bcmain.images4="",
this.bcmain.images5="",
this.bcmain.images6="",

    
      this.babycares.areapostusers(this.selectAreaId,this.formData).subscribe((res)=>{
         this.areas.area=""
      }) 
    


}

// ********************************************************* Adds Areas One ***********************************************************************

handleFileInputUpdates($event:any,images){
  this.imageAddsOne= File = $event.target.files[0];
 }
 

postAddsOne(){
this.formData.append('addsOneImg',this.imageAddsOne);
this.formData.append('addsOneLink',this.areaAddsOne.addsOneLink); 
 
 this.babycares.postAddsOne(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsOne.addsOneLink="",
     this.areaAddsOne.addsOneImg=""
}
babycaressareasAddsOnea(){
  this.babycares.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
     console.log(res);
     
   })
}
babycaressareasAddsOneaDelete(identities){
  let areaIds=identities._id
  this.babycares.babycaresAddsOneaDelete(areaIds).subscribe((res)=>{
    
  })
}

// ********************************************************* Adds Areas Two ***********************************************************************

handleFileInputUpdatesTwo($event:any,images){
  this.imageAddsTwo= File = $event.target.files[0];
 }
 

postAddsTwo(){
this.formData.append('addsTwoImg',this.imageAddsTwo);
this.formData.append('addsTwoLink',this.areaAddsTwo.addsTwoLink); 
 
 this.babycares.postAddsTwo(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsTwo.addsTwoLink="",
     this.areaAddsTwo.addsTwoImg=""
}
babycaressareasAddsTwoa(){
  this.babycares.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
   })
}
babycaressareasAddsTwoaDelete(identities){
  let areaIds=identities._id
  this.babycares.babycaresAddsTwoaDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Three ***********************************************************************

handleFileInputUpdatesThree($event:any,images){
  this.imageAddsThree= File = $event.target.files[0];
 }
 

postAddsThree(){
this.formData.append('addsThreeImg',this.imageAddsThree);
this.formData.append('addsThreeLink',this.areaAddsThree.addsThreeLink); 
 
 this.babycares.postAddsThree(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsThree.addsThreeLink="",
     this.areaAddsThree.addsThreeImg=""
}
babycaressareasAddsThreea(){
  this.babycares.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
   })
}
babycaressareasAddsThreeaDelete(identities){
  let areaIds=identities._id
  this.babycares.babycaresAddsThreeaDelete(areaIds).subscribe((res)=>{  
  })
}


// ********************************************************* Adds Areas Four ***********************************************************************

handleFileInputUpdatesFour($event:any,images){
  this.imageAddsFour= File = $event.target.files[0];
 }
 

postAddsFour(){
this.formData.append('addsFourImg',this.imageAddsFour);
this.formData.append('addsFourLink',this.areaAddsFour.addsFourLink); 
 
 this.babycares.postAddsFour(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsFour.addsFourLink="",
     this.areaAddsFour.addsFourImg=""
}
babycaressareasAddsFoura(){
  this.babycares.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res
   })
}
babycaressareasAddsFouraDelete(identities){
  let areaIds=identities._id
  this.babycares.babycaresAddsFouraDelete(areaIds).subscribe((res)=>{
  })
}

 

 


// ********************************************************* Adds Areas One ***********************************************************************

handleFileInputUpdatesOneLoc($event:any,images){
  this.imageAddsOneLoc= File = $event.target.files[0];
 }
 

postAddsOneLoc(){
this.formData.append('addsOneImgLoc',this.imageAddsOneLoc);
this.formData.append('addsOneLinkLoc',this.areaAddsOneLoc.addsOneLinkLoc); 
 
 this.babycares.postAddsOnepl(this.its,this.formData).subscribe((res)=>{
    
})
     this.areaAddsOneLoc.addsOneImgLoc="",
     this.areaAddsOneLoc.addsOneLinkLoc=""
}
babycaressareasAddsOneLoc(){
  this.babycares.babycaresAddsOnel(this.its).subscribe((res)=>{
     this.babycaresAddsOneLocl=res
   })
}
babycaressareasAddsOneLocDelete(identities){
  let areaIds=identities._id
  this.babycares.babycaresAddsOneLocDelete(areaIds).subscribe((res)=>{
    
  })
}

// ********************************************************* Adds Areas Two ***********************************************************************

handleFileInputUpdatesTwoLoc($event:any,images){
  this.imageAddsTwoLoc= File = $event.target.files[0];
 }
 

postAddsTwoLoc(){
this.formData.append('addsTwoImgLoc',this.imageAddsTwoLoc);
this.formData.append('addsTwoLinkLoc',this.areaAddsTwoLoc.addsTwoLinkLoc); 
 
 this.babycares.postAddsTwopl(this.its,this.formData).subscribe((res)=>{
     })
     this.areaAddsTwoLoc.addsTwoLinkLoc="",
     this.areaAddsTwoLoc.addsTwoImgLoc=""
}
babycaressareasAddsTwoLoc(){
  this.babycares.babycaresAddsTwol(this.its).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
   })
}
babycaressareasAddsTwoLocDelete(identities){
  let areaIds=identities._id
  this.babycares.babycaresAddsTwoLocDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Three Loc***********************************************************************

handleFileInputUpdatesThreeLoc($event:any,images){
  this.imageAddsThreeLoc= File = $event.target.files[0];
 }
 

postAddsThreeLoc(){
this.formData.append('addsThreeImgLoc',this.imageAddsThreeLoc);
this.formData.append('addsThreeLinkLoc',this.areaAddsThreeLoc.addsThreeLinkLoc); 
 
 this.babycares.postAddsThreepl(this.its,this.formData).subscribe((res)=>{
     })
     this.areaAddsThreeLoc.addsThreeImgLoc="",
     this.areaAddsThreeLoc.addsThreeLinkLoc=""
}
babycaressareasAddsThreeLoc(){
  this.babycares.babycaresAddsThreel(this.its).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
   })
}
babycaressareasAddsThreeLocDelete(identities){
  let areaIds=identities._id
  this.babycares.babycaresAddsThreeLocDelete(areaIds).subscribe((res)=>{
  
  })
}


// ********************************************************* Adds Areas Four Loc***********************************************************************

handleFileInputUpdatesFourLoc($event:any,images){
  this.imageAddsFourLoc= File = $event.target.files[0];
 }
 

postAddsFourLoc(){
this.formData.append('addsFourImgLoc',this.imageAddsFourLoc);
this.formData.append('addsFourLinkLoc',this.areaAddsFourLoc.addsFourLinkLoc); 
 
 this.babycares.postAddsFourpl(this.its,this.formData).subscribe((res)=>{
     })
     this.areaAddsFourLoc.addsFourImgLoc="",
     this.areaAddsFourLoc.addsFourLinkLoc=""
}
babycaressareasAddsFourLoc(){
  this.babycares.babycaresAddsFourl(this.its).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
   })
}
babycaressareasAddsFourLocDelete(identities){
  let areaIds=identities._id
  this.babycares.babycaresAddsFourLocDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Five Loc***********************************************************************

handleFileInputUpdatesFiveLoc($event:any,images){
  this.imageAddsFiveLoc= File = $event.target.files[0];
 }
 

postAddsFiveLoc(){
this.formData.append('addsFiveImgLoc',this.imageAddsFiveLoc);
this.formData.append('addsFiveLinkLoc',this.areaAddsFiveLoc.addsFiveLinkLoc); 
 
 this.babycares.postAddsFivepl(this.its,this.formData).subscribe((res)=>{
     })
     this.areaAddsFiveLoc.addsFiveImgLoc="",
     this.areaAddsFiveLoc.addsFiveLinkLoc=""
}
babycaressareasAddsFiveLoc(){
  this.babycares.babycaresAddsFivel(this.its).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
   })
}
babycaressareasAddsFiveLocDelete(identities){
  let areaIds=identities._id
  this.babycares.babycaresAddsFiveLocDelete(areaIds).subscribe((res)=>{
    
  })
}

 
}


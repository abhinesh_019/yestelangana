import { Component, OnInit } from '@angular/core';
import { MainmenuService } from './mainmenu.service';

@Component({
  selector: 'app-mainmenus',
  templateUrl: './mainmenus.component.html',
  styleUrls: ['./mainmenus.component.css']
})
export class MainmenusComponent implements OnInit {
public feedback:any
public add:any

public feedbackc:any
public addc:any
public imageone:any

public imagetwo:any
public imagethree:any
public imagefour:any
public imagefive:any
public imagesix:any
public imageseven:any
public imageeight:any
public imagenine:any
public imageten:any
public imageyhre:any
public addone:any
public addtwo:any
public addthree:any
public addfour:any
public addfive:any
public addsix:any
public addseven:any
public addeight:any
public addnine:any
public addten:any
public asone:any
public astwo:any
public asthree:any
public asfour:any
public asfive:any
public assix:any
public asseven:any
public aseight:any
public asnine:any
public asten:any
aone={
  images1:""
}

atwo={
  images1:""
}
athree={
  images1:""
}
afour={
  images1:""
}
afive={
  images1:""
}
asix={
  images1:""
}
aseven={
  images1:""
}
aeight={
  images1:""
}
anine={
  images1:""
}
aten={
  images1:""
}

  constructor(private swim:MainmenuService) { }

  ngOnInit() {
    this.feedbacks()
    this.adds()
    this.addsoneget()
    this.addstwoget()
    this.addsthreeget()
    this.addsfourget()
    this.addsfiveget()
    this.addssixget()
    this.addssevenget()
    this.addseightget()
    this.addsnineget()
    this.addstenget()
  }
  feedbacks(){
           
    this.swim.feedbacks().subscribe((res)=>{
      this.feedback=res
     })
   
  }
  feedbacksc(){
           
    this.swim.feedbacksc().subscribe((res)=>{
      this.feedbackc=res
     })
   
  }
  adds(){
           
    this.swim.addsget().subscribe((res)=>{
      this.add=res
 
      })
   }

   addsc(){
           
    this.swim.addsgetc().subscribe((res)=>{
      this.addc=res
      })
   }

// **********a one *******
handleFileInputaOne($event:any,images){
  this.imageone= File = $event.target.files[0];
  // this.formData.append(images, imageFile);
}
 
addsOne(){
  let formData: FormData = new FormData();
  formData.append('images1', this.imageone); 
  

   
  this.swim.addOne(formData).subscribe((res)=>{
    
      })
       this.aone.images1=""
 }
 addsoneget(){
           
  this.swim.addsonegets().subscribe((res)=>{
    this.asone=res
     
    })
 }
 addsonedelete(deleteid){
   let deleteidsO=deleteid._id 
   console.log(deleteidsO);
        console.log(deleteid._id);
        
  this.swim.addonedelete(deleteidsO).subscribe((res)=>{
     })
 }
// **********a two *******
handleFileInputatwo($event:any,images){
  this.imagetwo= File = $event.target.files[0];
  // this.formData.append(images, imageFile);
}
 
addstwo(){
  let formData: FormData = new FormData();
  formData.append('images1', this.imagetwo); 
  

   
  this.swim.addtwo(formData).subscribe((res)=>{
    
      })
       this.atwo.images1=""
 }
 addstwoget(){
           
  this.swim.addstwogets().subscribe((res)=>{
    this.astwo=res
    console.log(res);
    
    })
 }
 addstwodelete(deleteid){
  let deleteids=deleteid._id    
  console.log(deleteids);
  
 this.swim.addtwodelete(deleteids).subscribe((res)=>{
    })
}
 // **********a three *******
handleFileInputathree($event:any,images){
  this.imagethree= File = $event.target.files[0];
  // this.formData.append(images, imageFile);
}
 
addsthree(){
  let formData: FormData = new FormData();
  formData.append('images1', this.imagethree); 
  

   
  this.swim.addthree(formData).subscribe((res)=>{
    
      })
       this.athree.images1=""
 }
 addsthreeget(){
           
  this.swim.addsthreegets().subscribe((res)=>{
    this.asthree=res
    })
 }
 addstreedelete(deleteid){
  let deleteids=deleteid._id      
 this.swim.addthreedelete(deleteids).subscribe((res)=>{
    })
}
 // **********a four *******
handleFileInputafour($event:any,images){
  this.imagefour= File = $event.target.files[0];
  // this.formData.append(images, imageFile);
}
 
addsfour(){
  let formData: FormData = new FormData();
  formData.append('images1', this.imagefour); 
  

   
  this.swim.addfour(formData).subscribe((res)=>{
    
      })
       this.afour.images1=""
 }
 addsfourget(){
           
  this.swim.addsfourgets().subscribe((res)=>{
    this.asfour=res
    })
 }
 addsfourdelete(deleteid){
  let deleteids=deleteid._id      
 this.swim.addfourdelete(deleteids).subscribe((res)=>{
    })
}
 // **********a five *******
handleFileInputafive($event:any,images){
  this.imagefive= File = $event.target.files[0];
  // this.formData.append(images, imageFile);
}
 
addsfive(){
  let formData: FormData = new FormData();
  formData.append('images1', this.imagefive); 
  

   
  this.swim.addfive(formData).subscribe((res)=>{
    
      })
       this.afive.images1=""
 }
 addsfiveget(){
           
  this.swim.addsfivegets().subscribe((res)=>{
    this.asfive=res
    })
 }

 addsfivedelete(deleteid){
  let deleteids=deleteid._id      
 this.swim.addfivedelete(deleteids).subscribe((res)=>{
    })
}
 // **********a six *******
 handleFileInputasix($event:any,images){
  this.imagefive= File = $event.target.files[0];
  // this.formData.append(images, imageFile);
}
 
addssix(){
  let formData: FormData = new FormData();
  formData.append('images1', this.imagefive); 
   
  this.swim.addsix(formData).subscribe((res)=>{
    
      })
       this.asix.images1=""
 }
 addssixget(){
           
  this.swim.addssixgets().subscribe((res)=>{
    this.assix=res
    })
 }
 addssixdelete(deleteid){
  let deleteids=deleteid._id      
 this.swim.addsixdelete(deleteids).subscribe((res)=>{
    })
}

 // **********a seven *******
 handleFileInputaseven($event:any,images){
  this.imageseven= File = $event.target.files[0];
  // this.formData.append(images, imageFile);
}
 
addsevens(){
  let formData: FormData = new FormData();
  formData.append('images1', this.imageseven); 
   
  this.swim.addseven(formData).subscribe((res)=>{
    
      })
       this.aseven.images1=""
 }
 addssevenget(){
           
  this.swim.addssevengets().subscribe((res)=>{
    this.asseven=res
    })
 }
 addssevendelete(deleteid){
  let deleteids=deleteid._id      
 this.swim.addsevendelete(deleteids).subscribe((res)=>{
    })
}
 // **********a eight *******
 handleFileInputaeight($event:any,images){
  this.imageeight= File = $event.target.files[0];
  // this.formData.append(images, imageFile);
}
 
addseight(){
  let formData: FormData = new FormData();
  formData.append('images1', this.imageeight); 
   
  this.swim.addeight(formData).subscribe((res)=>{
    
      })
       this.aeight.images1=""
 }
 addseightget(){
           
  this.swim.addseightgets().subscribe((res)=>{
    this.aseight=res
    })
 }
 addseightdelete(deleteid){
  let deleteids=deleteid._id      
 this.swim.addeightdelete(deleteids).subscribe((res)=>{
    })
}
 // **********a nine *******
 handleFileInputanine($event:any,images){
  this.imagenine= File = $event.target.files[0];
  // this.formData.append(images, imageFile);
}
 
addsnine(){
  let formData: FormData = new FormData();
  formData.append('images1', this.imagenine); 
   
  this.swim.addnine(formData).subscribe((res)=>{
    
      })
       this.anine.images1=""
 }
 addsnineget(){
           
  this.swim.addsninegets().subscribe((res)=>{
    this.asnine=res
    })
 }
 addsninedelete(deleteid){
  let deleteids=deleteid._id      
 this.swim.addninedelete(deleteids).subscribe((res)=>{
    })
}
 // **********a ten *******
 handleFileInputaten($event:any,images){
  this.imageten= File = $event.target.files[0];
  // this.formData.append(images, imageFile);
}
 
addsten(){
  let formData: FormData = new FormData();
  formData.append('images1', this.imageten); 
   
  this.swim.addten(formData).subscribe((res)=>{
    
      })
       this.aten.images1=""
 }
 addstenget(){
           
  this.swim.addstengets().subscribe((res)=>{
    this.asten=res
    })
 }

 addstendelete(deleteid){
  let deleteids=deleteid._id      
 this.swim.addtendelete(deleteids).subscribe((res)=>{
    })
}
 
}

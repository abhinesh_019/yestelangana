import { TestBed, inject } from '@angular/core/testing';

import { MainmenuService } from './mainmenu.service';

describe('MainmenuService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MainmenuService]
    });
  });

  it('should be created', inject([MainmenuService], (service: MainmenuService) => {
    expect(service).toBeTruthy();
  }));
});

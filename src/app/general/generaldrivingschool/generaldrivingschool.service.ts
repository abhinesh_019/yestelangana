import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment';

@Injectable()
export class GeneraldrivingschoolService {
  constructor(private http: HttpClient) { }

  
  locationspost(data) {
    return   this.http.post(environment.apiUrl +"/drivingSchoolsLocations",data);

 
    }

    locationsget(){
       return this.http.get(environment.apiUrl +"/drivingSchoolsLocations");
                   }
                   
                   areaapi(id){
    
                    return this.http.get(environment.apiUrl +"/drivingSchoolsArea/"+id);
                  }

                  areapost(id,data){
                    return   this.http.post(environment.apiUrl +"/drivingSchoolsArea/"+id,data);
                   }
                   areapostusers(id,data){
                    return   this.http.post(environment.apiUrl +"/drivingSchoolsClientsPost/"+id,data);
                   }
 
                   babycaresAddsOnea(id){

                    return this.http.get(environment.apiUrl +"/drivingSchoolsAddsOneGeta/"+id);
                  }
                  babycaresAddsTwoa(id){
                    
                    return this.http.get(environment.apiUrl +"/drivingSchoolsAddsTwoGeta/"+id);
                  }
                  babycaresAddsThreea(id){
                    
                    return this.http.get(environment.apiUrl +"/drivingSchoolsAddsThreeGeta/"+id);
                  }
                  babycaresAddsFoura(id){
                    
                    return this.http.get(environment.apiUrl +"/drivingSchoolsAddsFourGeta/"+id);
                  }
                   
                  babycaresAddsOnel(id){
                    
                    return this.http.get(environment.apiUrl +"/drivingSchoolsAddsOneLocGet/"+id);
                  }
                  babycaresAddsTwol(id){
                    
                    return this.http.get(environment.apiUrl +"/drivingSchoolsAddsTwoLocGet/"+id);
                  }
                  babycaresAddsThreel(id){
                    
                    return this.http.get(environment.apiUrl +"/drivingSchoolsAddsThreeGetLoc/"+id);
                  }
                  babycaresAddsFourl(id){
                    
                    return this.http.get(environment.apiUrl +"/drivingSchoolsAddsFourLocGet/"+id);
                  }
                
                  babycaresAddsFivel(id){
                    
                    return this.http.get(environment.apiUrl +"/drivingSchoolsAddsFiveLocGet/"+id);
                  }
                   
                  postAddsOne(id,data){
                    return   this.http.post(environment.apiUrl +"/drivingSchoolsAddsOnePtsa/"+id,data);
                   }
                   postAddsTwo(id,data){
                    return   this.http.post(environment.apiUrl +"/drivingSchoolsAddsTwoPtsa/"+id,data);
                   }
                   postAddsThree(id,data){
                    return   this.http.post(environment.apiUrl +"/drivingSchoolsAddsThreePtsa/"+id,data);
                   }
                   postAddsFour(id,data){
                    return   this.http.post(environment.apiUrl +"/drivingSchoolsAddsFourPtsa/"+id,data);
                   }
                   
                
                
                  postAddsOnepl(id,data){
                    return   this.http.post(environment.apiUrl +"/drivingSchoolsAddsOneLocPts/"+id,data);
                   }
                   postAddsTwopl(id,data){
                    return   this.http.post(environment.apiUrl +"/drivingSchoolsAddsTwoLocPts/"+id,data);
                   }
                   postAddsThreepl(id,data){
                    return   this.http.post(environment.apiUrl +"/drivingSchoolsAddsThreePtsLoc/"+id,data);
                   }
                   postAddsFourpl(id,data){
                    return   this.http.post(environment.apiUrl +"/drivingSchoolsAddsFourLocPts/"+id,data);
                   }
                   postAddsFivepl(id,data){
                    return   this.http.post(environment.apiUrl +"/drivingSchoolsAddsFiveLocPts/"+id,data);
                   }
                   
                
                
                   babycaresAddsOneaDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/drivingSchoolsAddsOneDeletea/"+id);
                  }
                 
                  babycaresAddsTwoaDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/drivingSchoolsAddsTwoDeletea/"+id);
                  }
                  babycaresAddsThreeaDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/drivingSchoolsAddsThreeDeletea/"+id);
                  }
                  babycaresAddsFouraDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/drivingSchoolsAddsFourDeletea/"+id);
                  }
                
                
                   babycaresAddsOneLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/drivingSchoolsAddsOneLocDelete/"+id);
                  }
                
                  babycaresAddsTwoLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/drivingSchoolsAddsTwoLocDelete/"+id);
                  }
                  babycaresAddsThreeLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/drivingSchoolsAddsThreeDeleteLoc/"+id);
                  }
                  babycaresAddsFourLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/drivingSchoolsAddsFourLocDelete/"+id);
                  }
                  babycaresAddsFiveLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/drivingSchoolsAddsFiveLocDelete/"+id);
                  }
                   
                 }
                




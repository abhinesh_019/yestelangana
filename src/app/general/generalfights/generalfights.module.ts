import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { GeneralfightsComponent } from './generalfights.component';
import { GeneralfightsService } from './generalfights.service';
  
const routes:Routes=[{path:'generalsfights',component:GeneralfightsComponent}]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    GeneralfightsComponent
  ],
  providers: [ GeneralfightsService],
})
export class GeneralfightsModule { }

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment';

@Injectable()

export class GeneralfightsService {

  constructor(private http: HttpClient) { }
  catpost(data) {
    return   this.http.post(environment.apiUrl +"/fightsCategoriesp",data);
 }

 catapiget(){
       return this.http.get(environment.apiUrl +"/fightsCategories/");
                   }
 
  locationspost(id,data) {
    return   this.http.post(environment.apiUrl +"/fightsLocations/"+id,data);
 }

    locationsget(id){
       return this.http.get(environment.apiUrl +"/fightsLocations/"+id);
                   }
                   
                   areaapi(id){
    
                    return this.http.get(environment.apiUrl +"/fightsAreas/"+id);
                  }

                  areapost(id,data){
                    return   this.http.post(environment.apiUrl +"/fightsAreas/"+id,data);
                   }
                   areapostusers(id,data){
                    return   this.http.post(environment.apiUrl +"/fightsClients/"+id,data);

                   }
                   
                   babycaresAddsOnea(id){

                    return this.http.get(environment.apiUrl +"/fightsAddsOneGeta/"+id);
                  }
                  babycaresAddsTwoa(id){
                    
                    return this.http.get(environment.apiUrl +"/fightsAddsTwoGeta/"+id);
                  }
                  babycaresAddsThreea(id){
                    
                    return this.http.get(environment.apiUrl +"/fightsAddsThreeGeta/"+id);
                  }
                  babycaresAddsFoura(id){
                    
                    return this.http.get(environment.apiUrl +"/fightsAddsFourGeta/"+id);
                  }
                   
                  babycaresAddsOnel(id){
                    
                    return this.http.get(environment.apiUrl +"/fightsAddsOneLocGet/"+id);
                  }
                  babycaresAddsTwol(id){
                    
                    return this.http.get(environment.apiUrl +"/fightsAddsTwoLocGet/"+id);
                  }
                  babycaresAddsThreel(id){
                    
                    return this.http.get(environment.apiUrl +"/fightsAddsThreeGetLoc/"+id);
                  }
                  babycaresAddsFourl(id){
                    
                    return this.http.get(environment.apiUrl +"/fightsAddsFourLocGet/"+id);
                  }
                
                  babycaresAddsFivel(id){
                    
                    return this.http.get(environment.apiUrl +"/fightsAddsFiveLocGet/"+id);
                  }
                   
                  postAddsOne(id,data){
                    return   this.http.post(environment.apiUrl +"/fightsAddsOnePtsa/"+id,data);
                   }
                   postAddsTwo(id,data){
                    return   this.http.post(environment.apiUrl +"/fightsAddsTwoPtsa/"+id,data);
                   }
                   postAddsThree(id,data){
                    return   this.http.post(environment.apiUrl +"/fightsAddsThreePtsa/"+id,data);
                   }
                   postAddsFour(id,data){
                    return   this.http.post(environment.apiUrl +"/fightsAddsFourPtsa/"+id,data);
                   }
                   
                
                
                  postAddsOnepl(id,data){
                    return   this.http.post(environment.apiUrl +"/fightsAddsOneLocPts/"+id,data);
                   }
                   postAddsTwopl(id,data){
                    return   this.http.post(environment.apiUrl +"/fightsAddsTwoLocPts/"+id,data);
                   }
                   postAddsThreepl(id,data){
                    return   this.http.post(environment.apiUrl +"/fightsAddsThreePtsLoc/"+id,data);
                   }
                   postAddsFourpl(id,data){
                    return   this.http.post(environment.apiUrl +"/fightsAddsFourLocPts/"+id,data);
                   }
                   postAddsFivepl(id,data){
                    return   this.http.post(environment.apiUrl +"/fightsAddsFiveLocPts/"+id,data);
                   }
                   
                
                
                   babycaresAddsOneaDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/fightsAddsOneDeletea/"+id);
                  }
                 
                  babycaresAddsTwoaDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/fightsAddsTwoDeletea/"+id);
                  }
                  babycaresAddsThreeaDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/fightsAddsThreeDeletea/"+id);
                  }
                  babycaresAddsFouraDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/fightsAddsFourDeletea/"+id);
                  }
                
                
                   babycaresAddsOneLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/fightsAddsOneLocDelete/"+id);
                  }
                
                  babycaresAddsTwoLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/fightsAddsTwoLocDelete/"+id);
                  }
                  babycaresAddsThreeLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/fightsAddsThreeDeleteLoc/"+id);
                  }
                  babycaresAddsFourLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/fightsAddsFourLocDelete/"+id);
                  }
                  babycaresAddsFiveLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/fightsAddsFiveLocDelete/"+id);
                  }
                   
                 }
                




import { TestBed, inject } from '@angular/core/testing';

import { GeneralfightsService } from './generalfights.service';

describe('GeneralfightsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeneralfightsService]
    });
  });

  it('should be created', inject([GeneralfightsService], (service: GeneralfightsService) => {
    expect(service).toBeTruthy();
  }));
});

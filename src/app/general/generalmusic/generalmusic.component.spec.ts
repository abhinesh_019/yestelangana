import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralmusicComponent } from './generalmusic.component';

describe('GeneralmusicComponent', () => {
  let component: GeneralmusicComponent;
  let fixture: ComponentFixture<GeneralmusicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralmusicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralmusicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

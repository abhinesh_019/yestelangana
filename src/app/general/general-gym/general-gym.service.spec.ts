import { TestBed, inject } from '@angular/core/testing';

import { GeneralGymService } from './general-gym.service';

describe('GeneralGymService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeneralGymService]
    });
  });

  it('should be created', inject([GeneralGymService], (service: GeneralGymService) => {
    expect(service).toBeTruthy();
  }));
});

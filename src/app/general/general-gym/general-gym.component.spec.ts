import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralGymComponent } from './general-gym.component';

describe('GeneralGymComponent', () => {
  let component: GeneralGymComponent;
  let fixture: ComponentFixture<GeneralGymComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralGymComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralGymComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

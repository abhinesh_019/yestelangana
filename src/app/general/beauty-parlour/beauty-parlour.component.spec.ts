import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeautyParlourComponent } from './beauty-parlour.component';

describe('BeautyParlourComponent', () => {
  let component: BeautyParlourComponent;
  let fixture: ComponentFixture<BeautyParlourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeautyParlourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeautyParlourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

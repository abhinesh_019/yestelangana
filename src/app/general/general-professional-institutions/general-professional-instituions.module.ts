import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { GeneralProfessionalInstitutionsComponent } from './general-professional-institutions.component';
import { GeneralProfessionalInstituionsService } from './general-professional-instituions.service';
 


const routes:Routes=[{path:'generalProfessionalInstituions',component:GeneralProfessionalInstitutionsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    GeneralProfessionalInstitutionsComponent,
  ],
  providers: [GeneralProfessionalInstituionsService],
})
 
 
export class GeneralProfessionalInstituionsModule { }

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GeneralProfessionalInstituionsService } from './general-professional-instituions.service';

@Component({
  selector: 'app-general-professional-institutions',
  templateUrl: './general-professional-institutions.component.html',
  styleUrls: ['./general-professional-institutions.component.css']
})
export class GeneralProfessionalInstitutionsComponent implements OnInit {
  public imageAddsOne:any
  public babycaresAddsOnes:any
   
  public babycaresAddsFours:any
  public imageAddsFour:any
  public imageAddsThree:any
  public imageAddsTwo:any
  public babycaresAddsThrees:any
  public babycaresAddsTwos:any
  
  
  public babycaresAddsFivesLoc:any
  public imageAddsFiveLoc:any
  public imageAddsFourLoc:any
  public babycaresAddsFoursLoc:any
  public imageAddsThreeLoc:any
  public babycaresAddsThreeLoc:any
  public imageAddsTwoLoc:any
  public babycaresAddsTwoLoc:any
  public imageAddsOneLoc:any
  public babycaresAddsOneLoc:any
  public babycaresAddsOneLocl:any


  areaAddsOne={
    "addsOneLink":"",
    "addsOneImg":"",
  }
  areaAddsTwo={
    "addsTwoLink":"",
    "addsTwoImg":"",
  }
  areaAddsThree={
    "addsThreeLink":"",
    "addsThreeImg":"",
  }
  areaAddsFour={
    "addsFourLink":"",
    "addsFourImg":"",
  }
   
  
  areaAddsOneLoc={
    "addsOneLinkLoc":"",
    "addsOneImgLoc":"",
  }
  areaAddsTwoLoc={
    "addsTwoLinkLoc":"",
    "addsTwoImgLoc":"",
  }
  areaAddsThreeLoc={
    "addsThreeLinkLoc":"",
    "addsThreeImgLoc":"",
  }
  areaAddsFourLoc={
    "addsFourLinkLoc":"",
    "addsFourImgLoc":"",
  }
  areaAddsFiveLoc={
    "addsFiveLinkLoc":"",
    "addsFiveImgLoc":"",
  }
   
  public furniturlocations:any
  public selectAreaId:any
  public fileids:any
  public selectArea:any
  public locationsget:any
  public locationName:any
  public its:any;
  public locationget:any
  public areasAll:any
  public locationsgets:any
   public it:any
  public locationss:any
   public imagesName:any
  public catList:any
  public catName:any
  locationsdata={
    "locations":""
  }
  
  areas={
    "subArea":""
  }
  
  
  products={
    images:"",
    name:"",
    price:"",
    productDescription:"",
    guarantee:"",
  }
  
  
  pro={
    instName:"",
    name:"",
       day1:"",
     day2:"",
     day3:"",
     day4:"",
     day5:"",
     day6:"",
     day7:"",
     allTimes:"",
     houseNo:"",
     subArea:"",
     landmark:"",
     city:"",
     distict:"",
     state:"",
     pincode:"",
     officeNo:"",
     mobileNo:"",
     whatsupno:"",
     emailId:"",
     servicesDescriptionsOne:"",
     servicesDescriptionsTwo:"",
     latitude:"",
     longitude:"",
     images1:"",
     images2:"",
     images3:"",
     images4:"",
     images5:"",
     images6:""
  
  }
  catdata={
    "categories":"",
    "images1":"",
   }
  public formData: any = new FormData();
  
    constructor(private route:ActivatedRoute,private professional:GeneralProfessionalInstituionsService) { }
   
    ngOnInit() {
      this.getcatagories()
       
    }
    
// *********************catagories***************   
handleFile($event:any,images){
  this.imagesName= File = $event.target.files[0]; 
 
}

postcat(){
 
 this.formData.append('images1', this.imagesName);
 
 this.formData.append('categories',this.catdata.categories);
  this.professional.catpost(this.formData).subscribe((res)=>{
     })
   this.catdata.categories="",
     this.catdata.images1=""
     
}

getcatagories(){
  this.professional.catapiget().subscribe((res)=>{
this.catList=res 

var id =this.catList[0]; 
      this.catagoriescal(id)

        })  }
      catagoriescal(get?){
        this.catName=get.categories
        this.its=get._id
        this.getlocations()
       }
      
      // ************************************   


  postlocations(){
    this.professional.locationspost(this.its,this.locationsdata).subscribe((res)=>{
      this.locationsdata.locations=""
    })  }

    getlocations(){
      this.professional.locationsget(this.its).subscribe((res)=>{
      this.locationsgets=res
       var id =this.locationsgets[0];
      this.locations(id)
      })  }
      locations(get?){
        this.locationName=get.locations
        this.it=get._id
        this.locationss=get.locations
        this.allAreas()
        this.babycaressareasAddsOneLoc()
        this.babycaressareasAddsTwoLoc()
        this.babycaressareasAddsThreeLoc()
        this.babycaressareasAddsFourLoc()
        this.babycaressareasAddsFiveLoc()
      }

      allAreas(){
        this.professional.areaapi(this.it).subscribe((res)=>{
          this.areasAll=res
          var id =this.areasAll[0];          
          this.selectedAreas(id)
         })
       }
      
      selectedAreas(result){
        this.selectAreaId=result._id
        this.selectArea=result.subArea
       
        this.babycaressareasAddsOnea()
        this.babycaressareasAddsTwoa()
        this.babycaressareasAddsThreea()
        this.babycaressareasAddsFoura()
      }
      
      postarea(){
        this.professional.areapost(this.it,this.areas).subscribe((res)=>{ 
          this.areas.subArea=""
        }) 
      }
      //  ************************main users details *********************

 handleFileInput($event:any,img) {
  let imageFile= File = $event.target.files[0];
  this.formData.append(img, imageFile); 
  
}


    
postUsers(){
 
  this.formData.append('name',this.pro.name);
  this.formData.append('instName',this.pro.instName);
  this.formData.append('day2',this.pro.day1);
  this.formData.append('day2',this.pro.day2);
  this.formData.append('day3',this.pro.day3);
  this.formData.append('day4',this.pro.day4);
  this.formData.append('day5',this.pro.day5);
  this.formData.append('day6',this.pro.day6);
  this.formData.append('day7',this.pro.day7);
  this.formData.append('allTimes',this.pro.allTimes);
  this.formData.append('houseNo',this.pro.houseNo);
  this.formData.append('mainArea',this.selectArea);
  this.formData.append('subArea',this.pro.subArea);
  this.formData.append('landmark',this.pro.landmark);
  this.formData.append('city',this.pro.city);
  this.formData.append('distict',this.locationName);
  this.formData.append('state',this.pro.state);
  this.formData.append('pincode',this.pro.pincode);
  this.formData.append('officeNo',this.pro.officeNo);
  this.formData.append('mobileNo',this.pro.mobileNo);
  this.formData.append('whatsupno',this.pro.whatsupno);
  this.formData.append('emailId',this.pro.emailId);
  this.formData.append('servicesDescriptionsOne',this.pro.servicesDescriptionsOne);
  this.formData.append('servicesDescriptionsTwo',this.pro.servicesDescriptionsTwo);
  this.formData.append('latitude',this.pro.latitude);
  this.formData.append('longitude',this.pro.longitude);
  this.formData.append('instCat',this.catName);


this.pro.name="",
this.pro.instName="",
 this.pro.day1="",
this.pro.day2="",
this.pro.day3="",
this.pro.day4="",
this.pro.day5="",
this.pro.day6="",
this.pro.day7="",
this.pro.allTimes="",
this.pro.houseNo="",
this.pro.subArea="",
this.pro.landmark="",
this.pro.city="",
this.pro.state="",
this.pro.pincode="",
this.pro.officeNo="",
this.pro.mobileNo="",
this.pro.whatsupno="",
this.pro.emailId="",
this.pro.servicesDescriptionsOne="",
this.pro.servicesDescriptionsTwo="",
this.pro.latitude="",
this.pro.longitude="",
this.pro.images1="",
this.pro.images2="",
this.pro.images3="",
this.pro.images4="",
this.pro.images5="",
this.pro.images6="",

    
      this.professional.areapostusers(this.selectAreaId,this.formData).subscribe((res)=>{ 
         
      }) 
}


 
      

// ********************************************************* Adds Areas One ***********************************************************************

handleFileInputUpdates($event:any,images){
  this.imageAddsOne= File = $event.target.files[0];
 }
 

postAddsOne(){
this.formData.append('addsOneImg',this.imageAddsOne);
this.formData.append('addsOneLink',this.areaAddsOne.addsOneLink); 
 
 this.professional.postAddsOne(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsOne.addsOneLink="",
     this.areaAddsOne.addsOneImg=""
}
babycaressareasAddsOnea(){
  this.professional.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
 
   })
}
babycaressareasAddsOneaDelete(identities){
  let areaIds=identities._id
  this.professional.babycaresAddsOneaDelete(areaIds).subscribe((res)=>{
    
  })
}

// ********************************************************* Adds Areas Two ***********************************************************************

handleFileInputUpdatesTwo($event:any,images){
  this.imageAddsTwo= File = $event.target.files[0];
 }
 

postAddsTwo(){
this.formData.append('addsTwoImg',this.imageAddsTwo);
this.formData.append('addsTwoLink',this.areaAddsTwo.addsTwoLink); 
 
 this.professional.postAddsTwo(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsTwo.addsTwoLink="",
     this.areaAddsTwo.addsTwoImg=""
}
babycaressareasAddsTwoa(){
  this.professional.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
   })
}
babycaressareasAddsTwoaDelete(identities){
  let areaIds=identities._id
  this.professional.babycaresAddsTwoaDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Three ***********************************************************************

handleFileInputUpdatesThree($event:any,images){
  this.imageAddsThree= File = $event.target.files[0];
 }
 

postAddsThree(){
this.formData.append('addsThreeImg',this.imageAddsThree);
this.formData.append('addsThreeLink',this.areaAddsThree.addsThreeLink); 
 
 this.professional.postAddsThree(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsThree.addsThreeLink="",
     this.areaAddsThree.addsThreeImg=""
}
babycaressareasAddsThreea(){
  this.professional.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
   })
}
babycaressareasAddsThreeaDelete(identities){
  let areaIds=identities._id
  this.professional.babycaresAddsThreeaDelete(areaIds).subscribe((res)=>{  
  })
}


// ********************************************************* Adds Areas Four ***********************************************************************

handleFileInputUpdatesFour($event:any,images){
  this.imageAddsFour= File = $event.target.files[0];
 }
 

postAddsFour(){
this.formData.append('addsFourImg',this.imageAddsFour);
this.formData.append('addsFourLink',this.areaAddsFour.addsFourLink); 
 
 this.professional.postAddsFour(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsFour.addsFourLink="",
     this.areaAddsFour.addsFourImg=""
}
babycaressareasAddsFoura(){
  this.professional.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res
   })
}
babycaressareasAddsFouraDelete(identities){
  let areaIds=identities._id
  this.professional.babycaresAddsFouraDelete(areaIds).subscribe((res)=>{
  })
}

 

 


// ********************************************************* Adds Areas One ***********************************************************************

handleFileInputUpdatesOneLoc($event:any,images){
  this.imageAddsOneLoc= File = $event.target.files[0];
 }
 

postAddsOneLoc(){
this.formData.append('addsOneImgLoc',this.imageAddsOneLoc);
this.formData.append('addsOneLinkLoc',this.areaAddsOneLoc.addsOneLinkLoc); 
 
 this.professional.postAddsOnepl(this.it,this.formData).subscribe((res)=>{
    
})
     this.areaAddsOneLoc.addsOneImgLoc="",
     this.areaAddsOneLoc.addsOneLinkLoc=""
}
babycaressareasAddsOneLoc(){
  this.professional.babycaresAddsOnel(this.it).subscribe((res)=>{
     this.babycaresAddsOneLocl=res
   })
}
babycaressareasAddsOneLocDelete(identities){
  let areaIds=identities._id
  this.professional.babycaresAddsOneLocDelete(areaIds).subscribe((res)=>{
    
  })
}

// ********************************************************* Adds Areas Two ***********************************************************************

handleFileInputUpdatesTwoLoc($event:any,images){
  this.imageAddsTwoLoc= File = $event.target.files[0];
 }
 

postAddsTwoLoc(){
this.formData.append('addsTwoImgLoc',this.imageAddsTwoLoc);
this.formData.append('addsTwoLinkLoc',this.areaAddsTwoLoc.addsTwoLinkLoc); 
 
 this.professional.postAddsTwopl(this.it,this.formData).subscribe((res)=>{
     })
     this.areaAddsTwoLoc.addsTwoLinkLoc="",
     this.areaAddsTwoLoc.addsTwoImgLoc=""
}
babycaressareasAddsTwoLoc(){
  this.professional.babycaresAddsTwol(this.it).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
   })
}
babycaressareasAddsTwoLocDelete(identities){
  let areaIds=identities._id
  this.professional.babycaresAddsTwoLocDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Three Loc***********************************************************************

handleFileInputUpdatesThreeLoc($event:any,images){
  this.imageAddsThreeLoc= File = $event.target.files[0];
 }
 

postAddsThreeLoc(){
this.formData.append('addsThreeImgLoc',this.imageAddsThreeLoc);
this.formData.append('addsThreeLinkLoc',this.areaAddsThreeLoc.addsThreeLinkLoc); 
 
 this.professional.postAddsThreepl(this.it,this.formData).subscribe((res)=>{
     })
     this.areaAddsThreeLoc.addsThreeImgLoc="",
     this.areaAddsThreeLoc.addsThreeLinkLoc=""
}
babycaressareasAddsThreeLoc(){
  this.professional.babycaresAddsThreel(this.it).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
   })
}
babycaressareasAddsThreeLocDelete(identities){
  let areaIds=identities._id
  this.professional.babycaresAddsThreeLocDelete(areaIds).subscribe((res)=>{
  
  })
}


// ********************************************************* Adds Areas Four Loc***********************************************************************

handleFileInputUpdatesFourLoc($event:any,images){
  this.imageAddsFourLoc= File = $event.target.files[0];
 }
 

postAddsFourLoc(){
this.formData.append('addsFourImgLoc',this.imageAddsFourLoc);
this.formData.append('addsFourLinkLoc',this.areaAddsFourLoc.addsFourLinkLoc); 
 
 this.professional.postAddsFourpl(this.it,this.formData).subscribe((res)=>{
     })
     this.areaAddsFourLoc.addsFourImgLoc="",
     this.areaAddsFourLoc.addsFourLinkLoc=""
}
babycaressareasAddsFourLoc(){
  this.professional.babycaresAddsFourl(this.it).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
   })
}
babycaressareasAddsFourLocDelete(identities){
  let areaIds=identities._id
  this.professional.babycaresAddsFourLocDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Five Loc***********************************************************************

handleFileInputUpdatesFiveLoc($event:any,images){
  this.imageAddsFiveLoc= File = $event.target.files[0];
 }
 

postAddsFiveLoc(){
this.formData.append('addsFiveImgLoc',this.imageAddsFiveLoc);
this.formData.append('addsFiveLinkLoc',this.areaAddsFiveLoc.addsFiveLinkLoc); 
 
 this.professional.postAddsFivepl(this.it,this.formData).subscribe((res)=>{
     })
     this.areaAddsFiveLoc.addsFiveImgLoc="",
     this.areaAddsFiveLoc.addsFiveLinkLoc=""
}
babycaressareasAddsFiveLoc(){
  this.professional.babycaresAddsFivel(this.it).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
   })
}
babycaressareasAddsFiveLocDelete(identities){
  let areaIds=identities._id
  this.professional.babycaresAddsFiveLocDelete(areaIds).subscribe((res)=>{
    
  })
}

 
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment';
    
@Injectable()
export class GeneralProfessionalInstituionsService {
  constructor(private http: HttpClient) { }

  catpost(data) {
    return   this.http.post(environment.apiUrl +"/professionalInstCategoriesP",data);
 }

 catapiget(){
       return this.http.get(environment.apiUrl +"/professionalInstCategories");
                   }
 
                   locationspost(id,data) {
    return   this.http.post(environment.apiUrl +"/professionalInstlocations/"+id,data);
 }

    locationsget(id){
       return this.http.get(environment.apiUrl +"/professionalInstlocations/"+id);
                   }
                   
                   areaapi(id){
    
                    return this.http.get(environment.apiUrl +"/professionalInstAreas/"+id);
                  }

                  areapost(id,data){
                    return   this.http.post(environment.apiUrl +"/professionalInstAreas/"+id,data);
                   }
             
                
                   areapostusers(id,data){
                    return   this.http.post(environment.apiUrl +"/professionalInstclientsPost/"+id,data);
                   }
     

                   babycaresAddsOnea(id){

                    return this.http.get(environment.apiUrl +"/professionalAddsOneGeta/"+id);
                  }
                  babycaresAddsTwoa(id){
                    
                    return this.http.get(environment.apiUrl +"/professionalAddsTwoGeta/"+id);
                  }
                  babycaresAddsThreea(id){
                    
                    return this.http.get(environment.apiUrl +"/professionalAddsThreeGeta/"+id);
                  }
                  babycaresAddsFoura(id){
                    
                    return this.http.get(environment.apiUrl +"/professionalAddsFourGeta/"+id);
                  }
                   
                  babycaresAddsOnel(id){
                    
                    return this.http.get(environment.apiUrl +"/professionalAddsOneLocGet/"+id);
                  }
                  babycaresAddsTwol(id){
                    
                    return this.http.get(environment.apiUrl +"/professionalAddsTwoLocGet/"+id);
                  }
                  babycaresAddsThreel(id){
                    
                    return this.http.get(environment.apiUrl +"/professionalAddsThreeGetLoc/"+id);
                  }
                  babycaresAddsFourl(id){
                    
                    return this.http.get(environment.apiUrl +"/professionalAddsFourLocGet/"+id);
                  }
                
                  babycaresAddsFivel(id){
                    
                    return this.http.get(environment.apiUrl +"/professionalAddsFiveLocGet/"+id);
                  }
                   
                  postAddsOne(id,data){
                    return   this.http.post(environment.apiUrl +"/professionalAddsOnePtsa/"+id,data);
                   }
                   postAddsTwo(id,data){
                    return   this.http.post(environment.apiUrl +"/professionalAddsTwoPtsa/"+id,data);
                   }
                   postAddsThree(id,data){
                    return   this.http.post(environment.apiUrl +"/professionalAddsThreePtsa/"+id,data);
                   }
                   postAddsFour(id,data){
                    return   this.http.post(environment.apiUrl +"/professionalAddsFourPtsa/"+id,data);
                   }
                   
                
                
                  postAddsOnepl(id,data){
                    return   this.http.post(environment.apiUrl +"/professionalAddsOneLocPts/"+id,data);
                   }
                   postAddsTwopl(id,data){
                    return   this.http.post(environment.apiUrl +"/professionalAddsTwoLocPts/"+id,data);
                   }
                   postAddsThreepl(id,data){
                    return   this.http.post(environment.apiUrl +"/professionalAddsThreePtsLoc/"+id,data);
                   }
                   postAddsFourpl(id,data){
                    return   this.http.post(environment.apiUrl +"/professionalAddsFourLocPts/"+id,data);
                   }
                   postAddsFivepl(id,data){
                    return   this.http.post(environment.apiUrl +"/professionalAddsFiveLocPts/"+id,data);
                   }
                   
                
                
                   babycaresAddsOneaDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/professionalAddsOneDeletea/"+id);
                  }
                 
                  babycaresAddsTwoaDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/professionalAddsTwoDeletea/"+id);
                  }
                  babycaresAddsThreeaDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/professionalAddsThreeDeletea/"+id);
                  }
                  babycaresAddsFouraDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/professionalAddsFourDeletea/"+id);
                  }
                
                
                   babycaresAddsOneLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/professionalAddsOneLocDelete/"+id);
                  }
                
                  babycaresAddsTwoLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/professionalAddsTwoLocDelete/"+id);
                  }
                  babycaresAddsThreeLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/professionalAddsThreeDeleteLoc/"+id);
                  }
                  babycaresAddsFourLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/professionalAddsFourLocDelete/"+id);
                  }
                  babycaresAddsFiveLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/professionalAddsFiveLocDelete/"+id);
                  }
                   
                 }
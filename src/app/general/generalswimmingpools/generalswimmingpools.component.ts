import { Component, OnInit } from '@angular/core';
import { GeneralswmmingpoolsService } from './generalswmmingpools.service';

@Component({
  selector: 'app-generalswimmingpools',
  templateUrl: './generalswimmingpools.component.html',
  styleUrls: ['./generalswimmingpools.component.css']
})
export class GeneralswimmingpoolsComponent implements OnInit {
  public imageAddsOne:any
  public babycaresAddsOnes:any
   
  public babycaresAddsFours:any
  public imageAddsFour:any
  public imageAddsThree:any
  public imageAddsTwo:any
  public babycaresAddsThrees:any
  public babycaresAddsTwos:any
  
  
  public babycaresAddsFivesLoc:any
  public imageAddsFiveLoc:any
  public imageAddsFourLoc:any
  public babycaresAddsFoursLoc:any
  public imageAddsThreeLoc:any
  public babycaresAddsThreeLoc:any
  public imageAddsTwoLoc:any
  public babycaresAddsTwoLoc:any
  public imageAddsOneLoc:any
  public babycaresAddsOneLoc:any
  public babycaresAddsOneLocl:any


  areaAddsOne={
    "addsOneLink":"",
    "addsOneImg":"",
  }
  areaAddsTwo={
    "addsTwoLink":"",
    "addsTwoImg":"",
  }
  areaAddsThree={
    "addsThreeLink":"",
    "addsThreeImg":"",
  }
  areaAddsFour={
    "addsFourLink":"",
    "addsFourImg":"",
  }
   
  
  areaAddsOneLoc={
    "addsOneLinkLoc":"",
    "addsOneImgLoc":"",
  }
  areaAddsTwoLoc={
    "addsTwoLinkLoc":"",
    "addsTwoImgLoc":"",
  }
  areaAddsThreeLoc={
    "addsThreeLinkLoc":"",
    "addsThreeImgLoc":"",
  }
  areaAddsFourLoc={
    "addsFourLinkLoc":"",
    "addsFourImgLoc":"",
  }
  areaAddsFiveLoc={
    "addsFiveLinkLoc":"",
    "addsFiveImgLoc":"",
  }
  
  locationsdata={
    "locations":""
  }
  areas={
    "area":""
  }
  public locationsgets:any
  public locationName:any
  public its:any
  public locationss:any
  public areasAll:any
  public selectAreaId:any
  public selectArea:any

  bcmain={
    "name":"",
    "swimmingName":"",
    "aboutUseDes1":"",
    "aboutUseDes2":"",
    "ourTeam":"",
    "teamMembersNames":"",
    "teamDescriptions":"",
     "day1":"",
    "day2":"",
    "day3":"",
    "day4":"",
    "day5":"",
    "day6":"",
    "day7":"",
    "allTimes":"",
    "houseNo":"",
    "subArea":"",
    "landmark":"",
    "city":"",
     "state":"",
    "pincode":"",
    "officeNo":"",
    "mobileNo":"",
    "whatsupno":"",
    "emailId":"",
    "latitude":"",
    "longitude":"",
  "images1":"",
  "images2":"",
  "images3":"",
  "images4":"",
  "images5":"",
  "images6":""
  
  }
  
  public formData: any = new FormData();
  
  
  constructor(private swim:GeneralswmmingpoolsService) { }

  ngOnInit() {
    this.getlocations()
  }
  postlocations(){
 
    this.swim.locationspost(this.locationsdata).subscribe((res)=>{ 
      this.locationsdata.locations=""
    })  }

    getlocations(){
      this.swim.locationsget().subscribe((res)=>{
      this.locationsgets=res
     
      var id =this.locationsgets[0];
    
      
      this.locations(id)
         
      })  }
      locations(get?){
        this.locationName=get.locations
        this.its=get._id
        this.locationss=get.locations 
        this.allAreas()
        this.babycaressareasAddsOneLoc()
        this.babycaressareasAddsTwoLoc()
        this.babycaressareasAddsThreeLoc()
        this.babycaressareasAddsFourLoc()
        this.babycaressareasAddsFiveLoc()
      }
      allAreas(){
           
        this.swim.areaapi(this.its).subscribe((res)=>{
          this.areasAll=res
          var id =this.areasAll[0]; 
          
          this.selectedAreas(id)
          
        })
       
      }
      
      selectedAreas(result){
        this.selectAreaId=result._id
        this.selectArea=result.area
        this.babycaressareasAddsOnea()
        this.babycaressareasAddsTwoa()
        this.babycaressareasAddsThreea()
        this.babycaressareasAddsFoura() 
      }
      
      postarea(){
        this.swim.areapost(this.its,this.areas).subscribe((res)=>{ 
          this.areas.area=""
        }) 
      }
      
      //  ************************main users details *********************

 handleFileInput($event:any,img) {
  let imageFile= File = $event.target.files[0];
  this.formData.append(img, imageFile); 
  
}


    
postUsers(){
  
  this.formData.append('name',this.bcmain.name);
  this.formData.append('swimmingName',this.bcmain.swimmingName);
  this.formData.append('aboutUseDes1',this.bcmain.aboutUseDes1);
  this.formData.append('aboutUseDes2',this.bcmain.aboutUseDes2);
  this.formData.append('ourTeam',this.bcmain.ourTeam);
  this.formData.append('teamMembersNames',this.bcmain.teamMembersNames);
  this.formData.append('teamDescriptions',this.bcmain.teamDescriptions);
  this.formData.append('day1',this.bcmain.day1);
  this.formData.append('day2',this.bcmain.day2);
  this.formData.append('day3',this.bcmain.day3);
  this.formData.append('day4',this.bcmain.day4);
  this.formData.append('day5',this.bcmain.day5);
  this.formData.append('day6',this.bcmain.day6);
  this.formData.append('day7',this.bcmain.day7);
  this.formData.append('allTimes',this.bcmain.allTimes);
  this.formData.append('houseNo',this.bcmain.houseNo);
  this.formData.append('mainArea',this.selectArea);
  this.formData.append('subArea',this.bcmain.subArea);
  this.formData.append('landmark',this.bcmain.landmark);
  this.formData.append('city',this.bcmain.city);
  this.formData.append('distict',this.locationName);
  this.formData.append('state',this.bcmain.state);
  this.formData.append('pincode',this.bcmain.pincode);
  this.formData.append('officeNo',this.bcmain.officeNo);
  this.formData.append('mobileNo',this.bcmain.mobileNo);
  this.formData.append('whatsupno',this.bcmain.whatsupno);
  this.formData.append('emailId',this.bcmain.emailId);

this.bcmain.name="",
this.bcmain.swimmingName="",
this.bcmain.aboutUseDes1="",
this.bcmain.aboutUseDes2="",
 this.bcmain.day1="",
this.bcmain.day2="",
this.bcmain.day3="",
this.bcmain.day4="",
this.bcmain.day5="",
this.bcmain.day6="",
this.bcmain.day7="",
this.bcmain.allTimes="",
this.bcmain.houseNo="",
this.bcmain.subArea="",
this.bcmain.landmark="",
this.bcmain.city="",
 this.bcmain.state="",
this.bcmain.pincode="",
this.bcmain.officeNo="",
this.bcmain.mobileNo="",
this.bcmain.whatsupno="",
this.bcmain.emailId="",
this.bcmain.images1="",
this.bcmain.images2="",
this.bcmain.images3="",
this.bcmain.images4="",
this.bcmain.images5="",
this.bcmain.images6="",

    
      this.swim.areapostusers(this.selectAreaId,this.formData).subscribe((res)=>{
         this.areas.area=""
      }) 
 
}
   

// ********************************************************* Adds Areas One ***********************************************************************

handleFileInputUpdates($event:any,images){
  this.imageAddsOne= File = $event.target.files[0];
 }
 

postAddsOne(){
this.formData.append('addsOneImg',this.imageAddsOne);
this.formData.append('addsOneLink',this.areaAddsOne.addsOneLink); 
 
 this.swim.postAddsOne(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsOne.addsOneLink="",
     this.areaAddsOne.addsOneImg=""
}
babycaressareasAddsOnea(){
  this.swim.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
 
   })
}
babycaressareasAddsOneaDelete(identities){
  let areaIds=identities._id
  this.swim.babycaresAddsOneaDelete(areaIds).subscribe((res)=>{
    
  })
}

// ********************************************************* Adds Areas Two ***********************************************************************

handleFileInputUpdatesTwo($event:any,images){
  this.imageAddsTwo= File = $event.target.files[0];
 }
 

postAddsTwo(){
this.formData.append('addsTwoImg',this.imageAddsTwo);
this.formData.append('addsTwoLink',this.areaAddsTwo.addsTwoLink); 
 
 this.swim.postAddsTwo(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsTwo.addsTwoLink="",
     this.areaAddsTwo.addsTwoImg=""
}
babycaressareasAddsTwoa(){
  this.swim.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
   })
}
babycaressareasAddsTwoaDelete(identities){
  let areaIds=identities._id
  this.swim.babycaresAddsTwoaDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Three ***********************************************************************

handleFileInputUpdatesThree($event:any,images){
  this.imageAddsThree= File = $event.target.files[0];
 }
 

postAddsThree(){
this.formData.append('addsThreeImg',this.imageAddsThree);
this.formData.append('addsThreeLink',this.areaAddsThree.addsThreeLink); 
 
 this.swim.postAddsThree(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsThree.addsThreeLink="",
     this.areaAddsThree.addsThreeImg=""
}
babycaressareasAddsThreea(){
  this.swim.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
   })
}
babycaressareasAddsThreeaDelete(identities){
  let areaIds=identities._id
  this.swim.babycaresAddsThreeaDelete(areaIds).subscribe((res)=>{  
  })
}


// ********************************************************* Adds Areas Four ***********************************************************************

handleFileInputUpdatesFour($event:any,images){
  this.imageAddsFour= File = $event.target.files[0];
 }
 

postAddsFour(){
this.formData.append('addsFourImg',this.imageAddsFour);
this.formData.append('addsFourLink',this.areaAddsFour.addsFourLink); 
 
 this.swim.postAddsFour(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsFour.addsFourLink="",
     this.areaAddsFour.addsFourImg=""
}
babycaressareasAddsFoura(){
  this.swim.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res
   })
}
babycaressareasAddsFouraDelete(identities){
  let areaIds=identities._id
  this.swim.babycaresAddsFouraDelete(areaIds).subscribe((res)=>{
  })
}

 

 


// ********************************************************* Adds Areas One ***********************************************************************

handleFileInputUpdatesOneLoc($event:any,images){
  this.imageAddsOneLoc= File = $event.target.files[0];
 }
 

postAddsOneLoc(){
this.formData.append('addsOneImgLoc',this.imageAddsOneLoc);
this.formData.append('addsOneLinkLoc',this.areaAddsOneLoc.addsOneLinkLoc); 
 
 this.swim.postAddsOnepl(this.its,this.formData).subscribe((res)=>{
    
})
     this.areaAddsOneLoc.addsOneImgLoc="",
     this.areaAddsOneLoc.addsOneLinkLoc=""
}
babycaressareasAddsOneLoc(){
  this.swim.babycaresAddsOnel(this.its).subscribe((res)=>{
     this.babycaresAddsOneLocl=res
   })
}
babycaressareasAddsOneLocDelete(identities){
  let areaIds=identities._id
  this.swim.babycaresAddsOneLocDelete(areaIds).subscribe((res)=>{
    
  })
}

// ********************************************************* Adds Areas Two ***********************************************************************

handleFileInputUpdatesTwoLoc($event:any,images){
  this.imageAddsTwoLoc= File = $event.target.files[0];
 }
 

postAddsTwoLoc(){
this.formData.append('addsTwoImgLoc',this.imageAddsTwoLoc);
this.formData.append('addsTwoLinkLoc',this.areaAddsTwoLoc.addsTwoLinkLoc); 
 
 this.swim.postAddsTwopl(this.its,this.formData).subscribe((res)=>{
     })
     this.areaAddsTwoLoc.addsTwoLinkLoc="",
     this.areaAddsTwoLoc.addsTwoImgLoc=""
}
babycaressareasAddsTwoLoc(){
  this.swim.babycaresAddsTwol(this.its).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
   })
}
babycaressareasAddsTwoLocDelete(identities){
  let areaIds=identities._id
  this.swim.babycaresAddsTwoLocDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Three Loc***********************************************************************

handleFileInputUpdatesThreeLoc($event:any,images){
  this.imageAddsThreeLoc= File = $event.target.files[0];
 }
 

postAddsThreeLoc(){
this.formData.append('addsThreeImgLoc',this.imageAddsThreeLoc);
this.formData.append('addsThreeLinkLoc',this.areaAddsThreeLoc.addsThreeLinkLoc); 
 
 this.swim.postAddsThreepl(this.its,this.formData).subscribe((res)=>{
     })
     this.areaAddsThreeLoc.addsThreeImgLoc="",
     this.areaAddsThreeLoc.addsThreeLinkLoc=""
}
babycaressareasAddsThreeLoc(){
  this.swim.babycaresAddsThreel(this.its).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
   })
}
babycaressareasAddsThreeLocDelete(identities){
  let areaIds=identities._id
  this.swim.babycaresAddsThreeLocDelete(areaIds).subscribe((res)=>{
  
  })
}


// ********************************************************* Adds Areas Four Loc***********************************************************************

handleFileInputUpdatesFourLoc($event:any,images){
  this.imageAddsFourLoc= File = $event.target.files[0];
 }
 

postAddsFourLoc(){
this.formData.append('addsFourImgLoc',this.imageAddsFourLoc);
this.formData.append('addsFourLinkLoc',this.areaAddsFourLoc.addsFourLinkLoc); 
 
 this.swim.postAddsFourpl(this.its,this.formData).subscribe((res)=>{
     })
     this.areaAddsFourLoc.addsFourImgLoc="",
     this.areaAddsFourLoc.addsFourLinkLoc=""
}
babycaressareasAddsFourLoc(){
  this.swim.babycaresAddsFourl(this.its).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
   })
}
babycaressareasAddsFourLocDelete(identities){
  let areaIds=identities._id
  this.swim.babycaresAddsFourLocDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Five Loc***********************************************************************

handleFileInputUpdatesFiveLoc($event:any,images){
  this.imageAddsFiveLoc= File = $event.target.files[0];
 }
 

postAddsFiveLoc(){
this.formData.append('addsFiveImgLoc',this.imageAddsFiveLoc);
this.formData.append('addsFiveLinkLoc',this.areaAddsFiveLoc.addsFiveLinkLoc); 
 
 this.swim.postAddsFivepl(this.its,this.formData).subscribe((res)=>{
     })
     this.areaAddsFiveLoc.addsFiveImgLoc="",
     this.areaAddsFiveLoc.addsFiveLinkLoc=""
}
babycaressareasAddsFiveLoc(){
  this.swim.babycaresAddsFivel(this.its).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
   })
}
babycaressareasAddsFiveLocDelete(identities){
  let areaIds=identities._id
  this.swim.babycaresAddsFiveLocDelete(areaIds).subscribe((res)=>{
    
  })
}

 
}




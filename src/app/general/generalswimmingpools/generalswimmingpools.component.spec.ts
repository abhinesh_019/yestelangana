import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralswimmingpoolsComponent } from './generalswimmingpools.component';

describe('GeneralswimmingpoolsComponent', () => {
  let component: GeneralswimmingpoolsComponent;
  let fixture: ComponentFixture<GeneralswimmingpoolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralswimmingpoolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralswimmingpoolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment';

@Injectable()
export class FurnituresService {

  constructor(private http: HttpClient) { }

  locationspost(data){
    return   this.http.post(environment.apiUrl +"/furnitureslocations",data);
   }
   areapost(id,data){
    return   this.http.post(environment.apiUrl +"/furnituresAreas/"+id,data);
   }
   locationapi(){
    
    return this.http.get(environment.apiUrl +"/furnitureslocations");
  }
   
   

    postFiles(id,data) {
      return   this.http.post(environment.apiUrl +"/furnituresuserspostsdata/"+id,data);
 }
    
  

      areaapi(id){
    
        return this.http.get(environment.apiUrl +"/furnituresAreas/"+id);
      }
     
      babycaresAddsOnea(id){

        return this.http.get(environment.apiUrl +"/furnituresAddsOneGeta/"+id);
      }
      babycaresAddsTwoa(id){
        
        return this.http.get(environment.apiUrl +"/furnituresAddsTwoGeta/"+id);
      }
      babycaresAddsThreea(id){
        
        return this.http.get(environment.apiUrl +"/furnituresAddsThreeGeta/"+id);
      }
      babycaresAddsFoura(id){
        
        return this.http.get(environment.apiUrl +"/furnituresAddsFourGeta/"+id);
      }
       
      babycaresAddsOnel(id){
        
        return this.http.get(environment.apiUrl +"/furnituresAddsOneLocGet/"+id);
      }
      babycaresAddsTwol(id){
        
        return this.http.get(environment.apiUrl +"/furnituresAddsTwoLocGet/"+id);
      }
      babycaresAddsThreel(id){
        
        return this.http.get(environment.apiUrl +"/furnituresAddsThreeGetLoc/"+id);
      }
      babycaresAddsFourl(id){
        
        return this.http.get(environment.apiUrl +"/furnituresAddsFourLocGet/"+id);
      }
    
      babycaresAddsFivel(id){
        
        return this.http.get(environment.apiUrl +"/furnituresAddsFiveLocGet/"+id);
      }
       
      postAddsOne(id,data){
        return   this.http.post(environment.apiUrl +"/furnituresAddsOnePtsa/"+id,data);
       }
       postAddsTwo(id,data){
        return   this.http.post(environment.apiUrl +"/furnituresAddsTwoPtsa/"+id,data);
       }
       postAddsThree(id,data){
        return   this.http.post(environment.apiUrl +"/furnituresAddsThreePtsa/"+id,data);
       }
       postAddsFour(id,data){
        return   this.http.post(environment.apiUrl +"/furnituresAddsFourPtsa/"+id,data);
       }
       
    
    
      postAddsOnepl(id,data){
        return   this.http.post(environment.apiUrl +"/furnituresAddsOneLocPts/"+id,data);
       }
       postAddsTwopl(id,data){
        return   this.http.post(environment.apiUrl +"/furnituresAddsTwoLocPts/"+id,data);
       }
       postAddsThreepl(id,data){
        return   this.http.post(environment.apiUrl +"/furnituresAddsThreePtsLoc/"+id,data);
       }
       postAddsFourpl(id,data){
        return   this.http.post(environment.apiUrl +"/furnituresAddsFourLocPts/"+id,data);
       }
       postAddsFivepl(id,data){
        return   this.http.post(environment.apiUrl +"/furnituresAddsFiveLocPts/"+id,data);
       }
       
    
    
       babycaresAddsOneaDelete(id){
    
        return this.http.delete(environment.apiUrl +"/furnituresAddsOneDeletea/"+id);
      }
     
      babycaresAddsTwoaDelete(id){
    
        return this.http.delete(environment.apiUrl +"/furnituresAddsTwoDeletea/"+id);
      }
      babycaresAddsThreeaDelete(id){
    
        return this.http.delete(environment.apiUrl +"/furnituresAddsThreeDeletea/"+id);
      }
      babycaresAddsFouraDelete(id){
    
        return this.http.delete(environment.apiUrl +"/furnituresAddsFourDeletea/"+id);
      }
    
    
       babycaresAddsOneLocDelete(id){
    
        return this.http.delete(environment.apiUrl +"/furnituresAddsOneLocDelete/"+id);
      }
    
      babycaresAddsTwoLocDelete(id){
    
        return this.http.delete(environment.apiUrl +"/furnituresAddsTwoLocDelete/"+id);
      }
      babycaresAddsThreeLocDelete(id){
    
        return this.http.delete(environment.apiUrl +"/furnituresAddsThreeDeleteLoc/"+id);
      }
      babycaresAddsFourLocDelete(id){
    
        return this.http.delete(environment.apiUrl +"/furnituresAddsFourLocDelete/"+id);
      }
      babycaresAddsFiveLocDelete(id){
    
        return this.http.delete(environment.apiUrl +"/furnituresAddsFiveLocDelete/"+id);
      }
       
     }
    

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()


export class OutdoorgamesService {
  constructor(private http: HttpClient) { }

  locationapi(){
    
    return this.http.get(environment.apiUrl +"/outdoorgamesLocations");
  }

  areaapi(id){
    
    return this.http.get(environment.apiUrl +"/outdoorareas/"+id);
  }
  outdoorUsers(id,data){
    
    return this.http.get(environment.apiUrl +"/outdoorgamesClientsget/"+id,{params:data});
  }
  outdoorUserscountsAll(){
    
    return this.http.get(environment.apiUrl +"/outdoorgamesClientsAllCount");
  }

  outdoorUserscountAllMainAreas(id){
    
    return this.http.get(environment.apiUrl +"/outdoorgamesClientsCountAreas/"+id);
  }

  outdoorUserscountAllDist(){
    
    return this.http.get(environment.apiUrl +"/outdoorgamesClientsCountDist");
  }

  outdoorUserscountAllAreas(id){
    
    return this.http.get(environment.apiUrl +"/outdoorgamesClientsCount/"+id);
  }
  outdoorUserscountAllAreasover(id){
    
    return this.http.get(environment.apiUrl +"/outdoorgamesClientsAreas/"+id);
  }

  babycaresAddsOnea(id){

    return this.http.get(environment.apiUrl +"/outdoorGamesAddsOneGeta/"+id);
  }
  babycaresAddsTwoa(id){
    
    return this.http.get(environment.apiUrl +"/outdoorGamesAddsTwoGeta/"+id);
  }
  babycaresAddsThreea(id){
    
    return this.http.get(environment.apiUrl +"/outdoorGamesAddsThreeGeta/"+id);
  }
  babycaresAddsFoura(id){
    
    return this.http.get(environment.apiUrl +"/outdoorGamesAddsFourGeta/"+id);
  }
   
  babycaresAddsOnel(id){
    
    return this.http.get(environment.apiUrl +"/outdoorGamesAddsOneLocGet/"+id);
  }
  babycaresAddsTwol(id){
    
    return this.http.get(environment.apiUrl +"/outdoorGamesAddsTwoLocGet/"+id);
  }
  babycaresAddsThreel(id){
    
    return this.http.get(environment.apiUrl +"/outdoorGamesAddsThreeGetLoc/"+id);
  }
  babycaresAddsFourl(id){
    
    return this.http.get(environment.apiUrl +"/outdoorGamesAddsFourLocGet/"+id);
  }

  babycaresAddsFivel(id){
    
    return this.http.get(environment.apiUrl +"/outdoorGamesAddsFiveLocGet/"+id);
  }
}
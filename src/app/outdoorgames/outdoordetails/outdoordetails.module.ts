import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { OutdoordetailsComponent } from './outdoordetails.component';
import { OutdoordetailsService } from './outdoordetails.service';
 
const routes:Routes=[{path:'outdoorgamesdetails/:_id/:name',component:OutdoordetailsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    OutdoordetailsComponent,

  ],
  providers: [OutdoordetailsService],
})
export class OutdoordetailsModule { }

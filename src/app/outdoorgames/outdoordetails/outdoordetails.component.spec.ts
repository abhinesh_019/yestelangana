import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutdoordetailsComponent } from './outdoordetails.component';

describe('OutdoordetailsComponent', () => {
  let component: OutdoordetailsComponent;
  let fixture: ComponentFixture<OutdoordetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutdoordetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutdoordetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

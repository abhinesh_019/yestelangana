import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TutionsComponent } from './tutions.component';

describe('TutionsComponent', () => {
  let component: TutionsComponent;
  let fixture: ComponentFixture<TutionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TutionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TutionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

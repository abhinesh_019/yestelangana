import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { TutionsdetailsComponent } from './tutionsdetails.component';
import { TutionsService } from '../tutions.service';



const routes:Routes=[{path:'tutionsdetails/:_id/:name',component:TutionsdetailsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    TutionsdetailsComponent,

  ],
  providers: [TutionsService],
})
export class TutionsdetailsModule { }

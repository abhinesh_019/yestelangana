import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment.prod';

const httpOptions={
  headers:new HttpHeaders({'Content-Type':'application/json'})
};
@Injectable()
export class BabycareService {
 
  constructor(private http: HttpClient) { }
  locationapi( ){
    return   this.http.get(environment.apiUrl +"/babycarelocations");
   }
   babycare(id,data) {
    return this.http.get(environment.apiUrl + "/babycare/"+id,{params:data});
  }
  babycares() {
    return this.http.get(environment.apiUrl + "/babycares");
  }
  babycaredist() {
    return this.http.get(environment.apiUrl + "/babycaredist");
  }
  getbabycares(id) {
    return this.http.get(environment.apiUrl + "/babycares/"+id);
  }
  areaapi(id){
    return   this.http.get(environment.apiUrl +"/babycareArea/"+id);
   }
   
  overallscommentspost(id,data){
     
    
    return this.http.post(environment.apiUrl +"/babycarecomments/"+id,data);
    
  }
  overallscommentspostcounts(id){
    
    return this.http.get(environment.apiUrl +"/babycarecommentscounts/"+id);
  }
  commentsget(id) {
    return this.http.get(environment.apiUrl + "/updates/"+id);
  }
  
  upadtescounts(id){
    return this.http.get(environment.apiUrl + "/updatescounts/"+id);
  }
  
  commentspost(id,data) {
    
 return this.http.post(environment.apiUrl + "/updatesuserscomments/"+id,data);

};
updatesCommmentsCount(id){
  return this.http.get(environment.apiUrl +"/updatesuserscommentscount/"+id);

}
updatesComments(id) {
    
  return this.http.get(environment.apiUrl + "/updatesuserscomments/"+id);
 
 };
 updatesCommentscount(id) {
    
  return this.http.get(environment.apiUrl + "/updatesuserscommentscount/"+id);
 
 };
 updatesreplys(id) {
    
  return this.http.get(environment.apiUrl + "/updatesuserscommentsreply/"+id);
 
 };
 updatesCommmentsallCount(id){
  return this.http.get(environment.apiUrl +"/updatescounts/"+id);

}
servicesOne(id){
  return this.http.get(environment.apiUrl +"/babyCareServices/"+id);

}

servicesTwo(id){
  return this.http.get(environment.apiUrl +"/babyCareServicesTwoGet/"+id);

}
fecilitiesGet(id){
  return this.http.get(environment.apiUrl + "/babyCarefecitiesGet/"+id);
   }
   fecilitiesGetc(id){
    return this.http.get(environment.apiUrl + "/babyCarefecitiesGetCounts/"+id);
     }

     polularLocation() {
      return this.http.get(environment.apiUrl + "/babycaresLocationCounts");
    }
    areaCount(id){
      return this.http.get(environment.apiUrl + "/babycareCounts/"+id);
       }
       babycaresAddsOnea(id){

        return this.http.get(environment.apiUrl +"/babycaresAddsOneGeta/"+id);
      }
      babycaresAddsTwoa(id){
        
        return this.http.get(environment.apiUrl +"/babycaresAddsTwoGeta/"+id);
      }
      babycaresAddsThreea(id){
        
        return this.http.get(environment.apiUrl +"/babycaresAddsThreeGeta/"+id);
      }
      babycaresAddsFoura(id){
        
        return this.http.get(environment.apiUrl +"/babycaresAddsFourGeta/"+id);
      }
       
      babycaresAddsOnel(id){
        
        return this.http.get(environment.apiUrl +"/babycaresAddsOneLocGet/"+id);
      }
      babycaresAddsTwol(id){
        
        return this.http.get(environment.apiUrl +"/babycaresAddsTwoLocGet/"+id);
      }
      babycaresAddsThreel(id){
        
        return this.http.get(environment.apiUrl +"/babycaresAddsThreeGetLoc/"+id);
      }
      babycaresAddsFourl(id){
        
        return this.http.get(environment.apiUrl +"/babycaresAddsFourLocGet/"+id);
      }
    
      babycaresAddsFivel(id){
        
        return this.http.get(environment.apiUrl +"/babycaresAddsFiveLocGet/"+id);
      }
       
}

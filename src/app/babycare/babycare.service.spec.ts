import { TestBed, inject } from '@angular/core/testing';

import { BabycareService } from './babycare.service';

describe('BabycareService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BabycareService]
    });
  });

  it('should be created', inject([BabycareService], (service: BabycareService) => {
    expect(service).toBeTruthy();
  }));
});

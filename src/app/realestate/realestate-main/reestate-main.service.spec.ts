import { TestBed, inject } from '@angular/core/testing';

import { ReestateMainService } from './reestate-main.service';

describe('ReestateMainService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReestateMainService]
    });
  });

  it('should be created', inject([ReestateMainService], (service: ReestateMainService) => {
    expect(service).toBeTruthy();
  }));
});

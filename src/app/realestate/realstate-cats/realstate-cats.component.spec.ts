import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealstateCatsComponent } from './realstate-cats.component';

describe('RealstateCatsComponent', () => {
  let component: RealstateCatsComponent;
  let fixture: ComponentFixture<RealstateCatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealstateCatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealstateCatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { TestBed, inject } from '@angular/core/testing';

import { FightusersService } from './fightusers.service';

describe('FightusersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FightusersService]
    });
  });

  it('should be created', inject([FightusersService], (service: FightusersService) => {
    expect(service).toBeTruthy();
  }));
});

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FightDeatilsService } from './fight-deatils.service';

@Component({
  selector: 'app-fight-details',
  templateUrl: './fight-details.component.html',
  styleUrls: ['./fight-details.component.css']
})
export class FightDetailsComponent implements OnInit {

  public musicDetailsClients:any
  public ids:any
  public fightServicesGet:any
  public fightServicesGetc:any
  public fecilitiesGet:any
  public fecilitiesGetc:any
  public openshareid:any
public commentspostsId:any
public viewcommentsid:any
public clientUpdates:any
public replyid:any
public clientUpdatesCount:any
 public getUsersComments:any
public getUsersCommentsCounts:any
public share:boolean=false
public comments:boolean=false
public viewcomments:boolean=false
public replyfrom:boolean=false 
public replies:any


postComments={
  "descriptions":""
}
commentspost={
  "name":"",
  "email":"",
  "contactNo":"",
  "leaveComment":"",
    }
   
  constructor(private router:Router,private route:ActivatedRoute,private fight:FightDeatilsService) { }
   ngOnInit() {
    this.individualdata()
    this.usersDetails()
   this.fightServices()
   this.fightServicesc()
   this.fecilities()
   this.fecilitiesc()
   this.clientsUpdates()
   this.clientsUpdatesCount()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];

}

usersDetails(){ 
this.fight.usersDetails(this.ids).subscribe((res)=>{
this.musicDetailsClients=res
 
})
}
fightServices(){ 
  this.fight.fightServices(this.ids).subscribe((res)=>{
  this.fightServicesGet=res
   
  })
  }

  fightServicesc(){ 
    this.fight.fightServicesc(this.ids).subscribe((res)=>{
    this.fightServicesGetc=res
     })
    }
    fecilities(){ 
      this.fight.fecilities(this.ids).subscribe((res)=>{
      this.fecilitiesGet=res
      
      })
      }
  
      fecilitiesc(){ 
        this.fight.fecilitiesc(this.ids).subscribe((res)=>{
        this.fecilitiesGetc=res
         })
        }


// ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
   console.log(this.replyid);

   
}
 clientsUpdates(){   
  this.fight.clientsUpdatesGet(this.ids).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.fight.clientsUpdatesGetCounts(this.ids).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.fight.commentsPoststoClints(this.commentspostsId,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.fight.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.fight.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.fight.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
      console.log(res);
      
    })
 }


  // ***********0verallcomments************
  overallcomment(){
       
     
    this.fight.overallscommentspost(this.ids,this.commentspost).subscribe((res)=>{
     
     this.commentspost.name="",
     this.commentspost.contactNo="",
     this.commentspost.email="",
     this.commentspost.leaveComment=""
    })
  }


}

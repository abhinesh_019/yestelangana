import { TestBed, inject } from '@angular/core/testing';

import { FightDeatilsService } from './fight-deatils.service';

describe('FightDeatilsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FightDeatilsService]
    });
  });

  it('should be created', inject([FightDeatilsService], (service: FightDeatilsService) => {
    expect(service).toBeTruthy();
  }));
});

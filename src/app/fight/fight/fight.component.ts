import { Component, OnInit } from '@angular/core';
import { FightService } from './fight.service';

@Component({
  selector: 'app-fight',
  templateUrl: './fight.component.html',
  styleUrls: ['./fight.component.css']
})
export class FightComponent implements OnInit {

  public catList:any
  
  constructor(private fights:FightService) { }
   

  ngOnInit() {
    this.getcatagories()
    
  }
  
  getcatagories(){
    this.fights.catapigetdetails().subscribe((res)=>{
  this.catList=res
  console.log(this.catList);
   
          })  }
         
     
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfessionalInstiutionsDetailsModulesService } from './professional-instiutions-details-modules.service';

@Component({
  selector: 'app-professional-instiutions-details',
  templateUrl: './professional-instiutions-details.component.html',
  styleUrls: ['./professional-instiutions-details.component.css']
})
export class ProfessionalInstiutionsDetailsComponent implements OnInit {
  public ids:any;
  public proDetailsClients:any;
  public aboutUsDetai:any;
  public getPhilosophyd:any;
  public getWhyInst:any
  public getinfraStructures:any
  public getAwardsAndReward:any
  public getboardsDirector:any
  public getcatageriesOnesMain:any
  public catOneName:any
  public catOneNameId:any
  public getcatageriesTwos:any
  public catTwoNameId:any
  public getTrackRecordsYearMain:any
  public trackrecordsNameMain:any
  public trackrecordsIdMain:any
  public getcatageriesTwosMain:any
  public catTwoNameMains:any
  public catTwoNameIdMains:any
  public catOneNameMain:any
  public catOneNameIdMain:any
  public getBrancheMain:any
  public branchNameMain:any
  public branchNameIdMain:any
  public syllabusDetailsGetsMain:any
  public syllabusDetailsNameMain:any
  public syllabusDetailsNameIdMain:any
  public syllabusTwoDetailsGetsMain:any
  public syllabusTwoDetailsNameMain:any
  public syllabusTwoDetailsNameIdMain:any
  public elegibilitiesGetsMain:any
  public organizationsGetsMain:any
  public organizationsNameMain:any
  public organizationsNameIdMain:any
  public importantNotificationsGetsMain:any
  public importantNotificationsGetsMainc:any
  public importantNotificationsGetsMainNameMain:any
  public importantNotificationsGetsMainNameId:any
  public getMaterialMain:any
  public materialsMainNameMains:any
  public materialsMainNameIdMains:any
  public getnewBatchMains:any
  public newBatchNameMains:any
  public newBatchNameIdMains:any
  public admissionsGetsMain:any
  public admissionsGetsMainName:any
  public admissionsGetsMainNameId:any
  public freeStructureGetsMain:any
  public freeStructureGetsMainName:any
  public freeStructureGetsMainNameId:any
  public batchesGetsMain:any
  public batchesGetsMainName:any
  public batchesGetsMainNameId:any
  public getfacultyMains:any
  public gettestAnaysisMains:any
  public computerLabMains:any
  public libraryOneGetsMain:any
  public libraryOneGetsMainName:any
  public libraryOneGetsMainNameId:any
  public libraryTwoGetsMain:any
  public libraryTwoGetsMainName:any
  public libraryTwoGetsMainNameId:any
  public computerLabThreeMains:any
  public trackRecordsMains:any
  // public getnewBatchMains:any
  // public getnewBatchMains:any
  // public getnewBatchMains:any
  // public getnewBatchMains:any
  // public getnewBatchMains:any
  // public getnewBatchMains:any
  public openshareid:any
  public commentspostsId:any
  public viewcommentsid:any
  public clientUpdates:any
  public replyid:any
  public clientUpdatesCount:any
   public getUsersComments:any
  public getUsersCommentsCounts:any
  public share:boolean=false
  public comments:boolean=false
  public viewcomments:boolean=false
  public replyfrom:boolean=false 
  public replies:any
  postComments={
    "descriptions":""
  }
  commentspost={
    "name":"",
    "email":"",
    "contactNo":"",
    "leaveComment":"",
      }
  
  
  constructor(private router:Router,private route:ActivatedRoute,private pro:ProfessionalInstiutionsDetailsModulesService) { }

  ngOnInit() {
    this.individualdata()
    this.usersDetails()
    this.aboutUs()
    this.getPhilosophy()
    this.getWhyInsts()
    this.getinfraStructure()
    this.getAwardsAndRewards()
    this.getboardsDirectors()
    this.getcatageriesOneMain()
    this.clientsUpdates()
    this.clientsUpdatesCount()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];

}

usersDetails(){ 
  this.pro.usersDetails(this.ids).subscribe((res)=>{
  this.proDetailsClients=res
  console.log(res);
  
  })
  }
  aboutUs(){ 
    this.pro.aboutUsDetais(this.ids).subscribe((res)=>{
    this.aboutUsDetai=res
     
    })
    }
    getPhilosophy(){ 
      this.pro.getPhilosophys(this.ids).subscribe((res)=>{
      this.getPhilosophyd=res
      })
      }
      getWhyInsts(){ 
        this.pro.getWhyInsts(this.ids).subscribe((res)=>{
        this.getWhyInst=res
        })
        }
        getinfraStructure(){ 
          this.pro.getinfraStructure(this.ids).subscribe((res)=>{
          this.getinfraStructures=res
          })
          }
          getAwardsAndRewards(){ 
            this.pro.getAwardsAndRewards(this.ids).subscribe((res)=>{
            this.getAwardsAndReward=res
            })
            }
            getboardsDirectors(){ 
              this.pro.getboardsDirector(this.ids).subscribe((res)=>{
              this.getboardsDirector=res
              })
              }

              getcatageriesOneMain(){ 
                this.pro.getcatageriesOne(this.ids).subscribe((res)=>{
                this.getcatageriesOnesMain=res
                 })
                }
                       catOneMain(get?){
                        this.catOneNameMain=get.courseName
                        this.catOneNameIdMain=get._id
                        console.log(this.catOneNameIdMain);
                          this.getcatageriesTwoMain()
                                    }
                                 

                              getcatageriesTwoMain(){ 
                                this.pro.getcatageriesTwo(this.catOneNameIdMain).subscribe((res)=>{
                                this.getcatageriesTwosMain=res
                                var serTwoMain=this.getcatageriesTwosMain[0]
                                this.catTwoMain(serTwoMain)
                                  })
                                }
                                catTwoMain(get?){
                                  this.catTwoNameMains=get.courseName
                                  this.catTwoNameIdMains=get._id
                                  console.log(this.catTwoNameIdMains);
                                  
                                  this.getTrackRecordsYearsMain()
                                             }
                              getTrackRecordsYearsMain(){ 
                                this.pro.getTrackRecordsYears(this.catTwoNameIdMains).subscribe((res)=>{
                                this.getTrackRecordsYearMain=res
                                console.log(res);
                                
                                var yearsMain=this.getTrackRecordsYearMain[0]
                    this.yearsClickMain(yearsMain)
                                })
                                }
                    
                                yearsClickMain(get?){
                        this.trackrecordsNameMain=get.yearOfSelection
                        this.trackrecordsIdMain=get._id
                        this.getBranchesMain()
                                    }

                                    getBranchesMain(){ 
                                      this.pro.getBranches(this.trackrecordsIdMain).subscribe((res)=>{
                                      this.getBrancheMain=res
                                      console.log(res);
                                      var branchMain=this.getBrancheMain[0]
                                  this.branchClickMain(branchMain)
                                      })
                                      }
                                  
                                      branchClickMain(get?){
                                  this.branchNameMain=get.branchName
                                  this.branchNameIdMain=get._id
                                  this.syllabusDetailsGetMain()
                                  this.elegibilitiesGetClickGetMain()
                                  this.organizationsGetClickGetMain()
                                  this.importantNotificationGetMain()
                                  this.importantNotificationGetMainC()
                                  this.getMaterialsMain()
                                  this.getnewBatchMain()
                                  this.admissionsGetMain()
                                  this.freeStructureGetMain()
                                  this.batchesGetMain()
                                    }
                                    syllabusDetailsGetMain(){ 
                                      this.pro.syllabusDetailsGet(this.branchNameIdMain).subscribe((res)=>{
                                      this.syllabusDetailsGetsMain=res
                                      var syllOnes=this.syllabusDetailsGetsMain[0]
                                      this.syllabusDetailsMainClick(syllOnes)
                                      })
                                      }
                                      syllabusDetailsMainClick(get?){
                                        this.syllabusDetailsNameMain=get.subTitle
                                        this.syllabusDetailsNameIdMain=get._id
                                        this.syllabusTwoDetailsGetMain()
                                           }
                                    syllabusTwoDetailsGetMain(){ 
                                      this.pro.syllabusTwoDetailsGet(this.syllabusDetailsNameIdMain).subscribe((res)=>{
                                      this.syllabusTwoDetailsGetsMain=res
                                      var syllOnes=this.syllabusTwoDetailsGetsMain[0]
                                      this.syllabusTwoDetailsMainClick(syllOnes)
                                      })
                                      }
                                      syllabusTwoDetailsMainClick(get?){
                                        this.syllabusTwoDetailsNameMain=get.subTitle
                                        this.syllabusTwoDetailsNameIdMain=get._id
                                           }

                                           elegibilitiesGetClickGetMain(){ 
                                            this.pro.elegibilitiesGet(this.branchNameIdMain).subscribe((res)=>{
                                            this.elegibilitiesGetsMain=res
                                            var syllOnes=this.elegibilitiesGetsMain[0]
                                            this.elegibilitiesTwoDetailsMainClick(syllOnes)
                                            })
                                            }
                                            elegibilitiesTwoDetailsMainClick(get?){
                                              this.syllabusTwoDetailsNameMain=get.subTitle
                                              this.syllabusTwoDetailsNameIdMain=get._id
                                                 }

                                                 organizationsGetClickGetMain(){ 
                                                  this.pro.organizationsGets(this.branchNameIdMain).subscribe((res)=>{
                                                  this.organizationsGetsMain=res
                                                  var syllOnes=this.organizationsGetsMain[0]
                                                  this.elegibilitiesTwoDetailsMainClick(syllOnes)
                                                  })
                                                  }
                                                  organizationsMainClick(get?){
                                                    this.organizationsNameMain=get.subTitle
                                                    this.organizationsNameIdMain=get._id
                                                       }

                                                       importantNotificationGetMain(){ 
                                                        this.pro.importantNotificationsGets(this.branchNameIdMain).subscribe((res)=>{
                                                        this.importantNotificationsGetsMain=res
                                                        var syllOnes=this.importantNotificationsGetsMain[0]
                                                        this.importantNotificationsMainClick(syllOnes)
                                                        })
                                                        }
                                                        importantNotificationGetMainC(){ 
                                                          this.pro.importantNotificationsGetsc(this.branchNameIdMain).subscribe((res)=>{
                                                          this.importantNotificationsGetsMainc=res
                                                          
                                                          })
                                                          }
                                                        importantNotificationsMainClick(get?){
                                                          this.importantNotificationsGetsMainNameMain=get.subTitle
                                                          this.importantNotificationsGetsMainNameId=get._id
                                                             }

                                                             getMaterialsMain(){ 
                                                              this.pro.getmaterials(this.branchNameIdMain).subscribe((res)=>{
                                                              this.getMaterialMain=res
                                                              var materials=this.getMaterialMain[0]
                                                              this.materialsMain(materials)
                                                                })
                                                              }
                                                              materialsMain(get?){
                                                                this.materialsMainNameMains=get.mainTitle
                                                                this.materialsMainNameIdMains=get._id
                                                                            }

                                                                            getnewBatchMain(){ 
                                                                              this.pro.newBatchGets(this.branchNameIdMain).subscribe((res)=>{
                                                                              this.getnewBatchMains=res
                                                                              var newBatch=this.getnewBatchMains[0]
                                                                              this.newBatchMain(newBatch)
                                                                                })
                                                                              }
                                                                              newBatchMain(get?){
                                                                                this.newBatchNameMains=get.mainTitle
                                                                                this.newBatchNameIdMains=get._id
                                                                                            }
                                                                                            admissionsGetMain(){ 
                                                                                              this.pro.admissionsGet(this.branchNameIdMain).subscribe((res)=>{
                                                                                              this.admissionsGetsMain=res
                                                                                              var syllOnes=this.admissionsGetsMain[0]
                                                                                              this.admissionsMainClick(syllOnes)
                                                                                              })
                                                                                              }
                                                                                              
                                                                                              admissionsMainClick(get?){
                                                                                                this.admissionsGetsMainName=get.admissionProcessType
                                                                                                this.admissionsGetsMainNameId=get._id
                                                                                                   }

                                                                                                   freeStructureGetMain(){ 
                                                                                                    this.pro.freeStructureGet(this.branchNameIdMain).subscribe((res)=>{
                                                                                                    this.freeStructureGetsMain=res
                                                                                                    var freeStr=this.freeStructureGetsMain[0]
                                                                                                    this.freeStructureMainClick(freeStr)
                                                                                                    })
                                                                                                    }
                                                                                                    
                                                                                                    freeStructureMainClick(get?){
                                                                                                      this.freeStructureGetsMainName=get.batchName
                                                                                                      this.freeStructureGetsMainNameId=get._id
                                                                                                         }

                                                                                                         batchesGetMain(){ 
                                                                                                          this.pro.batchesGet(this.branchNameIdMain).subscribe((res)=>{
                                                                                                          this.batchesGetsMain=res
                                                                                                          var freeStr=this.batchesGetsMain[0]
                                                                                                          this.batchesMainClick(freeStr)
                                                                                                          })
                                                                                                          }
                                                                                                          
                                                                                                          batchesMainClick(get?){
                                                                                                            this.batchesGetsMainName=get.batchName
                                                                                                            this.batchesGetsMainNameId=get._id
                                                                                                            this.getfacultyMain()
                                                                                                            this.gettestAnaysisMain()
                                                                                                            this.computerLabMain()
                                                                                                            this.libraryOneGetMain()
                                                                                                           this.trackRecordsGetMain()
                                                                                                               }
                                                                                                               getfacultyMain(){ 
                                                                                                                this.pro.facultyGets(this.batchesGetsMainNameId).subscribe((res)=>{
                                                                                                                this.getfacultyMains=res
                                                                                                                   })
                                                                                                                }
                                                                                                                gettestAnaysisMain(){ 
                                                                                                                  this.pro.testAnaysisGets(this.batchesGetsMainNameId).subscribe((res)=>{
                                                                                                                  this.gettestAnaysisMains=res
                                                                                                                  console.log(res);
                                                                                                                    })
                                                                                                                  }

                                                                                                                  computerLabMain(){ 
                                                                                                                    this.pro.computerLabGets(this.batchesGetsMainNameId).subscribe((res)=>{
                                                                                                                    this.computerLabMains=res
                                                                                                                    console.log(res);
                                                                                                                      })
                                                                                                                    }
                                                                                                                    libraryOneGetMain(){ 
                                                                                                                      this.pro.libraryOneGet(this.batchesGetsMainNameId).subscribe((res)=>{
                                                                                                                      this.libraryOneGetsMain=res
                                                                                                                      var library=this.libraryOneGetsMain[0]
                                                                                                                      this.libraryOneMainClick(library)
                                                                                                                      })
                                                                                                                      }
                                                                                                                      
                                                                                                                      libraryOneMainClick(get?){
                                                                                                                        this.libraryOneGetsMainName=get.libraryQualification
                                                                                                                        this.libraryOneGetsMainNameId=get._id
                                                                                                                      this.libraryTwoGetMain()
                                                                                                                           }
                                                                                                                           libraryTwoGetMain(){ 
                                                                                                                            this.pro.libraryTwoGet(this.libraryOneGetsMainNameId).subscribe((res)=>{
                                                                                                                            this.libraryTwoGetsMain=res
                                                                                                                            var libraryT=this.libraryTwoGetsMain[0]
                                                                                                                            this.libraryTwoMainClick(libraryT)
                                                                                                                            })
                                                                                                                            }
                                                                                                                            
                                                                                                                            libraryTwoMainClick(get?){
                                                                                                                              this.libraryTwoGetsMainName=get.libraryDepartment
                                                                                                                              this.libraryTwoGetsMainNameId=get._id
                                                                                                                              this.computerLabThreeMain()
                                                                                                                               }
                                                                                                                               computerLabThreeMain(){ 
                                                                                                                                this.pro.computerLabThreeGets(this.libraryTwoGetsMainNameId).subscribe((res)=>{
                                                                                                                                this.computerLabThreeMains=res
                                                                                                                                   })
                                                                                                                                }

                                                                                                                                      trackRecordsGetMain(){ 
                                                                                                                                        this.pro.trackRecordsGets(this.batchesGetsMainNameId).subscribe((res)=>{
                                                                                                                                        this.trackRecordsMains=res
                                                                                                                                        console.log(res);
                                                                                                                                          })
                                                                                                                                        }
                                                                                                                                                                                                                                                      
                                                                                                                    
// ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
  console.log(this.replyid);
  
}
 clientsUpdates(){   
  this.pro.clientsUpdatesGet(this.ids).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.pro.clientsUpdatesGetCounts(this.ids).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.pro.commentsPoststoClints(this.commentspostsId,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.pro.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.pro.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.pro.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    console.log(res);
    
    })
 }


  // ***********0verallcomments************
  overallcomment(){
       
     
    this.pro.overallscommentspost(this.ids,this.commentspost).subscribe((res)=>{
     
     this.commentspost.name="",
     this.commentspost.contactNo="",
     this.commentspost.email="",
     this.commentspost.leaveComment=""
    })
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessionalInstitutionsCatageriesComponent } from './professional-institutions-catageries.component';

describe('ProfessionalInstitutionsCatageriesComponent', () => {
  let component: ProfessionalInstitutionsCatageriesComponent;
  let fixture: ComponentFixture<ProfessionalInstitutionsCatageriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfessionalInstitutionsCatageriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessionalInstitutionsCatageriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

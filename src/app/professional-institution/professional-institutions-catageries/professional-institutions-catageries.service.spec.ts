import { TestBed, inject } from '@angular/core/testing';

import { ProfessionalInstitutionsCatageriesService } from './professional-institutions-catageries.service';

describe('ProfessionalInstitutionsCatageriesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProfessionalInstitutionsCatageriesService]
    });
  });

  it('should be created', inject([ProfessionalInstitutionsCatageriesService], (service: ProfessionalInstitutionsCatageriesService) => {
    expect(service).toBeTruthy();
  }));
});

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { HttpClientModule } from '@angular/common/http'; 
import{ RouterModule,Routes} from'@angular/router';
import { AuthGaurdService } from './auth-gaurd.service';
    
 
const routes:Routes=[
  { path: '', redirectTo: 'telangana/telangana', pathMatch: 'full'},
  {
    path:'furnitures',
    loadChildren:'./furnitures/furniture/furniture.module#FurnitureModule'
  },
  {
    path:'babycare',
    loadChildren:'./babycare/babycare/babycare.module#BabycareModule'
  },
  {
    path:'beautyparlour',
    loadChildren:'./beautyparlour/beauty-parlour-catageries/beauty-parlour-catageries.module#BeautyParlourCatageriesModule'
  },
  {
    path:'tutions',
    loadChildren:'./tutions/tutions/tutions.module#TutionsModule'
  },
  {
    path:'gym',
    loadChildren:'./gym/gym/gym.module#GymModule'
  },
  {
    path:'pa',
    loadChildren:'./plantsandanimals/plants/plants.module#PlantsModule'
  },
  {
    path:'animals',
    loadChildren:'./animals/animals/animals.module#AnimalsModule'
  },
  {
    path:'halls',
    loadChildren:'./functons/function-hall-catageries/functonhallcat.module#FunctonhallcatModule'
  },
  {
    path:'dance',
    loadChildren:'./dance/dance/dance.module#DanceModule'
  },
  {
    path:'restaurants',
    loadChildren:'./restaurants/restaurants-cat/restaurants-cat.module#RestaurantsCatModule'
  },
 

  {
    path:'hostelcatageries',
    loadChildren:'./hostels/hostelcatageries/hostelcatageries.module#HostelcatageriesModule'
  },
   
  {
    path:'marriagebureaus',
    loadChildren:'./marriagebureaus/marriagebureaus/marriagebureaus.module#MarriagebureausModule'
  },
  {
    path:'swimming',
    loadChildren:'./swimmingpools/swimming/swimming.module#SwimmingModule'
  },
  {
    path:'fight',
    loadChildren:'./fight/fight/fight.module#FightModule'
  },
  {
    path:'music',
    loadChildren:'./music/music/music.module#MusicModule'
  },
  {
    path:'indoorgames',
    loadChildren:'./indoorgames/indoorgames/indoorgames.module#IndoorgamesModule'
  },
  {
    path:'outdoorgames',
    loadChildren:'./outdoorgames/outdoorgames/outdoorgames.module#OutdoorgamesModule'
  },
  {
    path:'drivingSchools',
    loadChildren:'./drivingSchool/driving-schools/driving-schools.module#DrivingSchoolsModule'
  },
  {
    path:'fireWorks',
    loadChildren:'./fireWorks/fire-works/fir-works.module#FirWorksModule'
  },
  
  {
    path:'packersMovers',
    loadChildren:'./packersMovers/packers-movers/packers-movers.module#PackersMoversModule'
  },
  {
    path:'professionalInstitutionsCatageries',
    loadChildren:'./professional-institution/professional-institutions-catageries/professional-institutions-catageries.module#ProfessionalInstitutionsCatageriesModule'
  },
 
  {
    path:'generals',
    loadChildren:'./general/general/general.module#GeneralModule'
  },
 
  {
    path:'realestate',
    loadChildren:'./realestate/realstate-cats/realestate-cats.module#RealestateCatsModule'
  },
  
  {
    path:'telangana',
    loadChildren:'./telangana/telangana/telangana.module#TelanganaModule'
  },
   
    {
      path:'admin',
      loadChildren:'./admin/admin/admin.module#AdminModule',
      canActivate:[AuthGaurdService]
    },
    {
      path:'login',
      loadChildren:'./login/main-module/main-module.module#MainModuleModule'
    },
    
    {
      path:'advertise',
      loadChildren:'./advertise/advertise/advertise.module#AdvertiseModule'
    },
]

@NgModule({
  declarations: [
    AppComponent,
    ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    
  ],
  providers: [AuthGaurdService],
  bootstrap: [AppComponent]
})
export class AppModule { }

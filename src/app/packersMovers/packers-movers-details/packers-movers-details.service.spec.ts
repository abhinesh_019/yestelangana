import { TestBed, inject } from '@angular/core/testing';

import { PackersMoversDetailsService } from './packers-movers-details.service';

describe('PackersMoversDetailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PackersMoversDetailsService]
    });
  });

  it('should be created', inject([PackersMoversDetailsService], (service: PackersMoversDetailsService) => {
    expect(service).toBeTruthy();
  }));
});

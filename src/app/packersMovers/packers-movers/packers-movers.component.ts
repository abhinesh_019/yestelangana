import { Component, OnInit } from '@angular/core';
import { PackersMoversService } from './packers-movers.service';

@Component({
  selector: 'app-packers-movers',
  templateUrl: './packers-movers.component.html',
  styleUrls: ['./packers-movers.component.css']
})
export class PackersMoversComponent implements OnInit {

  public locationsget:any;
public indoorsUser:any;
public its:any;
public locationget:any
public furnitureusercount:any
public furnitureusercounttot:any
public locationName:String
public searchString:String;
  searchTerm:String;
  public usercountAllDist:any
  public usercountAllAreas:any
  public usercountAllAreasOver:any
  // *********************************************************
    public areasAll:any
  public selectAreaId:any
  public selectArea:any
public name;
 public usercountAll:any
 public popularareas:any
 
  public usercountMainAreas:any
 // **********************************************************
 
 public imageAddsOne:any
 public babycaresAddsOnes:any
  
 public babycaresAddsFours:any
 public imageAddsFour:any
 public imageAddsThree:any
 public imageAddsTwo:any
 public babycaresAddsThrees:any
 public babycaresAddsTwos:any
 
 
 public babycaresAddsFivesLoc:any
 public imageAddsFiveLoc:any
 public imageAddsFourLoc:any
 public babycaresAddsFoursLoc:any
 public imageAddsThreeLoc:any
 public babycaresAddsThreeLoc:any
 public imageAddsTwoLoc:any
 public babycaresAddsTwoLoc:any
 public imageAddsOneLoc:any
 public babycaresAddsOneLoc:any
  constructor(private packers:PackersMoversService) { }

  ngOnInit() {
  
    this.getlocations()
    this.indoorUserscountAll()
    this.indoorUserscountAllDist()
   
  }

getlocations(){
    this. packers.locationapi().subscribe((res)=>{
      this.locationsget=res;
        var id =this.locationsget[0];
        this.locations(id)
    })
  
  }
   locations(get?){
    this.locationName=get.locations
    this.its=get._id
    this.locationget=get.locations
      this.allAreas()
      this.babycaressareasAddsOneLoc()
      this.babycaressareasAddsTwoLoc()
      this.babycaressareasAddsThreeLoc()
      this.babycaressareasAddsFourLoc()
      this.babycaressareasAddsFiveLoc()
     }
  allAreas(){
     
    this. packers.areaapi(this.its).subscribe((res)=>{
      this.areasAll=res
      var id =this.areasAll[0];
        this.selectedAreas(id)
      
    })
   
  }

  selectedAreas(result){
    this.selectAreaId=result._id
    this.selectArea=result.area
      this.indoorUsers()
   this.indoorUserscountAllAreas()
   this.indoorUserscountAllAreasover()
   this.indoorUserscountAllMainAreas() 
   this.babycaressareasAddsOnea()
   this.babycaressareasAddsTwoa()
   this.babycaressareasAddsThreea()
   this.babycaressareasAddsFoura()
   }
 
  indoorUsers(){
    var data:any = {}
    if(this.searchString){
      data.search=this.searchString
     }
    this. packers.indoorUsers(this.selectAreaId,data).subscribe((res)=>{
      this.indoorsUser=res
     })
    
  }

searchFilter(){
  this.indoorUsers()
}
   indoorUserscountAll(){
    this.packers.indoorUserscountsAll().subscribe((res)=>{
      this.usercountAll=res
     })
   }

   indoorUserscountAllMainAreas(){
    this.packers.indoorUserscountAllMainAreas(this.selectAreaId).subscribe((res)=>{
      this.usercountMainAreas=res
      console.log(res);
      
      })
   }
   


   indoorUserscountAllDist(){
    this.packers.indoorUserscountAllDist().subscribe((res)=>{
      this.usercountAllDist=res
      })
   }

   indoorUserscountAllAreas(){
    this.packers.indoorUserscountAllAreas(this.selectAreaId).subscribe((res)=>{
      this.usercountAllAreas=res
      })
   }
   indoorUserscountAllAreasover(){
    this.packers.indoorUserscountAllAreasover(this.selectAreaId).subscribe((res)=>{
      this.usercountAllAreasOver=res
       })
   }
 

// ****************************************************

babycaressareasAddsOnea(){
  this.packers.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
 
     
   })
}
babycaressareasAddsTwoa(){
  this.packers.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
  
   })
}
babycaressareasAddsThreea(){
  this.packers.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
  
   })
}

babycaressareasAddsFoura(){
  this.packers.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res

   })
}

babycaressareasAddsOneLoc(){
  this.packers.babycaresAddsOnel(this.its).subscribe((res)=>{
     this.babycaresAddsOneLoc=res 
   })
}
babycaressareasAddsTwoLoc(){
  this.packers.babycaresAddsTwol(this.its).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
    
   })
}
babycaressareasAddsThreeLoc(){
  this.packers.babycaresAddsThreel(this.its).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
     
   })
}
babycaressareasAddsFourLoc(){
  this.packers.babycaresAddsFourl(this.its).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
     
   })
}
babycaressareasAddsFiveLoc(){
  this.packers.babycaresAddsFivel(this.its).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
    
   })
}

}

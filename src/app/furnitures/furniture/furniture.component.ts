import { Component, OnInit } from '@angular/core';
import { FurnituresService } from '../furnitures.service';

@Component({
  selector: 'app-furniture',
  templateUrl: './furniture.component.html',
  styleUrls: ['./furniture.component.css']
})
export class FurnitureComponent implements OnInit {
public locationsget:any;
public furnitureuser:any;
public its:any;
public locationget:any
public furnitureusercount:any
public furnitureusercounttot:any
public locationName:String
public searchString:String;
  searchTerm:String;
  // *********************************************************
  imageUrl: string = "/assets/img/default-image.png";
   public areasAll:any
  public selectAreaId:any
  public selectArea:any
public name;
 
 public popularareas:any
 public usercountAllDist:any
public furniturewarangal:any
public furnitureHyd:any
 public usercountMainAreas:any
 // **********************************************************
 
 public imageAddsOne:any
 public babycaresAddsOnes:any
  
 public babycaresAddsFours:any
 public imageAddsFour:any
 public imageAddsThree:any
 public imageAddsTwo:any
 public babycaresAddsThrees:any
 public babycaresAddsTwos:any
 
 
 public babycaresAddsFivesLoc:any
 public imageAddsFiveLoc:any
 public imageAddsFourLoc:any
 public babycaresAddsFoursLoc:any
 public imageAddsThreeLoc:any
 public babycaresAddsThreeLoc:any
 public imageAddsTwoLoc:any
 public babycaresAddsTwoLoc:any
 public imageAddsOneLoc:any
 public babycaresAddsOneLoc:any
  constructor(private furniture:FurnituresService) { }

  ngOnInit() {
  
    this.getlocations()
    
  this.furnitureuserscounttot() 
   this.indoorUserscountAllDist()
   this.indoorUserscountAllMainAreas()
  
  }

getlocations(){
    this.furniture.locationapi().subscribe((res)=>{
      this.locationsget=res;
      
      var id =this.locationsget[0];
      
      
      this.locations(id)
    })
  
  }
  
  
  locations(get?){
    this.locationName=get.locations
    this.its=get._id
    this.locationget=get.locations
    
    this.allAreas()
    this.babycaressareasAddsOneLoc()
    this.babycaressareasAddsTwoLoc()
    this.babycaressareasAddsThreeLoc()
    this.babycaressareasAddsFourLoc()
    this.babycaressareasAddsFiveLoc()
  }
  allAreas(){
     
    this.furniture.areaapi(this.its).subscribe((res)=>{
      this.areasAll=res
      var id =this.areasAll[0];
      console.log(this.areasAll[0]);
      
      this.selectedAreas(id)
      
    })
   
  }

  selectedAreas(result){
    this.selectAreaId=result._id
    this.selectArea=result.area
    this.furnitureuserscount()   
    this.furnitureusers()
   this.furnituresareascount() 
   
   this.babycaressareasAddsOnea()
   this.babycaressareasAddsTwoa()
   this.babycaressareasAddsThreea()
   this.babycaressareasAddsFoura()
  }
  indoorUserscountAllMainAreas(){
    this.furniture.indoorUserscountAllMainAreas().subscribe((res)=>{
      this.usercountMainAreas=res
      })
   }
  indoorUserscountAllDist(){
    this.furniture.indoorUserscountAllDist().subscribe((res)=>{
      this.usercountAllDist=res
      })
   }
  furnitureusers(){
    var data:any =  {}
    if(this.searchString){
      data.search=this.searchString
   }
    this.furniture.furnitureusers(this.selectAreaId,data).subscribe((res)=>{
      this.furnitureuser=res
      })
    
  }

searchFilter(){
  this.furnitureusers()
}
// ***************************************************************
  furnitureuserscount(){
    this.furniture.furnitureuserscounts(this.selectAreaId).subscribe((res)=>{
      this.furnitureusercount=res
      console.log(res);
      
    })
   }
  
// ***************************************************************
   furnitureuserscounttot(){
    this.furniture.furnitureuserscountstot().subscribe((res)=>{
      this.furnitureusercounttot=res
      console.log(this.furnitureusercounttot);
      
    })
   }

   furnituresareascount(){
     this.furniture.furnitureuserscountsarea(this.selectAreaId).subscribe((res)=>{
       console.log(res);
       this.popularareas=res
     })
   }
 
  // routes(){
  //   this.router.navigate(["/users/hyderabad"]);

  // }
// ****************************************************

// ****************************************************

babycaressareasAddsOnea(){
  this.furniture.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
 
     
   })
}
babycaressareasAddsTwoa(){
  this.furniture.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
  
   })
}
babycaressareasAddsThreea(){
  this.furniture.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
  
   })
}

babycaressareasAddsFoura(){
  this.furniture.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res

   })
}

babycaressareasAddsOneLoc(){
  this.furniture.babycaresAddsOnel(this.its).subscribe((res)=>{
     this.babycaresAddsOneLoc=res 
   })
}
babycaressareasAddsTwoLoc(){
  this.furniture.babycaresAddsTwol(this.its).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
    
   })
}
babycaressareasAddsThreeLoc(){
  this.furniture.babycaresAddsThreel(this.its).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
     
   })
}
babycaressareasAddsFourLoc(){
  this.furniture.babycaresAddsFourl(this.its).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
     
   })
}
babycaressareasAddsFiveLoc(){
  this.furniture.babycaresAddsFivel(this.its).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
    
   })
}

}

import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
 import { PadetailsComponent } from '../padetails/padetails.component';
import { PlantsService } from '../plants.service';



const routes:Routes=[{path:'plantsdetails/:_id/:name',component:PadetailsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
   ],
  declarations: [
    PadetailsComponent,

  ],
  providers: [PlantsService],
})
 
export class PadetailsModule { }

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PlantsService } from '../plants.service';

@Component({
  selector: 'app-padetails',
  templateUrl: './padetails.component.html',
  styleUrls: ['./padetails.component.css']
})
export class PadetailsComponent implements OnInit {
  public animalsuser:any
  public ids:any
  public animalsTypes:any
  public animalsNames:any
  public animalsId:any
  public animalsTypesDes:any
  public selectAnimals:any
  public animalsTypesDesInd:any
  public clientUpdatesCount:any
  public clientUpdates:any
  public commentspostsId:any
  public getUsersComments:any
  public getUsersCommentsCounts:any
  public openshareid:any
  public viewcommentsid:any
  public replyid:any
  public replies:any
  public animalsTypesCnts:any
  public clientUpdatesCnts:any
  public plantsMaintaincs:any
  public plantsMaintaincsC:any
  public plantsFertilizerss:any
  public plantsFertilizersC:any

  public share:boolean=false
  public comments:boolean=false
  public viewcomments:boolean=false
  public replyfrom:boolean=false 
  
  postComments={
    "descriptions":""
  }
  commentspost={
    "name":"",
    "email":"",
    "contactNo":"",
    "leaveComment":"",
      }
     
  

  constructor(private router:Router,private route:ActivatedRoute,private plants:PlantsService) { }

  ngOnInit() {
    this.individualdata()
    this.clientsUpdates()
    this.plantsMaintainces()
    this.animalsLists()
    this.clientsUpdatesCount()
    this.plantsMaintaincesCounts()
    this.plantsFertilizers()
    this.plantsFertilizersCounts()
   
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];

this.usersDetails()
}

usersDetails(){ 
this.plants.plantsusersonly(this.ids).subscribe((res)=>{
this.animalsuser=res
})
}

 
animalsLists(){   
  this.plants.plantsTypes(this.ids).subscribe((res)=>{
    this.animalsTypes=res
    var id =this.animalsTypes[0];
     this.selectedAnimals(id);
      console.log(res);
   })
 }


selectedAnimals(result){
  this.selectAnimals="";
  this.animalsTypesDesInd="";
this.animalsId=result._id;
this.animalsNames=result.plantsTypes;
this.animalsListDetails();
this.animalsListsTypeCnt();

}
animalsListsTypeCnt(){   
  this.plants.plantsListsTypeCnt(this.animalsId).subscribe((res)=>{
    this.animalsTypesCnts=res;
    })
 }
animalsListDetails(){   
  this.plants.plantsListDetail(this.animalsId).subscribe((res)=>{
    this.animalsTypesDes=res;
    })
    
 }

 selectAnimal(selects){
this.selectAnimals=selects._id
this.animalsListDetailsIndividuals()
 }
 animalsListDetailsIndividuals(){   
  this.plants.plantsListDetailIndv(this.selectAnimals).subscribe((res)=>{
    this.animalsTypesDesInd=res
  })
 }

// ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
}
 clientsUpdates(){   
  this.plants.clientsUpdatesGet(this.ids).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.plants.clientsUpdatesGetCounts(this.ids).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.plants.commentsPoststoClints(this.commentspostsId,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.plants.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.plants.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.plants.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    })
 }


  // ***********0verallcomments************
  overallcomment(){
       
     
    this.plants.overallscommentspost(this.ids,this.commentspost).subscribe((res)=>{
     
     this.commentspost.name="",
     this.commentspost.contactNo="",
     this.commentspost.email="",
     this.commentspost.leaveComment=""
    })
  }

  plantsMaintainces(){   
    this.plants.plantsMaintainces(this.ids).subscribe((res)=>{
      this.plantsMaintaincs=res
      })
     }
     plantsMaintaincesCounts(){   
      this.plants.plantsMaintaincesCounts(this.ids).subscribe((res)=>{
        this.plantsMaintaincsC=res
        })
       }

       plantsFertilizers(){   
        this.plants.plantsFertilizerss(this.ids).subscribe((res)=>{
          this.plantsFertilizerss=res
          })
         }
         plantsFertilizersCounts(){   
          this.plants.plantsFertilizersCounts(this.ids).subscribe((res)=>{
            this.plantsFertilizersC=res
            })
           }
}

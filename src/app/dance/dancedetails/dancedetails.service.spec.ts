import { TestBed, inject } from '@angular/core/testing';

import { DancedetailsService } from './dancedetails.service';

describe('DancedetailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DancedetailsService]
    });
  });

  it('should be created', inject([DancedetailsService], (service: DancedetailsService) => {
    expect(service).toBeTruthy();
  }));
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DancedetailsComponent } from './dancedetails.component';

describe('DancedetailsComponent', () => {
  let component: DancedetailsComponent;
  let fixture: ComponentFixture<DancedetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DancedetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DancedetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

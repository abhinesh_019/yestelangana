import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { DancedetailsComponent } from './dancedetails.component';
import { DancedetailsService } from './dancedetails.service';



const routes:Routes=[{path:'dancedetails/:_id/:name',component:DancedetailsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    DancedetailsComponent,

  ],
  providers: [DancedetailsService],
})
 
export class DancedetailsModule { }

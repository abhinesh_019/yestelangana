import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
 import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
 import { SharedModule } from '../../shared/shared.module';
import { HosteldetailsModule } from '../hosteldetails/hosteldetails.module';
import { HostelsService } from '../hostels.service';
import { HostelcatageriesComponent } from './hostelcatageries.component';
import { HostelsModule } from '../hostels/hostels.module';

const routes: Routes = [
  {
    path: '', component: HostelcatageriesComponent, children:
      [
        { path: 'hostels/:_id/:name', loadChildren: 'app/hostels/hostels/hostels.module#HostelsModule'},
        { path: 'hosteldetails/:_id/:name', loadChildren: 'app/hostels/hosteldetails/hosteldetails.module#HosteldetailsModule'},

      ]
  }]
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    SharedModule,
    HostelsModule,
    HosteldetailsModule
  ],
  declarations: [
    HostelcatageriesComponent
  ],
  providers:[HostelsService]
})
export class HostelcatageriesModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HostelcatageriesComponent } from './hostelcatageries.component';

describe('HostelcatageriesComponent', () => {
  let component: HostelcatageriesComponent;
  let fixture: ComponentFixture<HostelcatageriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HostelcatageriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HostelcatageriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

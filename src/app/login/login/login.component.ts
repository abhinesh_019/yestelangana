import { Component, OnInit } from '@angular/core';
import { GolbalService } from '../golbal.service';
import { Router } from '@angular/router';
import { SweetalertService } from '../../sweetalert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public login
// public forgetpasswords;
public saveres;
  
  constructor(private globalService:GolbalService, private router:Router, private alert:SweetalertService) { }

   

  logins=
     {
        
        email:"",
        password:"",
        adminsId:""
      }


    
      
      public SignUser : any;
   

    ngOnInit() {
    
    }
  
    
  loginhtml(){
  

    this.globalService.loginService(this.logins).subscribe((response)=>{
      this.SignUser = response;
      console.log(this.SignUser);


      localStorage.setItem('_id',this.SignUser._id);
      localStorage.setItem('accesstoken',this.SignUser.token);
      localStorage.setItem('ads',this.SignUser.adminsId);

      if(this.SignUser.role =="admin"){
        if(this.SignUser){
          this.router.navigate(["admin"]);
          this.alert.loginSuccess();
        }else{
        this.alert.errorAlert("Enter Vaild Credentials","Failed to update","2000")
        }
      } 
      
      else{
       localStorage.setItem('_id',this.SignUser._id);
        if(this.SignUser.role == 'normal'){
          
           this.router.navigate(["/telangana/telangana"]);
       
          }
       }
      
    })
    
  
  }


}

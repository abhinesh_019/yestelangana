import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GolbalService } from '../golbal.service';
import { ActivatedRouteSnapshot,ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {

  constructor(private router:Router,private resetpwd:GolbalService,private route:ActivatedRoute) { }
  public rpass;
  public token
  // // public accesstoken
  resetpassors={
    password:"",
     //cpassword:""
     token:""

  }

  

    ngOnInit() {
      this.token=localStorage.getItem('accesstoken')
      console.log(this.token);
      this.route.params.subscribe(params => {
       this.resetpassors.token= params['token'];
        console.log('this.token',this.resetpassors.token)
        })

      
    }

    rgetpassword(){
      this.resetpwd.rgetpassword(this.resetpassors).subscribe((response)=>{
        // localStorage.getItem('accesstoken',this.SignUser.token);
    
        this.rpass = response
        console.log(this.rpass);
        
        
      })
        
        
    }
}

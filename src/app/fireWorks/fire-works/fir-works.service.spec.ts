import { TestBed, inject } from '@angular/core/testing';

import { FirWorksService } from './fir-works.service';

describe('FirWorksService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FirWorksService]
    });
  });

  it('should be created', inject([FirWorksService], (service: FirWorksService) => {
    expect(service).toBeTruthy();
  }));
});

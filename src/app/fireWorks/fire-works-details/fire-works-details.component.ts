import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FireWorksDetailsService } from './fire-works-details.service';

@Component({
  selector: 'app-fire-works-details',
  templateUrl: './fire-works-details.component.html',
  styleUrls: ['./fire-works-details.component.css']
})
export class FireWorksDetailsComponent implements OnInit {

  public clientsDetails:any
  public ids:any
  public showmains:boolean=false
  public updatesShows:boolean=false
  public viewupdatesShows:boolean=false
  public onlyCommentsShows:boolean=false
  public ImagesShows:boolean=false
  public ProductsShows:boolean=false
  public ProductsViewShows:boolean=false

  public usersID:any
public fireWorksClients:any
public viewsProfiles:any
public viewsNoti:any
public clientsInd:any
  public servicesOneGet:any
public servicevehicleName:any
public servicesIds:any
public servicesOneGets:any
public servicevehicleNames:any
public servicesone:any
public servicesTwos:any
public trackRecordsId:any
public trackRecordsName:any
public trackRecordsGet:any
public trackRecordsgetsData:any
public servicesOneGetc:any
public trackRecordsgetsDatac:any
public servicesTwosc:any
public totalCustomer:any
public totalCustomerc:any
public fecilitiesGets:any
public fecilitiesGetsc:any
public commentspostsId:any
  public viewcommentsid:any
  public clientUpdates:any
  public replyid:any
  public clientUpdatesCount:any
   public getUsersComments:any
  public getUsersCommentsCounts:any
  public share:boolean=false
  public comments:boolean=false
  public viewcomments:boolean=false
  public replyfrom:boolean=false 
  public replies:any
  public openshareid:any
public imageNameupdates:any
  public videoNames:any
  public gcomm:any
  public gcommc:any
  public fireWorksDetails:any
  public serviceTwoGetc:any
  public fireWorksDetailsId:any
  public fireWorksDetailsName:any
  public cusotmersgetDetails:any
  public cusotmersgetDetailsc:any
   

  
  postComments={
    "descriptions":""
  }
  commentspost={
    "name":"",
    "email":"",
    "contactNo":"",
    "leaveComment":"",
      }
  constructor(private router:Router,private route:ActivatedRoute,private fire:FireWorksDetailsService) { }
   ngOnInit() {
    this.individualdata()
    this.usersDetails()
   this.totalCustomersc()
   this.totalCustomers()
   this.servicesOne()
   this.servicesOnec()
   this.clientsUpdates()
   this.clientsUpdatesCount()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];

}

usersDetails(){ 
this.fire.usersDetails(this.ids).subscribe((res)=>{
this.clientsDetails=res
 
})
}

servicesOne(){ 
  this.fire.servicesOne(this.ids).subscribe((res)=>{
  this.servicesOneGet=res  
  })
  }
  servicesOnec(){ 
    this.fire.servicesOnec(this.ids).subscribe((res)=>{
    this.servicesOneGetc=res 
    })
    }
  // end
  
  ServicesOneClick(vec){
    this.servicesIds=vec._id
    this.servicevehicleName=vec.typeOfFiewWorks
 
  this.servicesTwo()
  this.servicesTwoc()
   }

   servicesTwo(){ 
    this.fire.servicesTwo(this.servicesIds).subscribe((res)=>{
    this.fireWorksDetails=res 
    })
    }
    servicesTwoc(){ 
      this.fire.servicesTwoc(this.servicesIds).subscribe((res)=>{
      this.serviceTwoGetc=res 
      })
      }
      viewCustomersClick(fire){
  this.fireWorksDetailsId=fire._id
  this.fireWorksDetailsName=fire.nameOfFireWorks
  console.log(this.fireWorksDetailsId);
  console.log(this.fireWorksDetailsName);
  this.customersDetailsGet()
  this.customersDetailsGetc()
      }
      customersDetailsGet(){ 
        this.fire.customersDetailsGet(this.fireWorksDetailsId).subscribe((res)=>{
        this.cusotmersgetDetails=res 
        console.log(this.cusotmersgetDetails);
        })
        }
        customersDetailsGetc(){ 
          this.fire.customersDetailsGetc(this.fireWorksDetailsId).subscribe((res)=>{
          this.cusotmersgetDetailsc=res 
          })
          }
          totalCustomers(){ 
            this.fire.totalCustomers(this.ids).subscribe((res)=>{
            this.totalCustomer=res 
            console.log(res);
            
            })
            }
            totalCustomersc(){ 
              this.fire.totalCustomersc(this.ids).subscribe((res)=>{
              this.totalCustomerc=res 
              console.log(res);
              
              })
              }
    // ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
  console.log(this.replyid);
  
}
 clientsUpdates(){   
  this.fire.clientsUpdatesGet(this.ids).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.fire.clientsUpdatesGetCounts(this.ids).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.fire.commentsPoststoClints(this.commentspostsId,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.fire.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.fire.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.fire.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    console.log(res);
    
    })
 }


  // ***********0verallcomments************
  overallcomment(){
       
     
    this.fire.overallscommentspost(this.ids,this.commentspost).subscribe((res)=>{
     
     this.commentspost.name="",
     this.commentspost.contactNo="",
     this.commentspost.email="",
     this.commentspost.leaveComment=""
    })
  }
 }



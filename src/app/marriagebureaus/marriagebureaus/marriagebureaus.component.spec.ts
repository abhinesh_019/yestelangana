import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarriagebureausComponent } from './marriagebureaus.component';

describe('MarriagebureausComponent', () => {
  let component: MarriagebureausComponent;
  let fixture: ComponentFixture<MarriagebureausComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarriagebureausComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarriagebureausComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

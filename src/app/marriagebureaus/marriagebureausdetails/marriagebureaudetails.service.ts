import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
  
@Injectable()
export class MarriagebureaudetailsService {

  constructor(private http: HttpClient) { }
  usersDetails(id){
    return   this.http.get(environment.apiUrl +"/marriagesClientsGetind/"+id);
   }
   casteCatGet(id){
    return this.http.get(environment.apiUrl + "/marriagesServicesTypeGet/"+id);
     
  }
  maincasteCatGet(id){
    return this.http.get(environment.apiUrl + "/marriagesServicesTypemainGet/"+id);
     
  }
  smaincasteCatGet(id){
    return this.http.get(environment.apiUrl + "/marriagesServicesGet/"+id);
     
  }
  tracksRecords(id){
    return this.http.get(environment.apiUrl + "/marriagesTrackRecordsGet/"+id);
     
  }
  tracksRecordsc(id){
    return this.http.get(environment.apiUrl + "/marriagesTrackRecordsGetCounts/"+id);
     
  }
  clientsUpdatesGet(id){
    
    return this.http.get(environment.apiUrl +"/marriagesupdatesget/"+id);
  }
  
  clientsUpdatesGetCounts(id){
    
    return this.http.get(environment.apiUrl +"/marriagesupdatesgetCounts/"+id);
  }
  commentsPoststoClints(id,data){
    
    return this.http.post(environment.apiUrl +"/marriagesupdatesCommentspost/"+id,data);
  }
  commentsGettoClints(id){
    
    return this.http.get(environment.apiUrl +"/marriagesupdatesCommentsget/"+id);
  }
  commentsGettoClintsCounts(id){
    
    return this.http.get(environment.apiUrl +"/marriagesupdatesCommentsgetcounts/"+id);
  }
  replyFromCommentesGet(id){
    
    return this.http.get(environment.apiUrl +"/marriagesupdatesCommentReplysGet/"+id);
  }

  overallscommentspost(id,data){
    
    return this.http.post(environment.apiUrl +"/marriageseuserscomments/"+id,data);
  }
  // ********************************

}


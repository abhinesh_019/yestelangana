import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { RestaurantsComponent } from './restaurants.component';
import { RestaurantsService } from './restaurants.service';
 


const routes:Routes=[{path:'restaurantSelect/:_id/:name',component:RestaurantsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    RestaurantsComponent,

  ],
  providers: [RestaurantsService],
})
 
export class RestaurantsModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaurantsdetailsComponent } from './restaurantsdetails.component';

describe('RestaurantsdetailsComponent', () => {
  let component: RestaurantsdetailsComponent;
  let fixture: ComponentFixture<RestaurantsdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaurantsdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurantsdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

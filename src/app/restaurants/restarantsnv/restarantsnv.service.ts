import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
     
@Injectable()
export class RestarantsnvService {

  constructor(private http: HttpClient) { }
  servicesOne(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsFoodTypeInd/"+id);
  } 
  dishTypeGet(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsFoodTypeDish/"+id);
  } 
  selectItemsTwoGet(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsFoodTypeTwo/"+id);
  }
  mainSelectItemGet(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsServicesGetOne/"+id);
  }
}

import { TestBed, inject } from '@angular/core/testing';

import { RestarantsnvService } from './restarantsnv.service';

describe('RestarantsnvService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RestarantsnvService]
    });
  });

  it('should be created', inject([RestarantsnvService], (service: RestarantsnvService) => {
    expect(service).toBeTruthy();
  }));
});

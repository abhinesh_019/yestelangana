import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
 import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
  import { RestaurantsdetailsModule } from '../restaurantsdetails/restaurantsdetails.module';
import { RestaurantsCatComponent } from './restaurants-cat.component';
import { RestaurantsCatService } from './restaurants-cat.service';
import { RestaurantsModule } from '../restaurants/restaurants.module';
import { RestarantsnvModule } from '../restarantsnv/restarantsnv.module';

const routes: Routes = [
  {
    path: '', component: RestaurantsCatComponent, children:
      [
      { path: 'restaurantSelect/:_id/:name', loadChildren: 'app/restaurants/restaurants/restaurants.module#RestaurantsModule'},
      { path: 'restaurantsdetails/:_id/:name', loadChildren: 'app/restaurants/restaurantsdetails/restaurantsdetails.module#RestaurantsdetailsModule'},
      { path: 'restaurantsNonVeg/:_id/:name', loadChildren: 'app/restaurants/restarantsnv/restarantsnv.module#RestarantsnvModule'},

    ]
  }]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
     SharedModule,
     RestaurantsdetailsModule,
     RestaurantsModule,
     RestarantsnvModule
    ],
  declarations: [
    RestaurantsCatComponent,
  ],
  providers:[RestaurantsCatService]
})
 
export class RestaurantsCatModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
 import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { AdvertiseComponent } from './advertise.component';
import { AdvertiseService } from './advertise.service';
 

const routes: Routes = [
  {
    path: '', component: AdvertiseComponent, children:
      [
      // { path: 'animalsdetails/:_id/:name', loadChildren: 'app/animals/animalsdetails/animalsdetails.module#AnimalsdetailsModule'},
      ]
  }]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
     SharedModule,
       
    ],
  declarations: [
    AdvertiseComponent,
  ],
  providers:[AdvertiseService]
})
export class AdvertiseModule { }

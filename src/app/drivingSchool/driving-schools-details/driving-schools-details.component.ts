import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DrivingSchoolsDetailsService } from './driving-schools-details.service';
 
@Component({
  selector: 'app-driving-schools-details',
  templateUrl: './driving-schools-details.component.html',
  styleUrls: ['./driving-schools-details.component.css']
})
export class DrivingSchoolsDetailsComponent implements OnInit {
  public clientsDetails:any
  public ids:any
  public servicesOneGet:any
  public servicesOneGetc:any
  public servicevehicleName:any
  public servicesIds:any
  public servicesOneGets:any
  public servicevehicleNames:any
  public servicesone:any
  public servicesTwos:any
  public trackRecordsId:any
  public trackRecordsName:any
  public trackRecordsGet:any
  public trackRecordsgetsData:any
   public trackRecordsgetsDatac:any
  public servicesTwosc:any
  public totalCustomer:any
  public totalCustomerc:any
  public fecilitiesGets:any
  public fecilitiesGetsc:any
  public commentspostsId:any
  public viewcommentsid:any
  public clientUpdates:any
  public replyid:any
  public clientUpdatesCount:any
   public getUsersComments:any
  public getUsersCommentsCounts:any
  public share:boolean=false
  public comments:boolean=false
  public viewcomments:boolean=false
  public replyfrom:boolean=false 
  public replies:any
  public openshareid:any

  
  postComments={
    "descriptions":""
  }
  commentspost={
    "name":"",
    "email":"",
    "contactNo":"",
    "leaveComment":"",
      }
   constructor(private router:Router,private route:ActivatedRoute,private driving:DrivingSchoolsDetailsService) { }
public  s="5d232d17a5744b686509ef27"
  ngOnInit() {
    this.individualdata()
    this.usersDetails()
   this.servicesOne()
   this.servicesOnec()
   this.totalCustomers()
   this.totalCustomersc()
   this.fecilitiesGet()
   this.fecilitiesGetc()
   this.clientsUpdates()
   this.clientsUpdatesCount()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];

}

usersDetails(){ 
this.driving.usersDetails(this.ids).subscribe((res)=>{
this.clientsDetails=res
 
})
}
servicesOne(){ 
  this.driving.servicesOne(this.ids).subscribe((res)=>{
  this.servicesOneGet=res 
  console.log(res);
  
  })
  }
  servicesOnec(){ 
    this.driving.servicesOnec(this.ids).subscribe((res)=>{
    this.servicesOneGetc=res 
    console.log(res);
    
    })
    }
    ServicesOneClick(vec){
      this.servicesIds=vec._id
      this.servicevehicleName=vec.vehicleName
      console.log(this.servicevehicleName);
      console.log(this.servicesIds);
       this.servicesTwo()
       this.servicesTwoc()
       this.trackRecords()
       this.trackRecordsc()
    }
     servicesTwo(){ 
      this.driving.servicesTwo(this.servicesIds).subscribe((res)=>{
      this.servicesTwos=res 
      console.log(res);
      
      })
      }
      servicesTwoc(){ 
        this.driving.servicesTwoc(this.servicesIds).subscribe((res)=>{
        this.servicesTwosc=res 
        console.log(res);
        
        })
        }
        trackRecords(){ 
          this.driving.trackRecords(this.servicesIds).subscribe((res)=>{
          this.trackRecordsgetsData=res 
          console.log(res);
          
          })
          }
          trackRecordsc(){ 
            this.driving.trackRecordsc(this.servicesIds).subscribe((res)=>{
            this.trackRecordsgetsDatac=res 
            console.log(res);
            
            })
            }
            totalCustomers(){ 
              this.driving.totalCustomers(this.ids).subscribe((res)=>{
              this.totalCustomer=res 
              console.log(res);
              
              })
              }
              totalCustomersc(){ 
                this.driving.totalCustomersc(this.ids).subscribe((res)=>{
                this.totalCustomerc=res 
                console.log(res);
                
                })
                }
                fecilitiesGet(){ 
                  this.driving.fecilitiesGet(this.ids).subscribe((res)=>{
                  this.fecilitiesGets=res 
                  console.log(res);
                  
                  })
                  }
                  fecilitiesGetc(){ 
                    this.driving.fecilitiesGetc(this.ids).subscribe((res)=>{
                    this.fecilitiesGetsc=res 
                    console.log(res);
                    
                    })
                    }
                     // ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
  console.log(this.replyid);
  
}
 clientsUpdates(){   
  this.driving.clientsUpdatesGet(this.ids).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.driving.clientsUpdatesGetCounts(this.ids).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.driving.commentsPoststoClints(this.commentspostsId,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.driving.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.driving.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.driving.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    console.log(res);
    
    })
 }


  // ***********0verallcomments************
  overallcomment(){
       
     
    this.driving.overallscommentspost(this.ids,this.commentspost).subscribe((res)=>{
     
     this.commentspost.name="",
     this.commentspost.contactNo="",
     this.commentspost.email="",
     this.commentspost.leaveComment=""
    })
  }
 }


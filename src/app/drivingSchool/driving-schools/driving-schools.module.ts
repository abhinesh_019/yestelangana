import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { DrivingSchoolsComponent } from './driving-schools.component';
import { DrivingSchoolsService } from './driving-schools.service';
import { DrivingSchoolsDetailsModule } from '../driving-schools-details/driving-schools-details.module';
 

const routes: Routes = [
  {
    path: '', component: DrivingSchoolsComponent, children:
      [
      { path: 'drivingSchoolsdetails/:_id/:name', loadChildren: 'app/drivingSchool/driving-schools-details/driving-schools-details.module#DrivingSchoolsDetailsModule'},
 
      ]
  }]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
     SharedModule,
     DrivingSchoolsDetailsModule
       ],
  declarations: [
    DrivingSchoolsComponent
     ],
  providers:[DrivingSchoolsService]
})

export class DrivingSchoolsModule { }

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SwimmingService } from './swimming.service';

@Component({
  selector: 'app-swimming',
  templateUrl: './swimming.component.html',
  styleUrls: ['./swimming.component.css']
})
export class SwimmingComponent implements OnInit {

  
  public area:any
  public searchString:String;
  searchTerm:String;
  public locationsget:any
  public locations:any
  public locationName:any
public its:any
public locationget:any
public areasAll:any
public selectAreaId:any
public selectArea:any
public marriageuser:any
public marriageusercount:any
public popularareas:any
public amarriageusercount:any
public marriageusercountLocations:any
public marriageusercounttot:any
 public marriageusercountMainAreas:any
public marriageusercountSubAreas:any
public marriageusercountSingleAreas:any

 // **********************************************************
 
 public imageAddsOne:any
 public babycaresAddsOnes:any
  
 public babycaresAddsFours:any
 public imageAddsFour:any
 public imageAddsThree:any
 public imageAddsTwo:any
 public babycaresAddsThrees:any
 public babycaresAddsTwos:any
 
 
 public babycaresAddsFivesLoc:any
 public imageAddsFiveLoc:any
 public imageAddsFourLoc:any
 public babycaresAddsFoursLoc:any
 public imageAddsThreeLoc:any
 public babycaresAddsThreeLoc:any
 public imageAddsTwoLoc:any
 public babycaresAddsTwoLoc:any
 public imageAddsOneLoc:any
 public babycaresAddsOneLoc:any
 
  constructor(private router:Router,private route:ActivatedRoute,private swimm:SwimmingService) { }

  ngOnInit() {
    this.getlocations()
    this.marriageuserscounttot()
    this.marriageuserscountLocations()
   }

    
  // locations*****************
getlocations(){
  this.swimm.locationapi().subscribe((res)=>{
    this.locationsget=res;
   var id =this.locationsget[0];
     this.location(id)
   })}

  location(get?){
    this.locationName=get.locations
    this.its=get._id
    this.allAreas()
    this.babycaressareasAddsOneLoc()
    this.babycaressareasAddsTwoLoc()
    this.babycaressareasAddsThreeLoc()
    this.babycaressareasAddsFourLoc()
    this.babycaressareasAddsFiveLoc()
   }
 
   allAreas(){
     this.swimm.areaapi(this.its).subscribe((res)=>{
      this.areasAll=res
      var id =this.areasAll[0];
       this.selectedAre(id)
     })
   }

selectedAre(result){
this.selectAreaId=result._id
this.selectArea=result.area
this.marriageuserscountMainAreas()   
this.marriageusers()
this.marriageuserscount() 
this.marriageuserSinglecountArea()
this.babycaressareasAddsOnea()
this.babycaressareasAddsTwoa()
this.babycaressareasAddsThreea()
this.babycaressareasAddsFoura()
   }


 marriageusers(){
  var data:any =  {}
  if(this.searchString){
    data.search=this.searchString
    }
  this.swimm.marriageusers(this.selectAreaId,data).subscribe((res)=>{
    this.marriageuser=res
    console.log(res);
    
   })
 }

searchFilter(){
this.marriageusers()
}

// ***************************************************************
marriageuserSinglecountArea(){
  this.swimm.marriageusersForSingleAreas(this.selectAreaId).subscribe((res)=>{
    this.marriageusercountSingleAreas=res
   })
 }
marriageuserscount(){
  this.swimm.marriageusersForSubAreas(this.selectAreaId).subscribe((res)=>{
    this.marriageusercountSubAreas=res
   })
 }
 marriageuserscountLocations(){
  this.swimm.marriageusersForLocations().subscribe((res)=>{
    this.marriageusercountLocations=res
   })
 }
 marriageuserscountMainAreas(){
  this.swimm.marriageusersForMainAreas(this.selectAreaId).subscribe((res)=>{
    this.marriageusercountMainAreas=res
   })
 }
// ***************************************************************
marriageuserscounttot(){
  this.swimm.marriageuserscountstot().subscribe((res)=>{
    this.marriageusercounttot=res
 })
 }
 
  
// ****************************************************

babycaressareasAddsOnea(){
  this.swimm.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
 
     
   })
}
babycaressareasAddsTwoa(){
  this.swimm.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
  
   })
}
babycaressareasAddsThreea(){
  this.swimm.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
  
   })
}

babycaressareasAddsFoura(){
  this.swimm.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res

   })
}

babycaressareasAddsOneLoc(){
  this.swimm.babycaresAddsOnel(this.its).subscribe((res)=>{
     this.babycaresAddsOneLoc=res 
   })
}
babycaressareasAddsTwoLoc(){
  this.swimm.babycaresAddsTwol(this.its).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
    
   })
}
babycaressareasAddsThreeLoc(){
  this.swimm.babycaresAddsThreel(this.its).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
     
   })
}
babycaressareasAddsFourLoc(){
  this.swimm.babycaresAddsFourl(this.its).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
     
   })
}
babycaressareasAddsFiveLoc(){
  this.swimm.babycaresAddsFivel(this.its).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
    
   })
}

}

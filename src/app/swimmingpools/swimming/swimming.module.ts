import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
 import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
 import { SwimmingComponent } from './swimming.component';
import { SwimmingService } from './swimming.service';
import { SharedModule } from '../../shared/shared.module';
import { SwimmingdetailsModule } from '../swimmingdetails/swimmingdetails.module';
 

const routes: Routes = [
  {
    path: '', component: SwimmingComponent, children:
      [
      { path: 'swimmingdetails/:_id/:name', loadChildren: 'app/swimmingpools/swimmingdetails/swimmingdetails.module#SwimmingdetailsModule'},
      ]
  }]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
     SharedModule,
     SwimmingdetailsModule
     ],
  declarations: [
    SwimmingComponent,
  ],
  providers:[SwimmingService]
})
 
export class SwimmingModule { }

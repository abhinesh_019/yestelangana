import { TestBed, inject } from '@angular/core/testing';

import { SwimmingService } from './swimming.service';

describe('SwimmingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SwimmingService]
    });
  });

  it('should be created', inject([SwimmingService], (service: SwimmingService) => {
    expect(service).toBeTruthy();
  }));
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
  
@Injectable()
export class SwimmingdetailsService {

  constructor(private http: HttpClient) { }
  locationapi( ){
    return   this.http.get(environment.apiUrl +"/swimmingLocations");
   }
   areaapi(id){
    return   this.http.get(environment.apiUrl +"/swimmingAreas/"+id);
   }
 
   usersDetails(id){
    return   this.http.get(environment.apiUrl +"/swimmingClientsGetind/"+id);
   }
   servicesGenders(id){
    return this.http.get(environment.apiUrl +"/swimmingCreatingGendersGet/"+id)
   }
   servicesaccGet(id){
    return this.http.get(environment.apiUrl +"/swimmingAccessaoriesGet/"+id)
   }
   servicesaccGetc(id){
    return this.http.get(environment.apiUrl +"/swimmingAccessaoriesCounts/"+id)
   }
   fecilitiesGet(id){
    return this.http.get(environment.apiUrl +"/swimmingFecilitiesGet/"+id)
   }
   fecilitiesGetc(id){
    return this.http.get(environment.apiUrl +"/swimmingFecilitiesCounts/"+id)
   }
   
   clientsUpdatesGet(id){
    
    return this.http.get(environment.apiUrl +"/swimmingsupdatesget/"+id);
  }
  
  clientsUpdatesGetCounts(id){
    
    return this.http.get(environment.apiUrl +"/swimmingsupdatesgetCounts/"+id);
  }
  commentsPoststoClints(id,data){
    
    return this.http.post(environment.apiUrl +"/swimmingsupdatesCommentspost/"+id,data);
  }
  commentsGettoClints(id){
    
    return this.http.get(environment.apiUrl +"/swimmingsupdatesCommentsget/"+id);
  }
  commentsGettoClintsCounts(id){
    
    return this.http.get(environment.apiUrl +"/swimmingsupdatesCommentsgetcounts/"+id);
  }
  replyFromCommentesGet(id){
    
    return this.http.get(environment.apiUrl +"/swimmingsupdatesCommentReplysGet/"+id);
  }

  overallscommentspost(id,data){
    
    return this.http.post(environment.apiUrl +"/swimmingseuserscomments/"+id,data);
  }
  // ********************************

}


import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import swal from 'sweetalert2';

@Injectable()
export class SweetalertService {

  SuccessAlert(title,textToShow,timeInMS?){
    if(!timeInMS)timeInMS=1500;
    Swal({
      title:title,
      text:textToShow,
      type: 'success',
      timer: timeInMS
    })
  }
  errorAlert(title,textToShow,timeInMS?){
    // if(!timeInMS)timeInMS=1500;
    Swal({
      type: 'error',
      title: title,
      text: textToShow,
      showConfirmButton: true,
      showCloseButton:true,
      timer: timeInMS
    })
  }
  DeleteRecord(title,textToShow){
    swal({
      type: 'warning',
      title:title,
      text:textToShow,
      showCancelButton: true,
      confirmButtonColor: '#3085d6', 
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    })
  }
  Deletedsuccess(){
    Swal(
      'Deleted!',
      'Your file has been deleted.',
      'success'
    )
  }
  loginSuccess(){
    const toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      animation:true,
      width: '100%',
      // background:'red',
      timer: 5000
    });
    toast({
      type: 'success',
      title: 'Signed in Successfully'
    })
  }
  logoutSuccess(){
    const toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      animation:true,
      width: '100%',
      // background:'red',
      timer: 5000
    });
    toast({
      type: 'success',
      title: 'Logged Out Successfully'
    })
  }
  }
  

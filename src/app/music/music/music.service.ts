import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class MusicService {

  constructor(private http: HttpClient) { }

  locationapi(){
    
    return this.http.get(environment.apiUrl +"/musicLocations");
  }

  areaapi(id){
    
    return this.http.get(environment.apiUrl +"/musicAreas/"+id);
  }
  musicUsers(id,data){
    
    return this.http.get(environment.apiUrl +"/musicClientsget/"+id,{params:data});
  }
  musicUserscountsAll(){
    
    return this.http.get(environment.apiUrl +"/musicClientsAllCount");
  }

  musicUserscountAllMainAreas(){
    
    return this.http.get(environment.apiUrl +"/musicClientsCountAreas");
  }

  musicUserscountAllDist(){
    
    return this.http.get(environment.apiUrl +"/musicClientsCountDist");
  }

  musicUserscountAllAreas(id){
    
    return this.http.get(environment.apiUrl +"/musicClientsCount/"+id);
  }
  musicUserscountAllAreasover(id){
    
    return this.http.get(environment.apiUrl +"/musicClientsAreas/"+id);
  }
  babycaresAddsOnea(id){

    return this.http.get(environment.apiUrl +"/musicAddsOneGeta/"+id);
  }
  babycaresAddsTwoa(id){
    
    return this.http.get(environment.apiUrl +"/musicAddsTwoGeta/"+id);
  }
  babycaresAddsThreea(id){
    
    return this.http.get(environment.apiUrl +"/musicAddsThreeGeta/"+id);
  }
  babycaresAddsFoura(id){
    
    return this.http.get(environment.apiUrl +"/musicAddsFourGeta/"+id);
  }
   
  babycaresAddsOnel(id){
    
    return this.http.get(environment.apiUrl +"/musicAddsOneLocGet/"+id);
  }
  babycaresAddsTwol(id){
    
    return this.http.get(environment.apiUrl +"/musicAddsTwoLocGet/"+id);
  }
  babycaresAddsThreel(id){
    
    return this.http.get(environment.apiUrl +"/musicAddsThreeGetLoc/"+id);
  }
  babycaresAddsFourl(id){
    
    return this.http.get(environment.apiUrl +"/musicAddsFourLocGet/"+id);
  }

  babycaresAddsFivel(id){
    
    return this.http.get(environment.apiUrl +"/musicAddsFiveLocGet/"+id);
  }
}
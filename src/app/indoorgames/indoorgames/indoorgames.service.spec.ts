import { TestBed, inject } from '@angular/core/testing';

import { IndoorgamesService } from './indoorgames.service';

describe('IndoorgamesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IndoorgamesService]
    });
  });

  it('should be created', inject([IndoorgamesService], (service: IndoorgamesService) => {
    expect(service).toBeTruthy();
  }));
});

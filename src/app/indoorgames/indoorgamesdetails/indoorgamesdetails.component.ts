import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IndoorgamesdetailsService } from './indoorgamesdetails.service';

@Component({
  selector: 'app-indoorgamesdetails',
  templateUrl: './indoorgamesdetails.component.html',
  styleUrls: ['./indoorgamesdetails.component.css']
})
export class IndoorgamesdetailsComponent implements OnInit {

  public indoorClients:any
  public ids:any
  public servicesOneGet:any
public servicesOneGetc:any
public servicesId:any
public imageFiles:any
public serviceGameName:any
public servicesOneGets:any
public servicesTwoGet:any
public servicesTwoGetc:any
public servicesIds:any
public serviceGameNames:any
public openshareid:any
public commentspostsId:any
public viewcommentsid:any
public clientUpdates:any
public replyid:any
public clientUpdatesCount:any
 public getUsersComments:any
public getUsersCommentsCounts:any
public share:boolean=false
public comments:boolean=false
public viewcomments:boolean=false
public replyfrom:boolean=false 
public replies:any
public fecilitiesGetc:any
public fecilitiesGet:any

postComments={
  "descriptions":""
}
commentspost={
  "name":"",
  "email":"",
  "contactNo":"",
  "leaveComment":"",
    }
  constructor(private router:Router,private route:ActivatedRoute,private indoor:IndoorgamesdetailsService) { }
 
  ngOnInit() {
    this.individualdata()
    this.usersDetails()
   this.servicesOne()
   this.servicesOnec()
   this.clientsUpdates()
   this.clientsUpdatesCount()
   this.fecilities()
   this.fecilitiesc()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];

}

usersDetails(){ 
this.indoor.usersDetails(this.ids).subscribe((res)=>{
this.indoorClients=res

})
}
servicesOne(){ 
  this.indoor.servicesOne(this.ids).subscribe((res)=>{
  this.servicesOneGet=res 
  })
  }
  ServicesOneClick(ser){
    this.servicesIds=ser._id
    this.serviceGameNames=ser.gameName 
    this.servicesTwo()
    this.servicesTwoc()
  }
  servicesOnes(){ 
    this.indoor.servicesOne(this.ids).subscribe((res)=>{
    this.servicesOneGets=res 
    var id=this.servicesOneGets[0]
     })
    }
  servicesOnec(){ 
    this.indoor.servicesOnec(this.ids).subscribe((res)=>{
    this.servicesOneGetc=res
})
    }

    servicesTwo(){ 
      this.indoor.servicesTwo(this.servicesIds).subscribe((res)=>{
      this.servicesTwoGet=res
      })
      }
  
      servicesTwoc(){ 
        this.indoor.servicesTwoc(this.servicesIds).subscribe((res)=>{
        this.servicesTwoGetc=res
         
        })
        }
    // ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
 
}
 clientsUpdates(){   
  this.indoor.clientsUpdatesGet(this.ids).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.indoor.clientsUpdatesGetCounts(this.ids).subscribe((res)=>{
    this.clientUpdatesCount=res
  })
 }

 commentsPoststoClints(){   
  this.indoor.commentsPoststoClints(this.commentspostsId,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.indoor.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.indoor.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.indoor.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    })
 }


  // ***********0verallcomments************
  overallcomment(){
       
     
    this.indoor.overallscommentspost(this.ids,this.commentspost).subscribe((res)=>{
     
     this.commentspost.name="",
     this.commentspost.contactNo="",
     this.commentspost.email="",
     this.commentspost.leaveComment=""
    })
  }

  fecilities(){ 
    this.indoor.fecilities(this.ids).subscribe((res)=>{
    this.fecilitiesGet=res
    })
    }

    fecilitiesc(){ 
      this.indoor.fecilitiesc(this.ids).subscribe((res)=>{
      this.fecilitiesGetc=res
       
      })
      }

}
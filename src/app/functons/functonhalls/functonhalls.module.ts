import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
 import { FunctonhallsComponent } from './functonhalls.component';
import { FunctionhallbriefService } from './functionhallbrief.service';



const routes:Routes=[
  {path:'hallSelect/:_id/:name',component:FunctonhallsComponent},

]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    FunctonhallsComponent,

  ],
  providers: [FunctionhallbriefService],
})
export class FunctonhallsModule { }

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
 import { FunctionhallbriefService } from './functionhallbrief.service';

@Component({
  selector: 'app-functonhalls',
  templateUrl: './functonhalls.component.html',
  styleUrls: ['./functonhalls.component.css']
})
export class FunctonhallsComponent implements OnInit {
  public clients:any
  public ids:any
  public mainuser:any
  public catdeatils:any
  public locationsgets:any
  public areasAll:any
  public locationName:any
  public idss:any
  public selectAreaId:any
  public selectArea:any
  public subsArea:any
  public areasCount:any
  public catAllCont:any
  public mensC:any
  public womensCo:any
  public searchString:any
  public banquietCount:any
  public funHall:any
 // **********************************************************

 public imageAddsOne:any
 public babycaresAddsOnes:any
  
 public babycaresAddsFours:any
 public imageAddsFour:any
 public imageAddsThree:any
 public imageAddsTwo:any
 public babycaresAddsThrees:any
 public babycaresAddsTwos:any
 
 
 public babycaresAddsFivesLoc:any
 public imageAddsFiveLoc:any
 public imageAddsFourLoc:any
 public babycaresAddsFoursLoc:any
 public imageAddsThreeLoc:any
 public babycaresAddsThreeLoc:any
 public imageAddsTwoLoc:any
 public babycaresAddsTwoLoc:any
 public imageAddsOneLoc:any
 public babycaresAddsOneLoc:any


  constructor(private halls:FunctionhallbriefService,private route:ActivatedRoute) { }
  

  ngOnInit() {
    this.individualdata()
    this.usersDetails()
    this.getlocations()   
    this.catContCounts()
    this.funHallCounts()
    this.banquitCounts()
    this.catContCounts()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];
console.log(this.ids);
}


usersDetails(){ 
  this.halls.individualcat(this.ids).subscribe((res)=>{
    this.catdeatils=res
   
    
      })
}

      // *********************** locations ****************
      
         getlocations(){
           this.halls.locationsget(this.ids).subscribe((res)=>{
           this.locationsgets=res
            var id =this.locationsgets[0];
            this.locations(id)
              console.log(res);
              
           })  }


           locations(get?){
             this.locationName=get.locations
             this.idss=get._id
              this.allAreas()

      this.babycaressareasAddsOneLoc()
      this.babycaressareasAddsTwoLoc()
      this.babycaressareasAddsThreeLoc()
      this.babycaressareasAddsFourLoc()
      this.babycaressareasAddsFiveLoc()
           }

 // ************************************************************************************************************************************
 
//  **********************************area *************************************

allAreas(){
        
this.halls.areaapi(this.idss).subscribe((res)=>{
 this.areasAll=res
 var id =this.areasAll[0];
 
 
 this.selectedAreas(id)
 
})

}

selectedAreas(result){
this.selectAreaId=result._id
this.selectArea=result.area

this.clientDetails()
this.areasCounts()
this.clientDetailsCountSubArea()

this.babycaressareasAddsOnea()
this.babycaressareasAddsTwoa()
this.babycaressareasAddsThreea()
this.babycaressareasAddsFoura()
}

// ************************************************************************

  clientDetails(){ 
    var data:any =  {}
    if(this.searchString){
      data.search=this.searchString
      }
    this.halls.Client(this.selectAreaId,data).subscribe((res)=>{
    this.clients=res
     })
    }
    searchFilter(){
      this.clientDetails()
    }
    clientDetailsCountSubArea(){ 
      this.halls.subArea(this.selectAreaId).subscribe((res)=>{
      this.subsArea=res
        
      })
      }
      areasCounts(){ 
        this.halls.Area(this.selectAreaId).subscribe((res)=>{
        this.areasCount=res
          
        })
        }
        funHallCounts(){ 
          this.halls.funHall().subscribe((res)=>{
          this.funHall=res
         

          })
          }
          banquitCounts(){ 
            this.halls.banquiet().subscribe((res)=>{
            this.banquietCount=res
           
  
            })
            }
          catContCounts(){ 
            this.halls.catCount().subscribe((res)=>{
            this.catAllCont=res
             
              
            })
            }

// ****************************************************

babycaressareasAddsOnea(){
  this.halls.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
     console.log(res);
     
   })
}
babycaressareasAddsTwoa(){
  this.halls.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
  
   })
}
babycaressareasAddsThreea(){
  this.halls.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
  
   })
}

babycaressareasAddsFoura(){
  this.halls.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res

   })
}

babycaressareasAddsOneLoc(){
  this.halls.babycaresAddsOnel(this.idss).subscribe((res)=>{
     this.babycaresAddsOneLoc=res 
   })
}
babycaressareasAddsTwoLoc(){
  this.halls.babycaresAddsTwol(this.idss).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
    
   })
}
babycaressareasAddsThreeLoc(){
  this.halls.babycaresAddsThreel(this.idss).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
     
   })
}
babycaressareasAddsFourLoc(){
  this.halls.babycaresAddsFourl(this.idss).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
     
   })
}
babycaressareasAddsFiveLoc(){
  this.halls.babycaresAddsFivel(this.idss).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
     
   })
}

}

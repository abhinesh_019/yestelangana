import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
 import { environment } from '../../../environments/environment.prod';
    
@Injectable()
export class FunctionhallbriefService {

  constructor(private http: HttpClient) { }
  individualcat(id){
    
    return this.http.get(environment.apiUrl +"/CategoriesIds/"+id);
  }  

  locationsget(id){
    
    return this.http.get(environment.apiUrl +"/locations/"+id);
  } 
  areaapi(id){
    
    return this.http.get(environment.apiUrl +"/Areas/"+id);
  } 


  Client(id,data){
    
    return this.http.get(environment.apiUrl +"/hclientsget/"+id,{params:data});
  }
  subArea(id){
    
    return this.http.get(environment.apiUrl +"/clientsAreas/"+id);
  }

  Area(id){
    
    return this.http.get(environment.apiUrl +"/clientsCount/"+id);
  }

  funHall(){
    
    return this.http.get(environment.apiUrl +"/UsersLocationsFunctionsHalls");
  }
  banquiet(){
    
    return this.http.get(environment.apiUrl +"/UsersLocationsBanquietHalls");
  }
  catCount(){
    
    return this.http.get(environment.apiUrl +"/ClientsgetLocationsCat");
  }



  babycaresAddsOnea(id){

    return this.http.get(environment.apiUrl +"/functionalHallAddsOneGeta/"+id);
  }
  babycaresAddsTwoa(id){
    
    return this.http.get(environment.apiUrl +"/functionalHallAddsTwoGeta/"+id);
  }
  babycaresAddsThreea(id){
    
    return this.http.get(environment.apiUrl +"/functionalHallAddsThreeGeta/"+id);
  }
  babycaresAddsFoura(id){
    
    return this.http.get(environment.apiUrl +"/functionalHallAddsFourGeta/"+id);
  }
   
  babycaresAddsOnel(id){
    
    return this.http.get(environment.apiUrl +"/functionalHallAddsOneLocGet/"+id);
  }
  babycaresAddsTwol(id){
    
    return this.http.get(environment.apiUrl +"/functionalHallAddsTwoLocGet/"+id);
  }
  babycaresAddsThreel(id){
    
    return this.http.get(environment.apiUrl +"/functionalHallAddsThreeGetLoc/"+id);
  }
  babycaresAddsFourl(id){
    
    return this.http.get(environment.apiUrl +"/functionalHallAddsFourLocGet/"+id);
  }
  babycaresAddsFivel(id){
    
    return this.http.get(environment.apiUrl +"/functionalHallAddsFiveLocGet/"+id);
  }
   

}

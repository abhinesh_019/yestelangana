import { Component, OnInit } from '@angular/core';
import { FunctonhallcatService } from './functonhallcat.service';

@Component({
  selector: 'app-function-hall-catageries',
  templateUrl: './function-hall-catageries.component.html',
  styleUrls: ['./function-hall-catageries.component.css']
})
export class FunctionHallCatageriesComponent implements OnInit {

  public catList:any
  
  constructor(private fh:FunctonhallcatService) { }
   

  ngOnInit() {
    this.getcatagories()
    
  }
  
  getcatagories(){
    this.fh.catapigetdetails().subscribe((res)=>{
  this.catList=res
  console.log(this.catList);
   
          })  }
         
     
}


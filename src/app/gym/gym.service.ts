import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment.prod';
 
@Injectable()

export class GymService {

  constructor(private http: HttpClient) { }
 
  individualcat(id){
    return this.http.get(environment.apiUrl +"/gymCategories");

  }
    locationsget(){
       return this.http.get(environment.apiUrl +"/gymlocations");
                   }
                   areaapi(id){
    
                    return this.http.get(environment.apiUrl +"/gymAreas/"+id);
                  } 
                  users(id,data){
    
                    return this.http.get(environment.apiUrl +"/gymClientsget/"+id,{params:data});
                  }   
                  gymuserscountstot(){
    
                    return this.http.get(environment.apiUrl +"/gymClientsAllCount");
                  }
                  
                  gymuserscountstotSAreas(id){
    
                    return this.http.get(environment.apiUrl +"/gymClientsCount/"+id);
                  }    
                  gymuserscountstotSAreasNames(id){
                    return this.http.get(environment.apiUrl +"/gymClientsAreas/"+id);
                  }  
                 
                  gymuserscountsAndAreasNames(id){
                    
                    return this.http.get(environment.apiUrl +"/gymClientsAreasAll/"+id);
                  }
                  // **************************************** catagerie ********************
gymusersCat(id){
                    
  return this.http.get(environment.apiUrl +"/gymCategorie/"+id);
}

// ***********************************************************


// ********************************** our services *************************

userServices(id){
                    
  return this.http.get(environment.apiUrl +"/gymServices/"+id);
}
userServicesind(id){
                    
  return this.http.get(environment.apiUrl +"/gymServicesind/"+id);
}
userServicesCount(id){
                    
  return this.http.get(environment.apiUrl +"/gymServicesCount/"+id);
}
// *********************************************************
// ********************************** equipments *************************

equipmentsget(id){
                    
  return this.http.get(environment.apiUrl +"/gymEquipments/"+id);
}
equipmentsgetCount(id){
                    
  return this.http.get(environment.apiUrl +"/gymEquipmentsCount/"+id);
}
equipmentsgetInd(id){
                    
  return this.http.get(environment.apiUrl +"/gymEquipmentsind/"+id);
}
// *********************************************************
// ********************************** fecilitites *************************

fecilitiesget(id){
                    
  return this.http.get(environment.apiUrl +"/gymFicilties/"+id);
}
fecilitiesgetCount(id){
                    
  return this.http.get(environment.apiUrl +"/gymFiciltiesCount/"+id);
}
 
// *********************************************************
// ********************************** users details *************************

usersdetailsget(id){
                    
  return this.http.get(environment.apiUrl +"/gymusers/"+id);
}
usersdetailsgetind(id){
                    
  return this.http.get(environment.apiUrl +"/gymusersid/"+id);
}
usersdetailsgetCount(id){
                    
  return this.http.get(environment.apiUrl +"/gymusersCount/"+id);
}
gymusersonly(id){
                    
  return this.http.get(environment.apiUrl +"/gymClientsGetind/"+id);
}
// *********************************************************


///////////////////////////////////  clients  updates    //////////////////////////////////////////



                 
     
                  clientsUpdatesGet(id){
    
                    return this.http.get(environment.apiUrl +"/gymupdatesget/"+id);
                  }
                  clientsUpdatesCnts(id){
                    
                    return this.http.get(environment.apiUrl +"/gymupdatesgetCounts/"+id);
                  }
                  clientsUpdatesGetCounts(id){
                    
                    return this.http.get(environment.apiUrl +"/gymupdatesgetCounts/"+id);
                  }
                  commentsPoststoClints(id,data){
                    
                    return this.http.post(environment.apiUrl +"/gymupdatesCommentspost/"+id,data);
                  }
                  commentsGettoClints(id){
                    
                    return this.http.get(environment.apiUrl +"/gymupdatesCommentsget/"+id);
                  }
                  commentsGettoClintsCounts(id){
                    
                    return this.http.get(environment.apiUrl +"/gymupdatesCommentsgetcounts/"+id);
                  }
                  replyFromCommentesGet(id){
                    
                    return this.http.get(environment.apiUrl +"/gymupdatesCommentReplysGet/"+id);
                  }
                
                  overallscommentspost(id,data){
                    
                    return this.http.post(environment.apiUrl +"/gymeuserscomments/"+id,data);
                  }
                  // ********************************
                
                  babycaresAddsOnea(id){

                    return this.http.get(environment.apiUrl +"/gymAddsOneGeta/"+id);
                  }
                  babycaresAddsTwoa(id){
                    
                    return this.http.get(environment.apiUrl +"/gymAddsTwoGeta/"+id);
                  }
                  babycaresAddsThreea(id){
                    
                    return this.http.get(environment.apiUrl +"/gymAddsThreeGeta/"+id);
                  }
                  babycaresAddsFoura(id){
                    
                    return this.http.get(environment.apiUrl +"/gymAddsFourGeta/"+id);
                  }
                   
                  babycaresAddsOnel(id){
                    
                    return this.http.get(environment.apiUrl +"/gymAddsOneLocGet/"+id);
                  }
                  babycaresAddsTwol(id){
                    
                    return this.http.get(environment.apiUrl +"/gymAddsTwoLocGet/"+id);
                  }
                  babycaresAddsThreel(id){
                    
                    return this.http.get(environment.apiUrl +"/gymAddsThreeGetLoc/"+id);
                  }
                  babycaresAddsFourl(id){
                    
                    return this.http.get(environment.apiUrl +"/gymAddsFourLocGet/"+id);
                  }
                
                  babycaresAddsFivel(id){
                    
                    return this.http.get(environment.apiUrl +"/gymAddsFiveLocGet/"+id);
                  }
                   
                }
                 ////////////////////////////////////////////////////////////////////////////
                
                
                 
 
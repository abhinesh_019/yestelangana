import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeautyparlourComponent } from './beautyparlour.component';

describe('BeautyparlourComponent', () => {
  let component: BeautyparlourComponent;
  let fixture: ComponentFixture<BeautyparlourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeautyparlourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeautyparlourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

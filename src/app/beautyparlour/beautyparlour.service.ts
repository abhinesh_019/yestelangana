import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment.prod';
   

@Injectable()

export class BeautyparlourService {

  constructor(private http: HttpClient) { }

  catapigetdetails(){
    
    return this.http.get(environment.apiUrl +"/parlourCategories");
  }       
  
  individualcat(id){
    return this.http.get(environment.apiUrl +"/parlourCategorie/"+id);

  }
   locationsget(id){
    return this.http.get(environment.apiUrl +"/parlourlocations/"+id);
                }
                areaapi(id){
     
                 return this.http.get(environment.apiUrl +"/beautyParlourAreas/"+id);
               }   

               
               Client(id,data){
    
    return this.http.get(environment.apiUrl +"/bpClientsget/"+id,{params:data});
  }
  subArea(id){
    
    return this.http.get(environment.apiUrl +"/bpClientsgetCounts/"+id);
  }
  Area(id){
    return this.http.get(environment.apiUrl +"/bpClientsgetAreaCounts/"+id);
  }


  clientsSeperate(id){
    return this.http.get(environment.apiUrl +"/bpClientsgetId/"+id);
  }

  mens(){
    
    return this.http.get(environment.apiUrl +"/bpUsersLocationsMens");
  }
  womens(){
    
    return this.http.get(environment.apiUrl +"/bpUsersLocationsWomens");
  }
  catCount(){
    
    return this.http.get(environment.apiUrl +"/bpClientsgetLocationsCatmens");
  }

  // ********************************************** Services  ********************************
  services(id){
    
    return this.http.get(environment.apiUrl +"/clientServices/"+id);
  }
  servicesCounts(id){
    
    return this.http.get(environment.apiUrl +"/clientServicesCounts/"+id);
  }

  overallscommentspost(id,data){
     
    
    return this.http.post(environment.apiUrl +"/bpGeneralCommentsPosts/"+id,data);
    
  }
 
  updatesget(id) {
    return this.http.get(environment.apiUrl + "/bpupdates/"+id);
  }
  updatesgetCount(id) {
    return this.http.get(environment.apiUrl + "/updatesCount/"+id);
  }

  commentsgetupdates(id) {
    return this.http.get(environment.apiUrl + "/bpupdatesComments/"+id);
  }
  commentsgetupdatesCount(id) {
    return this.http.get(environment.apiUrl + "/updatesCommentsCount/"+id);
  }
  commentspostupdates(id,data) {
    return this.http.post(environment.apiUrl +"/updatesCommentsPostes/"+id,data);
  }

  commentsgetupdatesReply(id) {
    return this.http.get(environment.apiUrl + "/bpupdatesCommentsReply/"+id);
  }
  equipnmentsGets(id) {
    return this.http.get(environment.apiUrl + "/beaautyParloureEquipmentsGet/"+id);
  }
  equipnmentsGetsCounts(id) {
    return this.http.get(environment.apiUrl + "/beaautyParlourEquipmentsGetCounts/"+id);
  }

  trainningGets(id) {
    return this.http.get(environment.apiUrl + "/bpTrainingGet/"+id);
  }
  trainningGetsCounts(id) {
    return this.http.get(environment.apiUrl + "/bpTrainingGetCounts/"+id);
  }
  totalCustomersGet(id) {
    return this.http.get(environment.apiUrl + "/beaautyParlourTotalCustomersGet/"+id);
  }
  totalCustomersGetCounts(id) {
    return this.http.get(environment.apiUrl + "/beaautyParloureTotalCustomersCounts/"+id);
  }
  fecilitesGet(id) {
    return this.http.get(environment.apiUrl + "/beaautyParlourecitiesGet/"+id);
  }
  fecilitesGetsCounts(id) {
    return this.http.get(environment.apiUrl + "/beaautyParlourecitiesGetCounts/"+id);
  }

  babycaresAddsOnea(id){

    return this.http.get(environment.apiUrl +"/beutyParlourAddsOneGeta/"+id);
  }
  babycaresAddsTwoa(id){
    
    return this.http.get(environment.apiUrl +"/beutyParlourAddsTwoGeta/"+id);
  }
  babycaresAddsThreea(id){
    
    return this.http.get(environment.apiUrl +"/beutyParlourAddsThreeGeta/"+id);
  }
  babycaresAddsFoura(id){
    
    return this.http.get(environment.apiUrl +"/beutyParlourAddsFourGeta/"+id);
  }
   
  babycaresAddsOnel(id){
    
    return this.http.get(environment.apiUrl +"/beutyParlourAddsOneLocGet/"+id);
  }
  babycaresAddsTwol(id){
    
    return this.http.get(environment.apiUrl +"/beutyParlourAddsTwoLocGet/"+id);
  }
  babycaresAddsThreel(id){
    
    return this.http.get(environment.apiUrl +"/beutyParlourAddsThreeGetLoc/"+id);
  }
  babycaresAddsFourl(id){
    
    return this.http.get(environment.apiUrl +"/beutyParlourAddsFourLocGet/"+id);
  }

  babycaresAddsFivel(id){
    
    return this.http.get(environment.apiUrl +"/beutyParlourAddsFiveLocGet/"+id);
  }
   
 }


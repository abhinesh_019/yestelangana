import { TestBed, inject } from '@angular/core/testing';

import { BeautyparlourService } from './beautyparlour.service';

describe('BeautyparlourService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BeautyparlourService]
    });
  });

  it('should be created', inject([BeautyparlourService], (service: BeautyparlourService) => {
    expect(service).toBeTruthy();
  }));
});

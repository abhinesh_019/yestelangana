import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
 import { ParlourdetailsComponent } from './parlourdetails.component';
import { BeautyparlourService } from '../beautyparlour.service';



const routes:Routes=[{path:'parlourdetails/:_id/:name',component:ParlourdetailsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    ParlourdetailsComponent,

  ],
  providers: [BeautyparlourService],
})
export class ParlourdetailsModule { }

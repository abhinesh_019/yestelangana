import { Component, OnInit } from '@angular/core';
import { BeautyparlourService } from '../beautyparlour.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-parlourdetails',
  templateUrl: './parlourdetails.component.html',
  styleUrls: ['./parlourdetails.component.css']
})
export class ParlourdetailsComponent implements OnInit {
public ids:any
public clientsInd:any
public clientsServices:any
public clientsServicesCount:any
public updatesIds:any
public show:any
public showes:any
public shows:any
public showess:any
public selectedblogIds:any
public adminsupdateCommentsReply:any
public commentsUpdatesId:any
public adminupdates:any
public adminsupdateCountsall:any
public adminsupdateComments:any
public adminsupdateCommentsCount:any
public equipnmentsGet:any
public equipnmentsGetsCount:any
public trainningGetsCount:any
public trainningGet:any
public totalCustomersGetCount:any
public totalCustomersGets:any
public fecilitesGetsCount:any
public fecilitesGets:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any

updatesCommentsdata={
  "descriptions":""
}

comments={
  name:"",
  email:"",
  contactNo:"",
  leaveComment:""
}


  constructor(private route:ActivatedRoute,private parlour:BeautyparlourService) { }

  ngOnInit() {
  this.individualdata()
 this.ServicesDetails()
 this.ServicesDetailsCounts()
 this.getupdates()
 this.getupdatesCount()
 this.equipnmentsGets()
 this.equipnmentsGetsCounts()
 this.trainningGets()
 this.trainningGetsCounts()
 this.totalCustomersGet()
 this.totalCustomersGetCounts()
 this.fecilitesGet()
 this.fecilitesGetsCounts()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];
console.log(this.ids);
this.clientDetailsIndividuial()
}
clientDetailsIndividuial(){ 
  this.parlour.clientsSeperate(this.ids).subscribe((res)=>{
  this.clientsInd=res
    console.log(res);
    
  })
  }
  ServicesDetails(){ 
    this.parlour.services(this.ids).subscribe((res)=>{
    this.clientsServices=res
  
    })
    }
    ServicesDetailsCounts(){ 
      this.parlour.servicesCounts(this.ids).subscribe((res)=>{
      this.clientsServicesCount=res
        console.log(this.clientsServicesCount);
        
      })
      }

// *************get updates****************
getupdates(){
 
  this.parlour.updatesget(this.ids).subscribe((res)=>{
    this.adminupdates=res
    
    console.log(this.adminupdates);
  
  })
       }
       getupdatesCount(){
 
        this.parlour.updatesgetCount(this.ids).subscribe((res)=>{
          this.adminsupdateCountsall=res
          
          console.log(this.adminsupdateCountsall);
        
        })
             }

showicons(){
  this.show=!this.show
  this.shows=false
}
showcomments(item?){
  this.show=false
  this.shows=!this.shows
   this.updatesIds=item._id
    
   this.comm()
 
        }
        updatestoAdmincommentsget(result?){
          this.showes=!this.showes
                   this.commentsUpdatesId=result
                   console.log(result);
                   this.comm()
   this.commCount()
                    }
       comm(){
        this.parlour.commentsgetupdates(this.commentsUpdatesId).subscribe((res)=>{
            this.adminsupdateComments=res
            console.log(this.adminsupdateComments);
            
           
          })
                } 
                commCount(){
                  this.parlour.commentsgetupdatesCount(this.commentsUpdatesId).subscribe((res)=>{
                      this.adminsupdateCommentsCount=res
                      console.log(this.adminsupdateCommentsCount);
                      
                     
                    })
                          }
         updatestoadmincomments(){
         
          this.parlour.commentspostupdates(this.updatesIds,this.updatesCommentsdata).subscribe((res)=>{
           
           
           console.log(res);
           
          })
          this.updatesCommentsdata.descriptions=""
           
               }
               updatesAdminReply(items){
                this.showess=!this.showess
                this.selectedblogIds=items
                this.parlour.commentsgetupdatesReply(this.selectedblogIds).subscribe((res)=>{
                  this.adminsupdateCommentsReply=res
                 console.log(res);
                 
                })
              } 
// ***********0verallcomments************
overallcomment(){
 
  this.parlour.overallscommentspost(this.ids,this.comments).subscribe((res)=>{
    
  })
 
  this.comments.name="",
  this.comments.contactNo="",
  this.comments.email="",
  this.comments.leaveComment=""
 
 
 }
 equipnmentsGets(){ 
  this.parlour.equipnmentsGets(this.ids).subscribe((res)=>{
  this.equipnmentsGet=res
    console.log(this.equipnmentsGet);
    
  })
  }

  equipnmentsGetsCounts(){ 
    this.parlour.equipnmentsGetsCounts(this.ids).subscribe((res)=>{
    this.equipnmentsGetsCount=res
      console.log(this.equipnmentsGetsCount);
      
    })
    }

    trainningGets(){ 
      this.parlour.trainningGets(this.ids).subscribe((res)=>{
      this.trainningGet=res
        console.log(this.trainningGet);
        
      })
      }
    
      trainningGetsCounts(){ 
        this.parlour.trainningGetsCounts(this.ids).subscribe((res)=>{
        this.trainningGetsCount=res
          console.log(this.trainningGetsCount);
          
        })
        }

        totalCustomersGet(){ 
          this.parlour.totalCustomersGet(this.ids).subscribe((res)=>{
          this.totalCustomersGets=res
            console.log(this.totalCustomersGets);
            
          })
          }
        
          totalCustomersGetCounts(){ 
            this.parlour.totalCustomersGetCounts(this.ids).subscribe((res)=>{
            this.totalCustomersGetCount=res
              console.log(this.totalCustomersGetCount);
              
            })
            }

            fecilitesGet(){ 
              this.parlour.fecilitesGet(this.ids).subscribe((res)=>{
              this.fecilitesGets=res
                console.log(this.fecilitesGets);
                
              })
              }
            
              fecilitesGetsCounts(){ 
                this.parlour.fecilitesGetsCounts(this.ids).subscribe((res)=>{
                this.fecilitesGetsCount=res
                  console.log(this.fecilitesGetsCount);
                  
                })
                }
                }

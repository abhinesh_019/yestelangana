import { Component, OnInit } from '@angular/core';
import { BeautyparlourService } from '../beautyparlour.service';
  
@Component({
  selector: 'app-beauty-parlour-catageries',
  templateUrl: './beauty-parlour-catageries.component.html',
  styleUrls: ['./beauty-parlour-catageries.component.css']
})
export class BeautyParlourCatageriesComponent implements OnInit {

  public catList:any
  
  constructor(private parlour:BeautyparlourService) { }
   

  ngOnInit() {
    this.getcatagories()
    
  }
  
  getcatagories(){
    this.parlour.catapigetdetails().subscribe((res)=>{
  this.catList=res
  console.log(this.catList);
   
          })  }
         
     
}

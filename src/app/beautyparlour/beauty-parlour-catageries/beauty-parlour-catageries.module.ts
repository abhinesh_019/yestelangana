import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
 import { ParlourdetailsModule } from '../parlourdetails/parlourdetails.module';
import { BeautyparlourService } from '../beautyparlour.service';
import { BeautyParlourCatageriesComponent } from './beauty-parlour-catageries.component';
import { BeautyparlourModule } from '../beautyparlour/beautyparlour.module';

const routes: Routes = [
  {
    path: '', component: BeautyParlourCatageriesComponent, children:
      [
      { path: 'parlourdetails/:_id/:name', loadChildren: 'app/beautyparlour/parlourdetails/parlourdetails.module#ParlourdetailsModule'},
      { path: 'parlourselect/:_id/:name', loadChildren: 'app/beautyparlour/beautyparlour/beautyparlour.module#BeautyparlourModule'},

      ]
  }]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
     SharedModule,
      BeautyparlourModule,
      ParlourdetailsModule
  ],
  declarations: [
    BeautyParlourCatageriesComponent,
  ],
  providers:[BeautyparlourService]
})

export class BeautyParlourCatageriesModule { }

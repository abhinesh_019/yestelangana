import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminmarriagebuerauService } from './adminmarriagebuerau.service';

@Component({
  selector: 'app-adminmarriagebuerau',
  templateUrl: './adminmarriagebuerau.component.html',
  styleUrls: ['./adminmarriagebuerau.component.css']
})
export class AdminmarriagebuerauComponent implements OnInit {
  public showmains:boolean=false
  public updatesShows:boolean=false
  public viewupdatesShows:boolean=false
  public onlyCommentsShows:boolean=false
  public ImagesShows:boolean=false
  public ProductsShows:boolean=false
  public ProductsViewShows:boolean=false

  public usersID:any
public marriageDetailsClients:any
public viewsProfiles:any
public viewsNoti:any
public cateCat:any
public castecatName:any
public castecatId:any
public msincateCat:any
public maincastecatId:any
public maincastecatName:any
public mainc:any
public subc:any
public smsincateCat:any
public smaincastecatId:any
public smaincastecatName:any
public imageName:any
public trackRec:any
public trackRecc:any
public imageNameupdates:any
public videoName:any
public commentspostsId:any
public viewcommentsid:any
public clientUpdates:any
public replyid:any
public clientUpdatesCount:any
 public getUsersComments:any
public getUsersCommentsCounts:any
public share:boolean=false
public comments:boolean=false
public viewcomments:boolean=false
public replyfrom:boolean=false 
public replies:any
public openshareid:any
public onlyCommC:any
public onlyComm:any

postComments={
  "descriptions":""
}

formData: FormData = new FormData(); 

ct={
  casteType:""
}
mct={
  MainCaste:"",
 }
 smct={
  subCaste:"",
  subCasteTypes:"",
  relatedTocaste:"",
  trackRecords:"",
  descriptions:"",
 }

 rec={
  groomeName:"", 
  brideName:"", 
  place:"", 
  descriptions:"", 
  images:"",
  casteName:""
 }
 ups={
  images:"",
    title:"",
    description:"",
    viedoes:"",
}
constructor(private route:ActivatedRoute,private mus:AdminmarriagebuerauService) { }
 ngOnInit() {
    this.usersID=localStorage.getItem('ads');
this.usersDetails()
this.casteCatGet()
this.clientsUpdates()
this.clientsUpdatesCount()
this.onlyCommentsC()
this.onlyComments()
  }
  usersDetails(){ 
    this.mus.usersDetails(this.usersID).subscribe((res)=>{
    this.marriageDetailsClients=res
     
    })
    }
    viewProfile(){
      this.viewsProfiles=!this.viewsProfiles
      this.viewsNoti=false
    }
    viewnoti(){
      this.viewsNoti=!this.viewsNoti
      this.viewsProfiles=false
  
    }

    mainMenuShow(){
      this.showmains=!this.showmains
      this.updatesShows=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    updatesShow(){
      this.updatesShows=!this.updatesShows
      this.showmains=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    viewupdatesShow(){
      this.viewupdatesShows=!this.viewupdatesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    onlyCommentsShow(){
      this.onlyCommentsShows=!this.onlyCommentsShows
      this.updatesShows=false
      this.showmains=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    ImagesShow(){
      
      this.ImagesShows=!this.ImagesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    ProductsShow(){
      this.ProductsShows=!this.ProductsShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsViewShows=false
    }
    ProductsViewShow(){
      this.ProductsViewShows=!this.ProductsViewShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
    }
// ********************************************** create caste ***************************** 
    casteCat(){ 
      this.mus.casteCat(this.usersID,this.ct).subscribe((res)=>{
       
      })
      this.ct.casteType=""
      }
      casteCatGet(){ 
        this.mus.casteCatGet(this.usersID).subscribe((res)=>{
        this.cateCat=res
        })
         }

         casteCatClick(c){
this.castecatName=c.casteType
this.castecatId=c._id
this.maincasteCatGet()
         }
         // ************************************************************************* 


// ********************************************** main caste ***************************** 
mainCasteCat(){ 
this.mainc={
  "MainCaste":this.mct.MainCaste,
  "casteType":this.castecatName
}
  this.mus.mainCasteCat(this.castecatId,this.mainc).subscribe((res)=>{
     })
  this.ct.casteType=""
  }
  maincasteCatGet(){ 
    this.mus.maincasteCatGet(this.castecatId).subscribe((res)=>{
    this.msincateCat=res
    })
     }

     mainCasteCatClick(c){
this.maincastecatName=c.MainCaste
this.maincastecatId=c._id
 this.subcasteCatGet()
 this.tracksRecords()
 this.tracksRecordsc()
     }
     // ************************************************************************* 

     // ********************************************** sub caste ***************************** 
     subCasteCat(){ 

  this.subc={
    "MainCaste":this.maincastecatName,
    "casteType":this.castecatName,
    "subCaste":this.smct.subCaste,
    "subCasteTypes":this.smct.subCasteTypes,
    "relatedTocaste":this.smct.relatedTocaste,
     "descriptions":this.smct.descriptions,
  }

    this.mus.smainCasteCat(this.maincastecatId,this.subc).subscribe((res)=>{
        })
    this.ct.casteType=""
    }
    subcasteCatGet(){ 
      this.mus.smaincasteCatGet(this.maincastecatId).subscribe((res)=>{
      this.smsincateCat=res
       
      })
       }
  
       subCasteCatClick(c){
           this.smaincastecatName=c.MainCaste
          this.smaincastecatId=c._id
         
                          }
       // ************************************************************************* 
  // ********** Track Records *******
  
  handleFileTrackRecords($event:any,images){
    this.imageName= File = $event.target.files[0];
   }
   
   trackRecords( ){
     this.formData.append('images', this.imageName);
     this.formData.append('descriptions', this.rec.descriptions);
     this.formData.append('groomeName', this.rec.groomeName);
     this.formData.append('brideName', this.rec.brideName);
     this.formData.append('place', this.rec.place);
     this.formData.append('casteName', this.rec.casteName);

     console.log(this.formData)
  
    this.mus.trackRecords(this.maincastecatId,this.formData).subscribe((res)=>{
       console.log('assssss',res);
       })
        this.rec.brideName="",
        this.rec.descriptions="",
        this.rec.images="",
        this.rec.groomeName="",
        this.rec.place="",
        this.rec.casteName=""
  }
  tracksRecords(){
    this.mus.tracksRecords(this.maincastecatId).subscribe((res)=>{
      this.trackRec=res
      console.log(res);
       })
}
tracksRecordsc(){
  this.mus.tracksRecordsc(this.maincastecatId).subscribe((res)=>{
    this.trackRecc=res
    console.log(res);
     })
}

// ***************************************************updades **************************************
handleFileInputUpdates($event:any,images){
  this.imageNameupdates= File = $event.target.files[0];
 
}

handleFileInputUpdatess($event:any,viedoes){
  this.videoName= File = $event.target.files[0];
  this.formData.append(viedoes,this.videoName);
 
 
}

updatesPostData(){
this.formData.append('images',this.imageNameupdates);
this.formData.append('title',this.ups.title);
this.formData.append('description',this.ups.description);
 

  console.log(this.formData)

 this.mus.postsUpdata(this.usersID,this.formData).subscribe((res)=>{
    console.log('assssss',res);
   console.log(this.usersID);
   
     })
     this.ups.title="",
     this.ups.description="",
     this.ups.images="",
     this.ups.viedoes=""
}
  
// ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
  console.log(this.replyid);
  
}
 clientsUpdates(){   
  this.mus.clientsUpdatesGet(this.usersID).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.mus.clientsUpdatesGetCounts(this.usersID).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.mus.commentsPoststoClints(this.replyid,this.postComments).subscribe((res)=>{
    console.log(res);
    console.log(this.replyid);
      })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.mus.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.mus.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.mus.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    })
 }


  // ********************************************************************************
 onlyComments(){   
  this.mus.overallscommentspost(this.usersID).subscribe((res)=>{
    this.onlyComm=res
    console.log(res);
    
    })
 }

 onlyCommentsC(){   
  this.mus.onlyCommentsGetCount(this.usersID).subscribe((res)=>{
    this.onlyCommC=res
    console.log(res);
    
    })
 }
}
  


import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminmarriagebuerauComponent } from './adminmarriagebuerau.component';

describe('AdminmarriagebuerauComponent', () => {
  let component: AdminmarriagebuerauComponent;
  let fixture: ComponentFixture<AdminmarriagebuerauComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminmarriagebuerauComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminmarriagebuerauComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

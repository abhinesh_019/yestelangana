import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../../environments/environment.prod';
 
@Injectable()
export class AdmintutionsService {

  constructor(private http: HttpClient) { }
  individualUsers(id){
     
    return this.http.get(environment.apiUrl +"/tutionshUsersin/"+id);}
    
  postSSC(id,data) {
    return this.http.post(environment.apiUrl + "/tutionshUserSSCp/"+id,data);
  }
  postints(id,data) {
    return this.http.post(environment.apiUrl + "/tutionshUserIntp/"+id,data);
  }
  postDegrees(id,data) {
    return this.http.post(environment.apiUrl + "/tutionshUserDegreep/"+id,data);
  }
 
  ssc(id){
    return this.http.get(environment.apiUrl +"/tutionshUserSSC/"+id);

  }
  int(id){
    return this.http.get(environment.apiUrl +"/tutionshUserInt/"+id);
         }
         deg(id){
          return this.http.get(environment.apiUrl +"/tutionshUserDegree/"+id);
               }
               overallscommentsget(id){
                return this.http.get(environment.apiUrl +"/tutionsgComments/"+id);

               }
               overallscommentsCont(id){
                return this.http.get(environment.apiUrl +"/tutionsgComment/"+id);

               }
               Getupdates(id){
                return this.http.get(environment.apiUrl +"/tutionsUserUpdates/"+id);

               }
               Getupdatescount(id){
                return this.http.get(environment.apiUrl +"/tutionsUserUpdatesCount/"+id);

               }
               postsUpdata(id,data){
                return this.http.post(environment.apiUrl + "/tutionsUserUpdatesp/"+id,data);
                 
              }

  commentsgetupdates(id) {
    return this.http.get(environment.apiUrl + "/tutionsgCommentsUpdatesg/"+id);
  }
  commentsgetupdatesCount(id) {
    return this.http.get(environment.apiUrl + "/tutionsgCommentUpdatesCountg/"+id);
  }
  commentspostupdates(id,data) {
    return this.http.post(environment.apiUrl +"/tutionspCommentsReplyUpdatesp/"+id,data);
  }

  commentsgetupdatesReply(id) {
    return this.http.get(environment.apiUrl + "/tutionsgCommentsReplyUpdatesg/"+id);
  }

}

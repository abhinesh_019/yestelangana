import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
 import { NgModule } from '@angular/core';
 import { SharedModule } from '../../shared/shared.module';
 import { PushNotificationsService } from '/home/abhinesh/Documents/angular5/p&a applications/hostels/gitprojects/yestelangana/src/app/push-notifications.service'
import { AdmintutionsComponent } from './admintutions.component';
import { AdmintutionsService } from './admintutions.service';


const routes:Routes=[{path:'tutionsadmin',component:AdmintutionsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    ],
  declarations: [
    AdmintutionsComponent
],
  providers: [AdmintutionsService,PushNotificationsService],
})
export class AdmintutionsModule { }

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdmintutionsService } from './admintutions.service';

@Component({
  selector: 'app-admintutions',
  templateUrl: './admintutions.component.html',
  styleUrls: ['./admintutions.component.css']
})
export class AdmintutionsComponent implements OnInit {

  public showmains:boolean=false
  public updatesShows:boolean=false
   public onlyCommentsShows:boolean=false
  public ImagesShows:boolean=false
  public SSCShows:boolean=false
  public IntermediateShows:boolean=false
  public DegreeShows:boolean=false
  public viewsProfiles:boolean=false
  public viewsNoti:boolean=false
  public usersID:any
  public mainuser:any
  public sscs:any
  public ints:any
  public degree:any
  public onlyComments:any
  public onlyCommentsCounts:any
  public imageName:any
  public imageName1:any
  public imageName2:any
  public imageName3:any
  public videoName:any
  public usersUpdatesCounts:any
  public adminupdates:any

  public adminsupdateCommentsCountsall:any
  public show:any
  public shows:any
  public showes:any
  public showess:any
  public updatesIds:any
  public commentsUpdatesId:any
  public adminsupdateComments:any
  public selectedblogIds:any
  public adminsupdateCommentsReply:any
  public adminsupdateCommentsCount:any
  

  postupdatedata={
    images:"",
    // images1:"",
    // images2:"",
    // images3:"",
    title:'',
    description:"",
    viedoes:""
  }
  
  ssc={
    "descriptionBasics":"",
    "nameOfTutorsBasics":"",
    "yearOfExperienceBasics":"",
    "descriptionssc":"",
    "nameOfTutorssc":"",
    "yearOfExperiencesssc":"",
   }
   inters={
    descriptionsEnglish:"",
    descriptionsTelugu:"",
    descriptionsHindi:"",
    descriptionsSanskrit:"",
    descriptionsurudhu:"",
    descriptionsArabic:"",
    descriptionsFranch:"",
    descriptionsTamil:"",
    descriptionsKanadam:"",
    descriptionsOriya:"",
    descriptionsMarathi:"",
    descriptionMathametics:"",
    descriptionPhysics:"",
    descriptionChemistry:"",
    descriptionBotony:"",
    descriptionZoology:"",
    descriptionCommerce:"",
    descriptionEconomics:"",
    descriptionCivics:"",
    descriptionHistory:"",
    descriptionPsycology:"",
    descriptionPublicAdminstration:"",
    descriptionLogic:"",
    descriptionSocialogy:"",
    descriptionGeography:"",
    descriptionGeology:"",
    tutorsenglish:"",
    tutorsTelugu:"",
    tutorsHindi:"",
    tutorsSanskrit:"",
    tutorsurudhu:"",
    tutorsArabic:"",
    tutorsFranch:"",
    tutorsTamil:"",
    tutorsKanadam:"",
    tutorsOriya:"",
    tutorsMarathi:"",
    tutorsMathametics:"",
    tutorsPhysics:"",
    tutorsChemistry:"",
    tutorsBotony:"",
    tutorsZoology:"",
    tutorsCommerce:"",
    tutorsEconomics:"",
    tutorsCivics:"",
    tutorsHistory:"",
    tutorsPsycology:"",
    tutorsPublicAdminstration:"",
    tutorsLogic:"",
    tutorsSocialogy:"",
    tutorsGeography:"",
    tutorsGeology:"",
    expenglish:"",
    expTelugu:"",
    expHindi:"",
    expSanskrit:"",
    expurudhu:"",
    expArabic:"",
    expFranch:"",
    expTamil:"",
    expKanadam:"",
    expOriya:"",
    expMarathi:"",
    expMathametics:"",
    expPhysics:"",
    expChemistry:"",
    expBotony:"",
    expZoology:"",
    expCommerce:"",
    expEconomics:"",
    expCivics:"",
    expHistory:"",
    expPsycology:"",
    expPublicAdminstration:"",
    expLogic:"",
    expSocialogy:"",
    expGeography:"",
    expGeology:"",
   }
deg={
  BusinessDescription:"",
    ComputingDescription:"",
    EngineeringDescription:"",
    HumaniliesDescription:"",
    LanguageDescription:"",
    LawDescription:"",
    PsychologyDescription:"",
    ScienceDescription:"",
    BusinessTutors:"",
    ComputingTutors:"",
    EngineeringTutors:"",
    HumaniliesTutors:"",
    LanguageTutors:"",
    LawTutors:"",
    PsychologyTutors:"",
    ScienceTutors:"",
    BusinessYearExp:"",
    ComputingYearExp:"",
    EngineeringYearExp:"",
    HumaniliesYearExp:"",
    LanguageYearExp:"",
    LawYearExp:"",
    PsychologyYearExp:"",
    ScienceYearExp:"",

}

updatesCommentsdata={
  "descriptions":""
}

formData: FormData = new FormData(); 
constructor(private route:ActivatedRoute,private tutions:AdmintutionsService) { }
  
    ngOnInit() {
      this.usersID=localStorage.getItem('ads');
      this.usersDetails()
      this.sscdetails()
      this.intDetaits()
      this.degreeDetaits()
      this.commentsCount()
      this.updadesMain()
      this.updadesMainCount()
    }

    usersDetails(){ 
      this.tutions.individualUsers(this.usersID).subscribe((res)=>{
        this.mainuser=res  
      })
    }
 
    viewProfile(){
      this.viewsProfiles=!this.viewsProfiles
      this.viewsNoti=false
    }
    viewnoti(){
      this.viewsNoti=!this.viewsNoti
      this.viewsProfiles=false
  
    }
    mainMenuShow(){
  this.showmains=!this.showmains
  this.updatesShows=false
  this.onlyCommentsShows=false
  this.ImagesShows=false
  this.SSCShows=false
  this.IntermediateShows=false
  this.DegreeShows=false
    }
    updatesShow(){
      this.updatesShows=!this.updatesShows
      this.showmains=false
      this.onlyCommentsShows=false
      this.ImagesShows=false 
      this.SSCShows=false
      this.IntermediateShows=false
      this.DegreeShows=false
    }
     onlyCommentsShow(){
      this.onlyCommentsShows=!this.onlyCommentsShows
      this.updatesShows=false
      this.showmains=false
      this.ImagesShows=false 
      this.SSCShows=false
      this.IntermediateShows=false
      this.DegreeShows=false

      this.tutions.overallscommentsget(this.usersID).subscribe((res)=>{
      
      
        this.onlyComments=res
         console.log(this.onlyComments);
         
       })
    }

    ImagesShow(){
      this.ImagesShows=!this.ImagesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false 
      this.SSCShows=false
      this.IntermediateShows=false
      this.DegreeShows=false
    }
     
    viewSSCShow(){
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false 
      this.ImagesShows=false
      this.SSCShows=!this.SSCShows
      this.IntermediateShows=false
      this.DegreeShows=false
    }
    viewIntShow(){
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false 
      this.ImagesShows=false
      this.SSCShows=false
      this.IntermediateShows=!this.IntermediateShows
      this.DegreeShows=false
    }
  
    viewDegreeShow(){
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false 
      this.ImagesShows=false
      this.SSCShows=false
      this.IntermediateShows=false
      this.DegreeShows=!this.DegreeShows
    }

    postSSCs(){
      this.tutions.postSSC(this.usersID,this.ssc).subscribe((res)=>{
        
        console.log(res)
        
      })
    }
    sscdetails(){ 
      this.tutions.ssc(this.usersID).subscribe((res)=>{
        this.sscs=res
      })
    }

    postInts(){
      this.tutions.postints(this.usersID,this.inters).subscribe((res)=>{
       })
    }
    postDegree(){
      this.tutions.postDegrees(this.usersID,this.deg).subscribe((res)=>{
         
      })
    }
    

    intDetaits(){ 
      this.tutions.int(this.usersID).subscribe((res)=>{
        this.ints=res 
       })
    }
    
    degreeDetaits(){ 
      this.tutions.deg(this.usersID).subscribe((res)=>{
        this.degree=res 
      })
    }
    commentsCount(){
      this.tutions.overallscommentsCont(this.usersID).subscribe((res)=>{
        this.onlyCommentsCounts=res
          
       })
    }
    updadesMain(){
         this.tutions.Getupdates(this.usersID).subscribe((res)=>{
          this.adminupdates=res
          console.log(this.adminupdates);
         }) 
    }
    updadesMainCount(){
         this.tutions.Getupdatescount(this.usersID).subscribe((res)=>{
          this.usersUpdatesCounts=res
           
         }) 
    }

    handleFileInputFurnitures($event:any,images){
      this.imageName= File = $event.target.files[0];

        console.log(this.imageName);
      
    }

    handleFileInputsFurnitures($event:any,viedoes){
      this.videoName= File = $event.target.files[0];
      this.formData.append(viedoes,this.videoName);
     console.log(this.videoName);
     
   }
  
   UpdatePostData( ){
    this.formData.append('images',this.imageName);
    // this.formData.append('images1',this.imageName1);
    // this.formData.append('images2',this.imageName2);
    // this.formData.append('images3',this.imageName3);
    this.formData.append('title',this.postupdatedata.title);
    this.formData.append('description',this.postupdatedata.description);
     
  
      console.log(this.formData)
  
     this.tutions.postsUpdata(this.usersID,this.formData).subscribe((res)=>{
        console.log('assssss',res);
       console.log(this.usersID);
       
         })
         this.postupdatedata.title="",
         this.postupdatedata.description="",
         this.postupdatedata.images="",
        //  this.postupdatedata.images1="",
        //  this.postupdatedata.images2="",
        //  this.postupdatedata.images3="",
         this.postupdatedata.viedoes=""
   }
  

showicons(){
  this.show=!this.show
  this.shows=false
}
showcomments(item?){
  this.show=false
  this.shows=!this.shows
   this.updatesIds=item._id

   
   console.log(this.updatesIds);
   
   this.comm()
 
        }
        updatestoAdmincommentsget(result?){
          this.showes=!this.showes
                   this.commentsUpdatesId=result
                   console.log(result);
                    this.comm()
                    this.commCount()
                    // this.showcomments()
                   }
       comm(){
        this.tutions.commentsgetupdates(this.commentsUpdatesId).subscribe((res)=>{
            this.adminsupdateComments=res
            console.log(this.adminsupdateComments);
            
           
          })
                } 
                commCount(){
                  this.tutions.commentsgetupdatesCount(this.commentsUpdatesId).subscribe((res)=>{
                      this.adminsupdateCommentsCount=res
                      console.log(this.adminsupdateCommentsCount);
                      
                     
                    })
                          }
         updatestoadmincomments(){
         
          this.tutions.commentspostupdates(this.selectedblogIds,this.updatesCommentsdata).subscribe((res)=>{
           
           
           console.log(res);
           
          })
          this.updatesCommentsdata.descriptions=""
           
               }
               updatesAdminReply(items){
                this.showess=!this.showess
                this.selectedblogIds=items
                this.tutions.commentsgetupdatesReply(this.selectedblogIds).subscribe((res)=>{
                  this.adminsupdateCommentsReply=res
                 console.log(res);
                 
                })
              }
              
  
  }
  

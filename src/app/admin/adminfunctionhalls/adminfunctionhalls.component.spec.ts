import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminfunctionhallsComponent } from './adminfunctionhalls.component';

describe('AdminfunctionhallsComponent', () => {
  let component: AdminfunctionhallsComponent;
  let fixture: ComponentFixture<AdminfunctionhallsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminfunctionhallsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminfunctionhallsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

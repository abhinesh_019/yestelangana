import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
 
@Injectable()
export class AdminfunctionhallsService {
  constructor(private http: HttpClient) { }
  usersonly(id){
                    
    return this.http.get(environment.apiUrl +"/clientsGetind/"+id);
  }
 
  postsUpdataFec(id,data){
                    
    return this.http.post(environment.apiUrl +"/fecitiesPost/"+id,data);
  }
  ficilities(id){
    
    return this.http.get(environment.apiUrl +"/fecitiesGet/"+id);
  } 
  ficilitiesCount(id){
    
    return this.http.get(environment.apiUrl +"/fecitiesGetCounts/"+id);
  }

  postsUpdatacus(id,data){
                    
    return this.http.post(environment.apiUrl +"/fcustomersPost/"+id,data);
  }
  customers(id){
    
    return this.http.get(environment.apiUrl +"/customersGet/"+id);
  } 
  customersC(id){
    
    return this.http.get(environment.apiUrl +"/customersGetCounts/"+id);
  }

  postsUpdata(id,data){
                    
    return this.http.post(environment.apiUrl +"/updatesgetpostsdata/"+id,data);
  }


///////////////////////////////////  clients  updates    //////////////////////////////////////////
                
     
clientsUpdatesGet(id){
    
  return this.http.get(environment.apiUrl +"/updatesget/"+id);
}
clientsUpdatesCnts(id){
  
  return this.http.get(environment.apiUrl +"/updatesgetCounts/"+id);
}
 
commentsPoststoClints(id,data){
  
  return this.http.post(environment.apiUrl +"/updatesCommentReplysPost/"+id,data);
}
commentsGettoClints(id){
  
  return this.http.get(environment.apiUrl +"/updatesCommentsget/"+id);
}
commentsGettoClintsCounts(id){
  
  return this.http.get(environment.apiUrl +"/updatesCommentsgetcounts/"+id);
}
replyFromCommentesGet(id){
  
  return this.http.get(environment.apiUrl +"/updatesCommentReplysGet/"+id);
}

overallscommentspost(id){
  
  return this.http.get(environment.apiUrl +"/userscomments/"+id);
}
// ********************************
 

onlyCommentsGetCount(id){
  
  return this.http.get(environment.apiUrl +"/userscommentsCounts/"+id);
}

futuresPosts(id,data){
  
  return this.http.post(environment.apiUrl +"/functionalHallAdditionalFuture/"+id,data);
}

futuresGets(id){
  
  return this.http.get(environment.apiUrl +"/functionalHallAdditionalFutures/"+id);
}

futuresTwoPosts(id,data){
  
  return this.http.post(environment.apiUrl +"/functionalHallAdditionalFuturesTwo/"+id,data);
}
futuresTwoGets(id){
  
  return this.http.get(environment.apiUrl +"/functionalHallAdditionalFuturesTwo/"+id);
}
}
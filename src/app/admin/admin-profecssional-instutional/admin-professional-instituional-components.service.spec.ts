import { TestBed, inject } from '@angular/core/testing';

import { AdminProfessionalInstituionalComponentsService } from './admin-professional-instituional-components.service';

describe('AdminProfessionalInstituionalComponentsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminProfessionalInstituionalComponentsService]
    });
  });

  it('should be created', inject([AdminProfessionalInstituionalComponentsService], (service: AdminProfessionalInstituionalComponentsService) => {
    expect(service).toBeTruthy();
  }));
});

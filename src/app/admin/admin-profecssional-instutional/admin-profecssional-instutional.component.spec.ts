import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminProfecssionalInstutionalComponent } from './admin-profecssional-instutional.component';

describe('AdminProfecssionalInstutionalComponent', () => {
  let component: AdminProfecssionalInstutionalComponent;
  let fixture: ComponentFixture<AdminProfecssionalInstutionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminProfecssionalInstutionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminProfecssionalInstutionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

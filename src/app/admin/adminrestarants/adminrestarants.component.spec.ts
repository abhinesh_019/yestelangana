import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminrestarantsComponent } from './adminrestarants.component';

describe('AdminrestarantsComponent', () => {
  let component: AdminrestarantsComponent;
  let fixture: ComponentFixture<AdminrestarantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminrestarantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminrestarantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

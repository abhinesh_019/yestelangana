import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AdminrestarantsComponent } from './adminrestarants.component';
import { AdminrestarantsService } from './adminrestarants.service';
 

const routes:Routes=[{path:'adminRestarants',component:AdminrestarantsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
  ],
  declarations: [
    AdminrestarantsComponent,

  ],
  providers: [AdminrestarantsService],
})
export class AdminrestarantsModule { }

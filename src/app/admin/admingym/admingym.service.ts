import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
 
@Injectable()
export class AdmingymService {

  constructor(private http: HttpClient) { }
  gymusersonly(id){
                    
    return this.http.get(environment.apiUrl +"/gymClientsGetind/"+id);
  }
  postsUpdata(id,data){
    return this.http.post(environment.apiUrl + "/gymServicesP/"+id,data);
     
  }
  postsEquipment(id,data){
    return this.http.post(environment.apiUrl + "/gymEquipmentsP/"+id,data);
     
  }
  equipmentsgetInd(id){
    return this.http.get(environment.apiUrl + "/gymEquipmentsind/"+id);
     
  }
  postsficilities(id,data){
    return this.http.post(environment.apiUrl + "/gymFiciltiesP/"+id,data);
     
  }
  equipmentsgetCount(id){
    return this.http.get(environment.apiUrl + "/gymEquipmentsCount/"+id);
     
  }
  equipmentsget(id){
    return this.http.get(environment.apiUrl + "/gymEquipments/"+id);
     
  }
  postscatatageries(id,data){
    return this.http.post(environment.apiUrl + "/gymCategoriesp/"+id,data);
     
  }
  postscatatageriesget(id){
    return this.http.get(environment.apiUrl + "/gymCategorie/"+id);
     
  }

  userServicesCount(id){
    return this.http.get(environment.apiUrl + "/gymServicesCount/"+id);
     
  }
  userServicesind(id){
    return this.http.get(environment.apiUrl + "/gymServicesind/"+id);
     
  }
  userServices(id){
    return this.http.get(environment.apiUrl + "/gymServices/"+id);
     
  }


// ********************************** fecilitites *************************

fecilitiesget(id){
                    
  return this.http.get(environment.apiUrl +"/gymFicilties/"+id);
}
fecilitiesgetCount(id){
                    
  return this.http.get(environment.apiUrl +"/gymFiciltiesCount/"+id);
}
overallscommentsge(id){
                    
  return this.http.get(environment.apiUrl +"/gymeuserscomments/"+id);
}
onlyCommentsGetCount(id){
                    
  return this.http.get(environment.apiUrl +"/gymCeuserscommentsCounts/"+id);
}
 
// *********************************************************


///////////////////////////////////  clients  updates    //////////////////////////////////////////

postsUpdates(id,data){
    
  return this.http.post(environment.apiUrl +"/gymupdatesgetpostsdata/"+id,data);
}

                 
     
clientsUpdatesGet(id){
    
  return this.http.get(environment.apiUrl +"/gymupdatesget/"+id);
}
 
clientsUpdatesGetCounts(id){
  
  return this.http.get(environment.apiUrl +"/gymupdatesgetCounts/"+id);
}
commentsPoststoClints(id,data){
  
  return this.http.post(environment.apiUrl +"/gymupdatesCommentReplysPost/"+id,data);
}
commentsGettoClints(id){
  
  return this.http.get(environment.apiUrl +"/gymupdatesCommentsget/"+id);
}
commentsGettoClintsCounts(id){
  
  return this.http.get(environment.apiUrl +"/gymupdatesCommentsgetcounts/"+id);
}
replyFromCommentesGet(id){
  
  return this.http.get(environment.apiUrl +"/gymupdatesCommentReplysGet/"+id);
}

overallscommentspost(id,data){
  
  return this.http.post(environment.apiUrl +"/gymeuserscomments/"+id,data);
}
// ********************************
postsUpdatausers(id,data){
  
  return this.http.post(environment.apiUrl +"/gymusersP/"+id,data);
}
usersdetailsget(id){
  
  return this.http.get(environment.apiUrl +"/gymusers/"+id);
}
usersdetailsgetCount(id){
  
  return this.http.get(environment.apiUrl +"/gymusersCount/"+id);
}
}

import { TestBed, inject } from '@angular/core/testing';

import { AdmingymService } from './admingym.service';

describe('AdmingymService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdmingymService]
    });
  });

  it('should be created', inject([AdmingymService], (service: AdmingymService) => {
    expect(service).toBeTruthy();
  }));
});

import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AdminoutdoorgamesComponent } from './adminoutdoorgames.component';
import { AdminoutdoorgamesService } from './adminoutdoorgames.service';
 

const routes:Routes=[{path:'adminoutdoor',component:AdminoutdoorgamesComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
  ],
  declarations: [
    AdminoutdoorgamesComponent,

  ],
  providers: [AdminoutdoorgamesService],
})
export class AdminoutdoorgamesModule { }

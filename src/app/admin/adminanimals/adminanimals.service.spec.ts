import { TestBed, inject } from '@angular/core/testing';

import { AdminanimalsService } from './adminanimals.service';

describe('AdminanimalsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminanimalsService]
    });
  });

  it('should be created', inject([AdminanimalsService], (service: AdminanimalsService) => {
    expect(service).toBeTruthy();
  }));
});

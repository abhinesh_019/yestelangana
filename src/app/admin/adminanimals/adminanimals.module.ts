import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AdminanimalsComponent } from './adminanimals.component';
import { AdminanimalsService } from './adminanimals.service';
 
const routes:Routes=[{path:'adminAnimals',component:AdminanimalsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
  ],
  declarations: [
    AdminanimalsComponent,

  ],
  providers: [AdminanimalsService],
})
export class AdminanimalsModule { }

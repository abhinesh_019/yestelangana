import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminanimalsComponent } from './adminanimals.component';

describe('AdminanimalsComponent', () => {
  let component: AdminanimalsComponent;
  let fixture: ComponentFixture<AdminanimalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminanimalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminanimalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

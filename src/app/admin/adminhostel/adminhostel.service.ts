import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
 
@Injectable()
export class AdminhostelService {

  constructor(private http: HttpClient) { }
  
  usersDetails(id){
    return this.http.get(environment.apiUrl + "/boysHostelsgInd/"+id);
     
  }
//  ************************updates present ***************************
postUpdatesForm(id,data){
  return this.http.post(environment.apiUrl + "/hostelsupdatespostsdata/"+id,data);
   }

clientsUpdatesGet(id){
    
  return this.http.get(environment.apiUrl +"/hostelsupdatesget/"+id);
}

clientsUpdatesGetCounts(id){
  
  return this.http.get(environment.apiUrl +"/hostelsupdatesgetCounts/"+id);
}
commentsPoststoClints(id,data){
  
  return this.http.post(environment.apiUrl +"/hostelsupdatesCommentReplysPost/"+id,data);
}
commentsGettoClints(id){
  
  return this.http.get(environment.apiUrl +"/hostelsupdatesCommentsget/"+id);
}
commentsGettoClintsCounts(id){
  
  return this.http.get(environment.apiUrl +"/hostelsupdatesCommentsgetcounts/"+id);
}
replyFromCommentesGet(id){
  
  return this.http.get(environment.apiUrl +"/hostelsupdatesCommentReplysGet/"+id);
}

overallscommentspost(id,data){
  
  return this.http.post(environment.apiUrl +"/hostelseuserscomments/"+id,data);
}
// ********************************
postFecilities(id,data){
  return this.http.post(environment.apiUrl + "/hostelsfecitiesPost/"+id,data);
   }
   fecilities(id){
    return this.http.get(environment.apiUrl + "/hostelsfecitiesGet/"+id);
   }
   fecilitiesc(id){
    return this.http.get(environment.apiUrl + "/hostelsfecitiesGetCounts/"+id);
   }
 
   generalsCommGet(id) {
    return this.http.get(environment.apiUrl + "/hostelseuserscomments/"+id);
  }
  generalsCommGetC(id) {
    return this.http.get(environment.apiUrl + "/hostelseuserscommentsCounts/"+id);
  }
}

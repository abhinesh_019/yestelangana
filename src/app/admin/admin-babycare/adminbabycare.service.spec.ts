import { TestBed, inject } from '@angular/core/testing';

import { AdminbabycareService } from './adminbabycare.service';

describe('AdminbabycareService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminbabycareService]
    });
  });

  it('should be created', inject([AdminbabycareService], (service: AdminbabycareService) => {
    expect(service).toBeTruthy();
  }));
});

import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { FurnituresService } from '../../general/general-furnitures/generalfurnitures.service';
import { PushNotificationsService } from '/home/abhinesh/Documents/angular5/p&a applications/hostels/gitprojects/yestelangana/src/app/push-notifications.service'
import { AdminBabycareComponent } from './admin-babycare.component';
import { AdminbabycareService } from './adminbabycare.service';


const routes:Routes=[{path:'adminBabycare',component:AdminBabycareComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    AdminBabycareComponent,

  ],
  providers: [FurnituresService,PushNotificationsService,AdminbabycareService],
})
export class AdminBabycareModule { }

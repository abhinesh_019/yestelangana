import { TestBed, inject } from '@angular/core/testing';

import { AdminfireWorksService } from './adminfire-works.service';

describe('AdminfireWorksService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminfireWorksService]
    });
  });

  it('should be created', inject([AdminfireWorksService], (service: AdminfireWorksService) => {
    expect(service).toBeTruthy();
  }));
});

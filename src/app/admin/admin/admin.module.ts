import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminComponent } from './admin.component';
import { SharedModule } from '../../shared/shared.module';
import { AdminService } from '../admin.service';
import { BabycareService } from '../../babycare/babycare.service';
import {MatRadioModule} from '@angular/material/radio';
import { FurnituresService } from '../../furnitures/furnitures.service';
import { FurnituresadminModule } from '../furnituresadmin/furnituresadmin.module';
import { MaterialsModule } from '../../login/shared/materials.module';
import { AdminBabycareModule } from '../admin-babycare/admin-babycare.module';
import { AdmintutionsModule } from '../admintutions/admintutions.module';
import { AdminbeautyparlourModule } from '../adminbeautyparlour/adminbeautyparlour.module';
import { AdminbeautyparlourService } from '../adminbeautyparlour/adminbeautyparlour.service';
import { AdminhostelModule } from '../adminhostel/adminhostel.module';
import { AdminhostelService } from '../adminhostel/adminhostel.service';
import { AdmingymModule } from '../admingym/admingym.module';
import { AdmingymService } from '../admingym/admingym.service';
import { AdmintravelsModule } from '../admintravels/admintravels.module';
import { AdmintravelsService } from '../admintravels/admintravels.service';
import { AdminfunctionhallsModule } from '../adminfunctionhalls/adminfunctionhalls.module';
import { AdminmusicModule } from '../adminmusic/adminmusic.module';
import { AdminrealestateModule } from '../adminrealestate/adminrealestate.module';
import { AdminrealestatefinalModule } from '../adminrealestatefinal/adminrealestatefinal.module';
import { AdmindanceModule } from '../admindance/admindance.module';
import { AdminrestarantsModule } from '../adminrestarants/adminrestarants.module';
import { AdminmarriagebuerauModule } from '../adminmarriagebuerau/adminmarriagebuerau.module';
import { AdminswimmingModule } from '../adminswimming/adminswimming.module';
import { AdminfightModule } from '../adminfight/adminfight.module';
import { AdminindoorgamesModule } from '../adminindoorgames/adminindoorgames.module';
import { AdminoutdoorgamesModule } from '../adminoutdoorgames/adminoutdoorgames.module';
import { AdminDrivingSchoolModule } from '../admin-driving-school/admin-driving-school.module';
import { AdminfireWorksModule } from '../admin-fire-works/adminfire-works.module';
import { AdminPackersMoversModule } from '../admin-packers-movers/admin-packers-movers.module';
 import { AdminPlantsModule } from '../admin-plants/admin-plants.module';
 import { AdminProfessionalInstituionalComponentsModule } from '../admin-profecssional-instutional/admin-professional-instituional-components.module';
import { SweetalertService } from '../../sweetalert.service';
import { AdminanimalsModule } from '../adminanimals/adminanimals.module';
 
  

const routes: Routes = [
  {
    path: '', component: AdminComponent, children:
      [
        { path: 'adminAnimals', loadChildren: 'app/admin/adminanimals/adminanimals.module#AdminanimalsModule'},
        { path: 'furnituresadmin', loadChildren: 'app/admin/furnituresadmin/furnituresadmin.module#FurnituresadminModule'},
        { path: 'adminBabycare', loadChildren: 'app/admin/admin-babycare/admin-babycare.module#AdminBabycareModule'},
        { path: 'tutionsadmin', loadChildren: 'app/admin/admintutions/admintutions.module#AdmintutionsModule'},
        { path: 'adminbeautyparlour', loadChildren: 'app/admin/adminbeautyparlour/adminbeautyparlour.module#AdminbeautyparlourModule'},
        { path: 'adminhostels', loadChildren: 'app/admin/adminhostel/adminhostel.module#AdminhostelModule'},
        { path: 'admingym', loadChildren: 'app/admin/admingym/admingym.module#AdmingymModule'},
        { path: 'admintravels', loadChildren: 'app/admin/admintravels/admintravels.module#AdmintravelsModule'},
        { path: 'adminfunctionHalls', loadChildren: 'app/admin/adminfunctionhalls/adminfunctionhalls.module#AdminfunctionhallsModule'},
        { path: 'adminmusics', loadChildren: 'app/admin/adminmusic/adminmusic.module#AdminmusicModule'},
        { path: 'adminrealestate', loadChildren: 'app/admin/adminrealestate/adminrealestate.module#AdminrealestateModule'},
        { path: 'adminrealestatefinal/:_id/:name', loadChildren: 'app/admin/adminrealestatefinal/adminrealestatefinal.module#AdminrealestatefinalModule'},
        { path: 'adminDances', loadChildren: 'app/admin/admindance/admindance.module#AdmindanceModule'},
        { path: 'adminRestarants', loadChildren: 'app/admin/adminrestarants/adminrestarants.module#AdminrestarantsModule'},
        { path: 'marriagesB', loadChildren: 'app/admin/adminmarriagebuerau/adminmarriagebuerau.module#AdminmarriagebuerauModule'},
        { path: 'adminswimming', loadChildren: 'app/admin/adminswimming/adminswimming.module#AdminswimmingModule'},
        { path: 'adminfights', loadChildren: 'app/admin/adminfight/adminfight.module#AdminfightModule'},
        { path: 'adminoutdoor', loadChildren: 'app/admin/adminoutdoorgames/adminoutdoorgames.module#AdminoutdoorgamesModule'},
        { path: 'adminindoor', loadChildren: 'app/admin/adminindoorgames/adminindoorgames.module#AdminindoorgamesModule'},
        { path: 'admidrivingSchools', loadChildren: 'app/admin/admin-driving-school/admin-driving-school.module#AdminDrivingSchoolModule'},
        { path: 'adminFireWorks', loadChildren: 'app/admin/admin-fire-works/adminfire-works.module#AdminfireWorksModule'},
        { path: 'admidPackersMovers', loadChildren: 'app/admin/admin-packers-movers/admin-packers-movers.module#AdminPackersMoversModule'},
        { path: 'adminProfessionalInstitutions', loadChildren: 'app/admin/admin-profecssional-instutional/admin-professional-instituional-components.module#AdminProfessionalInstituionalComponentsModule'},
        { path: 'adminPlants', loadChildren: 'app/admin/admin-plants/admin-plants.module#AdminPlantsModule'},

      ]
    }]
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatRadioModule,
    FurnituresadminModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    SharedModule,
    MaterialsModule,
    AdminBabycareModule,
    AdmintutionsModule,
    HttpClientModule,
    AdminbeautyparlourModule,
    AdminhostelModule,
    AdmingymModule,
    AdmintravelsModule,
    AdminfunctionhallsModule,
    AdminmusicModule,
    AdminrealestateModule,
    AdminrealestatefinalModule,
    AdmindanceModule,
    AdminrestarantsModule,
    AdminmarriagebuerauModule,
    AdminswimmingModule,
    AdminfightModule,
    AdminoutdoorgamesModule,
    AdminindoorgamesModule,
    AdminDrivingSchoolModule,
    AdminfireWorksModule,
    AdminPackersMoversModule,
    AdminProfessionalInstituionalComponentsModule,
    AdminPlantsModule,
     AdminanimalsModule
     
    ],
  declarations: [
    AdminComponent,
   ],
  providers: [AdminService,SweetalertService,BabycareService,FurnituresService,AdminbeautyparlourService,AdminhostelService,AdmingymService,AdmintravelsService],
})
 
export class AdminModule { }

import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
 
import { NgModule } from '@angular/core';
import { FurnituresadminComponent } from './furnituresadmin.component';
import { SharedModule } from '../../shared/shared.module';
import { FurnituresService } from '../../general/general-furnitures/generalfurnitures.service';
import { FurnitureadminService } from './furnitureadmin.service';


const routes:Routes=[{path:'furnituresadmin',component:FurnituresadminComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    FurnituresadminComponent,

  ],
  providers: [FurnituresService,FurnitureadminService],
})
export class FurnituresadminModule { }

import { Component, OnInit } from '@angular/core';
import { FurnituresService } from '../../furnitures/furnitures.service';
import { AdminService } from '../admin.service';
import {  PushNotificationsService } from '/home/abhinesh/Documents/angular5/p&a applications/hostels/gitprojects/yestelangana/src/app/push-notifications.service';
import { FurnitureadminService } from './furnitureadmin.service';
@Component({
  moduleId: module.id,
  selector: 'app-furnituresadmin',
  templateUrl: './furnituresadmin.component.html',
  styleUrls: ['./furnituresadmin.component.css']
})
export class FurnituresadminComponent implements OnInit {
public showmains:boolean=false
public updatesShows:boolean=false
public viewupdatesShows:boolean=false
public onlyCommentsShows:boolean=false
public ImagesShows:boolean=false
public ProductsShows:boolean=false
public ProductsViewShows:boolean=false
public adminsupdateCommentsCounts:any
public customersNameId:any
public customersID:any
 public customersGet:any
public customersGetCount:any
public productNmeID:any
public productsName:any
// public adminsupdateCommentsCounts:any
// public adminsupdateCommentsCounts:any
// public adminsupdateCommentsCounts:any
// public adminsupdateCommentsCounts:any
// public adminsupdateCommentsCounts:any

public usersID:any
public  furnitureuser:any

postupdatedataFurnitures={
  title:"",
  images:"",
  descriptions:"",
  viedoes:""
}


public imageName:any
public videoName:any
public adminupdatesFurnitures:any
public onlyComments:any
public usersUpdatesCounts:any
public onlyCommentsCounts:any

productsPosts={
  images:"",
  name:"",
  price:"",
  productDescription:"",
  guarantee:"",
}
cus={
  customerName:"",
    customerPlace:"",
     customerImg:"",

}
updatesCommentsdataReply={
  
  descriptions:"",
   
}
public replyId:any
public productscounts:any
public imagesName:any
public products:any
public viewsProfiles:any
public viewsNoti:any
public show:any
public shows:any
public updatesIds:any
public showes:any
public showess:any
public selectedblogIds:any
public commentsUpdatesId:any
public adminsupdateCommentsReply:any
public adminsupdateComments:any
public adminShows:any
public notifi:any
public furnitureTypeGets:any
public furnitueTypeId:any
public furnitueTypeName:any
public furnitureTypeGetss:any
public productsServicesTwos:any
// public furnitureTypeGets:any
// public furnitureTypeGets:any
// public furnitureTypeGets:any
// public furnitureTypeGets:any
// public furnitureTypeGets:any
// public furnitureTypeGets:any
// public furnitureTypeGets:any
// public furnitureTypeGets:any
// public furnitureTypeGets:any
// public furnitureTypeGets:any

tr={
  furnitureType:"",
}

formData: FormData = new FormData(); 
  constructor(private furnituresOne:FurnitureadminService,private furniture:FurnituresService,private admins:AdminService,private _notificationService: PushNotificationsService) { 
    
  }

  ngOnInit() {
    
    
    this.usersID=localStorage.getItem('ads');
    this.users()
    this.updatesCounts()
    this.kingUsers()
    this.pros()
    this.updadesMain()
    this.generalCommentsCounts()
    this.productsCount()
    this.furnitureTypeGet()
    
  }

  viewProfile(){
    this.viewsProfiles=!this.viewsProfiles
    this.viewsNoti=false
  }
  viewnoti(){
    this.viewsNoti=!this.viewsNoti
    this.viewsProfiles=false

  }

  mainMenuShow(){
this.showmains=!this.showmains
this.updatesShows=false
this.viewupdatesShows=false
this.onlyCommentsShows=false
this.ImagesShows=false
this.ProductsShows=false
this.ProductsViewShows=false
 
  }

  users(){
    this.furniture.furnitureusersonly(this.usersID).subscribe((res)=>{
      this.furnitureuser=res
       
    })
  }
  updatesShow(){
    this.updatesShows=!this.updatesShows
    this.showmains=false
    this.viewupdatesShows=false
    this.onlyCommentsShows=false
    this.ImagesShows=false
    this.ProductsShows=false
    this.ProductsViewShows=false

    
  }
  viewupdatesShow(){
    
    this.viewupdatesShows=!this.viewupdatesShows
    this.updatesShows=false
    this.showmains=false
    this.onlyCommentsShows=false
    this.ImagesShows=false
    this.ProductsShows=false
    this.ProductsViewShows=false   
  }
updadesMain(){
  this.usersID=localStorage.getItem('ads');
    this.furnituresOne.Getupdates(this.usersID).subscribe((res)=>{
       
      this.adminupdatesFurnitures=res
      
    }) 
}
  kingUsers(){
    this.usersID=localStorage.getItem('ads');
this.furnituresOne.furnitureusersonly(this.usersID).subscribe((res)=>{
  this.furnitureuser=res
   
})
  }

  updatesCounts(){
    this.usersID=localStorage.getItem('ads');
    this.furnituresOne.GetupdatesCounts(this.usersID).subscribe((res)=>{
         
      this.usersUpdatesCounts=res
      
    }) 
  }
  

  onlyCommentsShow(){
    this.onlyCommentsShows=!this.onlyCommentsShows
    this.updatesShows=false
    this.showmains=false
    this.viewupdatesShows=false
    this.ImagesShows=false
    this.ProductsShows=false
    this.ProductsViewShows=false

   

     
    this.furnituresOne.overallscommentsGet(this.usersID).subscribe((res)=>{
      
      
     this.onlyComments=res
       
    })
  }

  generalCommentsCounts(){
    this.furnituresOne.overallscommentspostCounts(this.usersID).subscribe((res)=>{
      
      
      this.onlyCommentsCounts=res
        
     })
  }
  productsCount(){
    this.usersID=localStorage.getItem('ads');
    this.furnituresOne.productsCounts(this.usersID).subscribe((res)=>{
         
      this.productscounts=res
      
    }) 
  }
  ImagesShow(){
    
    this.ImagesShows=!this.ImagesShows
    this.updatesShows=false
    this.showmains=false
    this.onlyCommentsShows=false
    this.viewupdatesShows=false
    this.ProductsShows=false
    this.ProductsViewShows=false
  }
  ProductsShow(){
    this.ProductsShows=!this.ProductsShows
    this.updatesShows=false
    this.showmains=false
    this.onlyCommentsShows=false
    this.viewupdatesShows=false
    this.ImagesShows=false
    this.ProductsViewShows=false
  }
  ProductsViewShow(){
    this.ProductsViewShows=!this.ProductsViewShows
    this.updatesShows=false
    this.showmains=false
    this.onlyCommentsShows=false
    this.viewupdatesShows=false
    this.ImagesShows=false
    this.ProductsShows=false

  
  }

pros(){
  this.usersID=localStorage.getItem('ads');
  this.furnituresOne.products(this.usersID).subscribe((res)=>{

    this.products=res
    var cudOne= this.products[0]
    this.OnClickToCustomers(cudOne)
  }) 
}
OnClickToCustomers(get){
this.customersID=get._id
this.customersNameId=get.name
 this.customersGets()
this.customersGetsCount()
}
  handleFileInputFurnitures($event:any,images){
    this.imageName= File = $event.target.files[0];
     
  }

  handleFileInputsFurnitures($event:any,viedoes){
    this.videoName= File = $event.target.files[0];
 
    
 }

 FurnituresUpdatePostData( ){
  let formData: FormData = new FormData();
   formData.append('images', this.imageName);
   formData.append('viedoes', this.videoName);
   formData.append('title', this.postupdatedataFurnitures.title);
   formData.append('descriptions', this.postupdatedataFurnitures.descriptions);
   this.usersID=localStorage.getItem('ads');
    this.furnituresOne.postsdataupFurnitures(this.usersID,formData).subscribe((res)=>{
     
       })
       this.postupdatedataFurnitures.title="",
       this.postupdatedataFurnitures.descriptions="",
       this.postupdatedataFurnitures.images,
       this.postupdatedataFurnitures.viedoes
 }
 
handleFile($event:any,images){
  this.imagesName= File = $event.target.files[0];
  
}

OnSubmitProductsFurniturespost(){
  this.formData.append('images', this.imagesName);
 this.formData.append('name',this.productsPosts.name);
  this.formData.append('price',this.productsPosts.price);
  this.formData.append('productDescription',this.productsPosts.productDescription);
  this.formData.append('guarantee',this.productsPosts.guarantee); 
  
 this.furnituresOne.postProducts(this.furnitueTypeId,this.formData).subscribe((res)=>{
     
     })

     this.productsPosts.name="",
     this.productsPosts.price,
     this.productsPosts.productDescription="",
     this.productsPosts.guarantee=""
     this.productsPosts.images=""
 
}
productsServiceTwo(){
  this.furnituresOne.productsServiceTwos(this.furnitueTypeId).subscribe((res)=>{
      this.productsServicesTwos=res
      })
     }




showicons(){
  this.show=!this.show
  this.shows=false
}
showcomments(item){
  
  this.shows=!this.shows
   this.updatesIds=item._id

   
    
   this.comm()
   this.comCounts()
       }
                        comm(){
                        this.furniture.commentsgetupdates(this.updatesIds).subscribe((res)=>{
                            this.adminsupdateComments=res
                            
                           })

                            }
                                commentsReplyId(result){
                                  this.updatesCommentsdataReply.descriptions=""
                                  this.adminShows=!this.adminShows
this.replyId=result
                          }     
                                updatesAdminCommentsReply(){
        this.furnituresOne.commentsUpdatesAdminReply(this.replyId,this.updatesCommentsdataReply).subscribe((res)=>{
                })
    this.updatesCommentsdataReply.descriptions=""
                     }
                     updatesAdminReply(items){
                      this.showess=!this.showess
                      this.selectedblogIds=items
                      this.furnituresOne.commentsgetupdatesReply(this.selectedblogIds).subscribe((res)=>{
                        this.adminsupdateCommentsReply=res
                       })
                    } 

                    comCounts(){
                      this.furnituresOne.commentsgetupdatesCounts(this.updatesIds).subscribe((res)=>{
                          this.adminsupdateCommentsCounts=res
                         })                   
}
// *********************************************    Customers            ********************************************

handleFileInputCustomers($event:any,images){
  this.imageName= File = $event.target.files[0];
    }
 
    OnSubmitProductsFurnituresposts(){
  let formData: FormData = new FormData();
 formData.append('customerName', this.cus.customerName);
 formData.append('customerPlace', this.cus.customerPlace);
 this.furnituresOne.updatePostDataCustomers(this.customersID,formData).subscribe((res)=>{
  
     })
     this.cus.customerPlace="",
     this.cus.customerName="",
     this.cus.customerImg=""
}
 
customersGets(){
   this.furnituresOne.customersGetsForProducts(this.customersID).subscribe((res)=>{
     this.customersGet=res
    
       var cudOne= this.customersGet[0]
    this.productsClicksSlects(cudOne)
  }) 
}
productsClicksSlects(gets){
  this.productNmeID=gets._id
  this.productsName=gets.name
}
customersGetsCount(){
  
  this.furnituresOne.customersGetsForProductsCount(this.customersID).subscribe((res)=>{
    
   this.customersGetCount=res
   
 }) 
}
// *********************************************    furniture types             ********************************************
  
 furnitureTypesPosts(){
  this.usersID=localStorage.getItem('ads');
  this.furnituresOne.furnitureTypresPosts(this.usersID,this.tr).subscribe((res)=>{
    
    console.log(res)
    console.log(this.usersID)
    console.log(this.tr)
   })
   this.tr.furnitureType =""
}
 furnitureTypeGet(){
  this.usersID=localStorage.getItem('ads');
 this.furnituresOne.furnitureTypeGets(this.usersID).subscribe((res)=>{
this.furnitureTypeGetss=res
console.log(res);

 var furs=this.furnitureTypeGetss[0]
this.furnitureClick(furs)
})
}
furnitureClick(get?){
this.furnitueTypeId=get._id
this.furnitueTypeName=get.furnitureType
this.productsServiceTwo()
}

}

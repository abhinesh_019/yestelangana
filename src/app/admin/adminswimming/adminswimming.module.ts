import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AdminswimmingComponent } from './adminswimming.component';
import { AdminswimmingService } from './adminswimming.service';
 
const routes:Routes=[{path:'adminswimming',component:AdminswimmingComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    AdminswimmingComponent,

  ],
  providers: [AdminswimmingService],
})
export class AdminswimmingModule { }

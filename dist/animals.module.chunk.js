webpackJsonp(["animals.module"],{

/***/ "./src/app/animals/animals/animals.component.css":
/***/ (function(module, exports) {

module.exports = "/* ************************nav bar *****************/\n.bg{  padding-bottom:60px;   background-color: #e8e6e56b;}\n.mars{margin-top:20px}\n.fff{ width: 60px; /* margin-right: 60px; */  margin-top: 12px;}\n.lsc{cursor: pointer;}\n.profilesv{position: absolute; right: 0; top: 45px;text-align: center; /* margin-right: 42px; */ padding-bottom: 27px;  z-index:99999  }\n.fnamess{ font-size: 11px;font-weight: bolder; }\n.sigbtn{  width: 120px;  padding: 8px; border-radius: 5px;  }\n.activities{-webkit-box-shadow: -7px 8px 6px 7px;box-shadow: -7px 8px 6px 7px; background-color: burlywood; border-radius: 12px;  border-bottom: 12px solid beige;margin-left: 25px;text-align: center }\n.acName{border-bottom: 4px solid gainsboro;  padding-bottom: 12px;}\n.sidenavact{ margin-bottom: 12px; background-color: #a52a2a7a; font-weight: bolder;  margin-top: 12px;color: honeydew;cursor: pointer;}\n.commm{list-style: none}\n.gimages{background-color: blue;  width: 50%;  text-align: center; font-size: 25px;  padding: 12px; border-radius: 5px; color: white;}\n.imagesfur{width:30%}\n/* ***************************************/\n.divtwo{border-right: 12px solid #0000ff59;}\n.cat{\n    padding: 12px;\n    border-radius: 5px;\n    text-align: center;\n  }\n.areas{color:red;font-size:15px;font-weight: bolder;text-transform: capitalize}\n.center{font-size:15px;font-weight: bolder;text-transform: capitalize}\n.bnts{margin-right: 90px;\n    color: #fff;\n    background-color: #337ab7;\n    border-color: #2e6da4;\n    padding: 12px;\n    font-size: 17px;\n    margin-bottom: 25px;\n    text-transform: capitalize;}\n.dropdown-menu{\n  top: 1% !important;\nleft: 15px !important;\ntext-align: center;\nfont-size: 15px;\nbackground: #846c4fba;\ncursor: pointer;\ncolor: white;\ntext-transform: capitalize;\n}\n.locationsdrops{ border-bottom: 2px solid #00000024;padding: 7px}\n.locationsdrops:hover{background-color: #ffca00;color: black}\n.search{width: 11%}\n.mat-form-field { width: 50%}\n.tdimages{width: 30%;}\n.bases{border-right: 1px solid #8080806b;}\n.boxshaves{\n    border: 1px solid;\n    padding: 10px;\n    -webkit-box-shadow:10px 10px 5px 10px 18px grey;\n            box-shadow:10px 10px 5px 10px 18px grey;\n    background-color: #0070ff5c;\n  }\n.s{\n    border: 1px solid;\n    padding: 10px;\n    -webkit-box-shadow: 5px 10px;\n            box-shadow: 5px 10px;\n    background-color: #0070ff5c;  }\n.bbc{\n    width: 40%;\n    font-weight: bolder;\n    color: #1c2277;\n    font-size: 25px;\n    text-transform: capitalize;}\n.dff:hover{\n      color:orange;\n    }\n.sides{color:blue; text-transform: capitalize;}\n.mains{ \n      text-transform: capitalize}\n@media screen and (max-width:320px){\n        .mains{margin-top: 25px;\n          text-transform: capitalize;height:300px;}\n      }\n.moresde{\n      color: red;\n      font-size: 14px;\n      margin-top: 55px;  \n    }\n.boxshaws{ \n      padding: 10px;\n      -webkit-box-shadow: 40px 32px 24px black;\n              box-shadow: 40px 32px 24px black;\n    margin-bottom: 40px;\n    cursor: pointer;  margin-top: 40px;\n   \n    }\n.boxshaws:hover{color:orange}\n.imgs{border-right: 2px solid;}\n.titlesshopname{text-align: center;\n  font-size: 25px;}\n.normaldetails{ color: black;\n    font-weight: bold;}\n/* .normaldetails:hover{color:orange} */\n.insidecap{\n      text-transform: capitalize\n    }\n.side{ color: blue;\n      text-transform: capitalize;\n      font-size: 25px;\n  }\n.anchoas{text-decoration: none;color:black}\n   "

/***/ }),

/***/ "./src/app/animals/animals/animals.component.html":
/***/ (function(module, exports) {

module.exports = " <!-- ******************* nav bar ************************** -->\n\n<nav class=\"navbar navbar-default bg fixed\">\n    <div class=\"container-fluid\">\n      <div class=\"navbar-header\">\n        <a class=\"navbar-brand yesT\">  <img src=\"assets/images/logo.png\"    style=\"width:25%\">    </a>\n      </div>\n      \n         \n  <div class=\"col-sm-3 profilesv\">\n      <div class=\"col-sm-12\">\n             <a [routerLink]= \"['/login/login']\" class=\"\">  Login  </a> /   <a [routerLink]= \"['/login/signup']\" class=\"\">  Sign Up  </a> \n     </div>\n   </div>\n    </div>\n  </nav>\n  \n  <div class=\"col-sm-12\" *ngFor=\"let one of animalsAddsOnesl\">\n      <a href=\"{{one.addsOneLinkLoc}}\">\n          <img src=\"{{one.addsOneImgLoc}}\" alt=\"about us\" style=\"width:100%;height:200px\">\n        </a>\n    </div>\n  <div class=\"col-sm-12\">\n      <img src=\"assets/images/ani.png\" alt=\"about us\" style=\"width:100%;height: 500px\">\n\n  </div>\n  <div class=\"col-sm-12\" *ngFor=\"let one of animalsAddsTwol\">\n      <a href=\"{{one.addsTwoLinkLoc}}\">\n        <img src=\"{{one.addsTwoImgLoc}}\" alt=\"about us\" style=\"width:100%;height:200px\">\n      </a>\n    </div>\n<div class=\"col-sm-12 boxshaves\">\n  <form class=\"example-form \">\n      <div class=\"col-sm-3 allsdivs\">\n          <button type=\"button\" class=\"btn btn-primary dropdown-toggle bnts\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n              {{locationName}} <span class=\"caret\"></span>\n            </button>\n            <ul class=\"dropdown-menu\">\n                <li class=\"locationsdrops\" *ngFor=\"let get of locationsget\" (click)=\"location(get)\">{{get.locations}}</li>\n                </ul> \n      </div>\n      </form>\n      <div class=\"col-sm-9\" *ngFor=\"let one of animalsAddsThreel\">\n          <a href=\"{{one.addsThreeLinkLoc}}\">\n            <img src=\"{{one.addsThreeImgLoc}}\" alt=\"about us\" style=\"width:100%;height:200px\">\n          </a>\n        </div>\n</div>\n\n<hr/>\n\n\n<div class=\"container-fluid\">\n<div class=\"row\">\n  <div class=\"col-sm-3 s\">\n      <div class=\"col-sm-12\" *ngFor=\"let one of animalsAddsOnes\">\n          <a href=\"{{one.addsOneLink}}\">\n            <img src=\"{{one.addsOneImg}}\" alt=\"about us\" style=\"width:100%;height:200px\">\n          </a>\n        </div>\n        <hr/>\n        <div class=\"col-sm-12\">\n      <h5 class=\"bg-primary cat\"> Catagerious </h5>\n       \n    </div>\n      <hr/>\n      <ul class=\"text-left\">\n        <li><a  class=\"center anchoas\"> Animals ({{animalsusercounttot}}) </a></li>\n       \n      </ul>\n      <div class=\"col-sm-12\" *ngFor=\"let one of animalsAddsTwoa\">\n          <a href=\"{{one.addsTwoLink}}\">\n            <img src=\"{{one.addsTwoImg}}\" alt=\"about us\" style=\"width:100%;height:200px\">\n          </a>\n        </div>\n    <hr/>\n    <div class=\"col-sm-12\">\n      <h5 class=\"bg-primary cat insidecap\"> Popular Locations  </h5>\n    </div>\n      <hr/>\n      <ul class=\"text-left\">\n          <li *ngFor=\"let item of animalsusercountLocations\"><a class=\"anchoas\"> {{item._id.distict}} ( {{item.count}} )  </a></li>\n        </ul>\n      <hr/>\n      <div class=\"col-sm-12\" *ngFor=\"let one of animalsAddsThreea\">\n          <a href=\"{{one.addsThreeLink}}\">\n            <img src=\"{{one.addsThreeImg}}\" alt=\"about us\" style=\"width:100%;height:200px\">\n          </a>\n        </div>\n        <div class=\"col-sm-12\">\n      <h5 class=\"bg-primary cat insidecap\"> Area Related To You </h5>\n    </div>\n      <hr/>\n      <h6> your are in : <span class=\"areas\"> {{locationName}}  </span></h6>\n      <h6> your are in : <span class=\"areas\"> {{selectArea}} ({{animalsusercount}}) </span></h6>\n      <ul class=\"text-left\">\n        <li *ngFor=\"let item of animalsuser\"><a class=\"anchoas\"> {{item.shopname}} </a></li>\n       \n      </ul>\n  \n            \n       \n       \n       <div class=\"col-sm-12\" *ngFor=\"let one of animalsAddsFours\">\n      <a href=\"{{one.addsFourLink}}\">\n      <img src=\"{{one.addsFourImg}}\" alt=\"about us\" style=\"width:100%;height:200px\">\n      </a>\n       \n       </div> \n       \n  \n      <hr/>\n      <div class=\"col-sm-12\">\n      <h5 class=\"bg-primary cat insidecap\"> Popular Areas Related To You </h5>\n    </div>\n      <hr/>\n      <ul class=\"text-left\">\n        <li *ngFor=\"let item of popularareas\"><a class=\"anchoas\"> {{item._id.area}} ( {{item.count}} )  </a></li>\n      </ul>\n      <hr/>\n      <h5 class=\"bg-primary cat insidecap\"> Other Services </h5>\n      <hr/>\n      \n      <ul class=\"text-left\">\n          <li> <a [routerLink]= \"['/animals']\" class=\"\"> Animals </a></li>\n\n        <li><a [routerLink]= \"['/babycare']\" class=\"\"> Baby Care </a></li>\n        <li><a [routerLink]= \"['/beautyparlour']\" class=\"\"> Beauty Parlour </a></li>\n        <li><a [routerLink]= \"['/dance']\" class=\"\"> Dance Institutions </a></li>\n        <li><a [routerLink]= \"['/drivingSchools']\" class=\"\"> Driving Schools </a></li>\n        <li><a [routerLink]= \"['/fight']\" class=\"\"> Fight Institutions </a></li>\n        <li><a [routerLink]= \"['/fireWorks']\" class=\"\"> Fire Works </a></li>\n        <li><a [routerLink]= \"['/halls']\" class=\"\"> Function Halls / Banquet Halls </a></li>\n        <li><a [routerLink]= \"['/furnitures']\" class=\"\"> Furnitures </a></li>\n        <li><a [routerLink]= \"['/gym']\" class=\"\"> Gym / Fitness Centers </a></li>\n        <li><a [routerLink]= \"['/hostelcatageries']\" class=\"\"> Hostels </a></li>\n        <li><a [routerLink]= \"['/indoorgames']\" class=\"\"> Indoor Games </a></li>\n        <li><a [routerLink]= \"['/marriagebureaus']\" class=\"\"> Marriage Bureaus </a></li>\n        <li><a [routerLink]= \"['/music']\" class=\"\"> Music Institutions </a></li>\n        <li><a [routerLink]= \"['/outdoorgames']\" class=\"\"> Outdoor Games </a></li>\n        <li> <a [routerLink]= \"['/pa']\" class=\"\"> Plants </a></li>\n        <li> <a [routerLink]= \"['/packersMovers']\" class=\"\"> Packers & Movers </a></li>\n        <li> <a [routerLink]= \"['/professionalInstitutionsCatageries']\" routerLinkActive=\"active-link\" class=\"home\">Professional Institutions </a>\n        </li>\n                  <li> <a [routerLink]= \"['/realestate']\" class=\"\"> Real Estate  </a></li>\n                  <li> <a [routerLink]= \"['/restaurants']\" class=\"\"> Restaurants </a></li>\n                  <li> <a [routerLink]= \"['/swimming']\" class=\"\"> Swmming Pools </a></li>\n                  <li> <a [routerLink]= \"['/tutions']\" class=\"\"> tution Centers </a></li> \n\n                </ul>\n  </div>\n  <!-- **************************next middle box************************** -->\n  <div class=\"col-sm-9 boxshaves\">\n     \n    <div class=\"col-sm-3\">\n      <button type=\"button\" class=\"btn btn-primary dropdown-toggle bnts\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n          {{selectArea}}  <span class=\"caret\"></span>\n        </button>\n        <ul class=\"dropdown-menu\">\n            <li class=\"locationsdrops\" *ngFor=\"let gets of areasAll\" (click)=\"selectedAre(gets)\">{{gets.area}}</li>\n            </ul> \n    </div>\n    \n    <div class=\"col-sm-9\">\n        <mat-form-field class=\"example-full-width\">\n            <input matInput type=\"text\" placeholder=\"search by name / area / pincode\" (ngModelChange)=\"searchFilter()\" [(ngModel)]=\"searchString\" name=\"animals\">\n        </mat-form-field>\n        \n     </div>\n     <div class=\"col-sm-12\" *ngFor=\"let one of animalsAddsFourl\">\n        <a href=\"{{one.addsFourLinkLoc}}\">\n          <img src=\"{{one.addsFourImgLoc}}\" alt=\"about us\" style=\"width:100%;height:200px\">\n        </a>\n      </div>\n<div class=\"col-md-12 col-xs-12\">\n  \n    <div class=\"col-sm-10\" *ngFor=\"let item of animalsuser\">\n      \n      <a [routerLink]=\"['/animals/animalsdetails',item._id,item.shopname]\">\n        <div class=\"panel panel-primary dff boxshaws\">\n            <h4 class=\"titlesshopname\"> <b class=\"side\">  {{item.shopname}} </b></h4>\n             <div class=\"panel-body dffff\">\n            <div class=\"col-sm-5 imgs\">\n                <img src=\"{{item.images1}}\" alt=\"about us\" style=\"width:100%; height: 180px;\"> \n                \n              </div>  \n            <div class=\"col-sm-5 mains\">\n           \n              <p class=\"normaldetails\"> contact number : <b class=\"sides\">  {{item.mobileno}} </b></p>\n              <p class=\"normaldetails\"> whatsApp number : <b class=\"sides\">  {{item.whatsupno}} </b></p>\n              <p class=\"normaldetails\"> area: <b class=\"sides\">  {{item.area}} </b></p>\n              <p class=\"normaldetails\"> landmark: <b class=\"sides\">  {{item.landmark}} </b></p>\n               <p class=\"normaldetails\"> pincode : <b class=\"sides\">  {{item.pincode}} </b></p>\n              <p class=\"\"> more details..</p>\n\n            </div>\n            </div>\n          </div>\n        </a>\n    </div>\n \n</div>\n<div class=\"col-sm-12\" *ngFor=\"let one of animalsAddsFivesLoc\">\n    <a href=\"{{one.addsFiveLinkLoc}}\">\n    <img src=\"{{one.addsFiveImgLoc}}\" alt=\"about us\" style=\"width:100%;height:200px\">\n    </a>\n     \n    </div>\n</div>\n</div>\n</div>\n\n<div class=\"col-sm-12 footerback\">\n    <div class=\"col-sm-8 col-sm-offset-3\">\n      <div class=\"col-sm-8\">\n        <!-- <div class=\"col-sm-4\">\n          <a [routerLink]= \"['/tutions']\" routerLinkActive=\"active-link\" class=\"homes\">\n    \n             <h3> About Us </h3>\n         </a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;\n        </div> -->\n        <div class=\"col-sm-4\">\n         <a [routerLink]= \"['/advertise']\" routerLinkActive=\"active-link\" class=\"\">\n     \n          <h3>  Advertises  </h3>\n        </a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;\n      </div>\n      <div class=\"col-sm-4\">\n        <a [routerLink]= \"['/tutions']\" routerLinkActive=\"active-link\" class=\"homes\">\n     \n          <h3> Feedback   </h3>\n      </a>\n    </div>\n      </div>\n     <div class=\"col-sm-8 catas\">\n        <a [routerLink]= \"['/animals']\" routerLinkActive=\"active-link\" class=\"homes\"> \n                Animals  \n        </a>\n         /  \n         <a [routerLink]= \"['/babycare']\" routerLinkActive=\"active-link\" class=\"homes\">\n    \n             Baby Care Centers  \n        </a>\n         /  \n         <a [routerLink]= \"['/beautyparlour']\" routerLinkActive=\"active-link\" class=\"homes\"> \n    \n          Beauty Parlour   </a>\n          /\n         \n         <a [routerLink]= \"['/dance']\" routerLinkActive=\"active-link\" class=\"homes\"> \n    \n            Dance   </a>\n             / \n             <a [routerLink]= \"['/drivingSchools']\" routerLinkActive=\"active-link\" class=\"homes\"> \n              Driving Schools  \n           </a> \n           / \n             \n           <a [routerLink]= \"['/fight']\" routerLinkActive=\"active-link\" class=\"homes\"> \n               Fights Institutions \n           </a> \n           /  <a [routerLink]= \"['/fireWorks']\" routerLinkActive=\"active-link\" class=\"homes\"> \n              Fire Works  \n           </a> /  <a [routerLink]= \"['/halls']\" routerLinkActive=\"active-link\" class=\"homes\"> \n      \n               Function Halls \n          </a> /  <a [routerLink]= \"['/furnitures']\" routerLinkActive=\"active-link\" class=\"homes\"> \n      \n               Furnitures  \n          </a> / <a [routerLink]= \"['/gym']\" routerLinkActive=\"active-link\" class=\"homes\"> \n      \n             GYM  \n          </a> / <a [routerLink]= \"['/hostelcatageries']\" routerLinkActive=\"active-link\" class=\"homes\">\n              Hostels \n          </a> / <a [routerLink]= \"['/indoorgames']\" routerLinkActive=\"active-link\" class=\"homes\"> \n             Indoor Games  \n           </a> /  <a [routerLink]= \"['/marriagebureaus']\" routerLinkActive=\"active-link\" class=\"homes\"> \n        \n              Marriage Bureaus  \n          </a> /  <a [routerLink]= \"['/music']\" routerLinkActive=\"active-link\" class=\"homes\">\n    \n               Music  Institutions\n          </a> /  <a [routerLink]= \"['/outdoorgames']\" routerLinkActive=\"active-link\" class=\"homes\"> \n              Outdoor Games  \n           </a> /  <a [routerLink]= \"['/pa']\" routerLinkActive=\"active-link\" class=\"homes\"> \n      \n             Plants  \n          </a> /  <a [routerLink]= \"['/packersMovers']\" routerLinkActive=\"active-link\" class=\"homes\"> \n               Packers and Movers  \n           </a> /  <a [routerLink]= \"['/professionalInstitutionsCatageries']\" routerLinkActive=\"active-link\" class=\"homes\"> \n               Professional Insitutions  \n           </a> /      <a [routerLink]= \"['/realestate']\" routerLinkActive=\"active-link\" class=\"homes\">\n      \n             Real Estate \n          </a> /   <a [routerLink]= \"['/restaurants']\" routerLinkActive=\"active-link\" class=\"homes\"> \n    \n              Restaurants  \n          </a> /   <a [routerLink]= \"['/swimming']\" routerLinkActive=\"active-link\" class=\"homes\"> \n    \n       Swimming Pools  \n          </a> / <a [routerLink]= \"['/tutions']\" routerLinkActive=\"active-link\" class=\"homes\">\n    \n               Tutions  Centers  \n          </a>\n        \n     </div>\n     <div class=\"col-sm-8 locationcs\">\n       <a class=\"homes\"> warangal </a>/<a class=\"homes\">  karimnagar</a> /<a class=\"homes\">  kammam </a>\n     </div>\n\n     <div class=\"col-sm-6\">\n      \n      <h3> Keep in touch </h3>\n      <div class=\"col-sm-2\">\n        <a href=\"https://www.facebook.com/abhinesh.ghugloth\" target=\"_blank\">\n    <i class=\"fa fa-facebook-square fb lsc\" aria-hidden=\"true\"></i>\n    </a>\n</div>\n\n<div class=\"col-sm-2\"> \n<a href=\"https://twitter.com/yestelanganase1\" target=\"_blank\">\n<i class=\"fa fa-twitter twiter lsc\" aria-hidden=\"true\"></i>  </a>\n</div>\n<div class=\"col-sm-2\"> \n<a href=\"https://www.instagram.com/yestelanganaservices/\" target=\"_blank\">\n<i class=\"fa fa-instagram inst lsc\" aria-hidden=\"true\"></i></a>\n</div>\n\n\n      </div>\n<div class=\"col-sm-8 \">\n<h6>Copyrights @ 2019-20 . All Rights Reserved . <b> Yes Telangan Services  </b> </h6>\n</div>\n    </div>\n  </div>\n  "

/***/ }),

/***/ "./src/app/animals/animals/animals.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AnimalsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__animals_service__ = __webpack_require__("./src/app/animals/animals.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AnimalsComponent = (function () {
    function AnimalsComponent(router, route, animals) {
        this.router = router;
        this.route = route;
        this.animals = animals;
    }
    AnimalsComponent.prototype.ngOnInit = function () {
        this.getlocations();
        this.animalsuserscounttot();
        this.animalsuserscountLocations();
    };
    // locations*****************
    AnimalsComponent.prototype.getlocations = function () {
        var _this = this;
        this.animals.locationapi().subscribe(function (res) {
            _this.locationsget = res;
            var id = _this.locationsget[0];
            _this.location(id);
        });
    };
    AnimalsComponent.prototype.location = function (get) {
        this.locationName = get.locations;
        this.its = get._id;
        this.allAreas();
        this.animalssareasAddsOneLoc();
        this.animalssareasAddsTwoLoc();
        this.animalssareasAddsThreeLoc();
        this.animalssareasAddsFourLoc();
        this.animalssareasAddsFiveLoc();
    };
    AnimalsComponent.prototype.allAreas = function () {
        var _this = this;
        this.animals.areaapi(this.its).subscribe(function (res) {
            _this.areasAll = res;
            var id = _this.areasAll[0];
            _this.selectedAre(id);
        });
    };
    AnimalsComponent.prototype.selectedAre = function (result) {
        this.selectAreaId = result._id;
        this.selectArea = result.area;
        this.animalsuserscount();
        this.animalsusers();
        this.animalssareascount();
        this.animalssareasAddsOnea();
        this.animalssareasAddsTwoa();
        this.animalssareasAddsThreea();
        this.animalssareasAddsFoura();
    };
    AnimalsComponent.prototype.animalsusers = function () {
        var _this = this;
        var data = {};
        if (this.searchString) {
            data.search = this.searchString;
        }
        this.animals.animalsusers(this.selectAreaId, data).subscribe(function (res) {
            _this.animalsuser = res;
        });
    };
    AnimalsComponent.prototype.searchFilter = function () {
        this.animalsusers();
    };
    // ***************************************************************
    AnimalsComponent.prototype.animalsuserscount = function () {
        var _this = this;
        this.animals.animalsuserscounts(this.selectAreaId).subscribe(function (res) {
            _this.animalsusercount = res;
        });
    };
    AnimalsComponent.prototype.animalsuserscountLocations = function () {
        var _this = this;
        this.animals.animalsusersForLocations().subscribe(function (res) {
            _this.animalsusercountLocations = res;
        });
    };
    // ***************************************************************
    AnimalsComponent.prototype.animalsuserscounttot = function () {
        var _this = this;
        this.animals.animalsuserscountstot().subscribe(function (res) {
            _this.animalsusercounttot = res;
        });
    };
    AnimalsComponent.prototype.animalssareascount = function () {
        var _this = this;
        this.animals.animalsuserscountsarea(this.selectAreaId).subscribe(function (res) {
            _this.popularareas = res;
        });
    };
    AnimalsComponent.prototype.animalssareasAddsOnea = function () {
        var _this = this;
        this.animals.animalsAddsOnea(this.selectAreaId).subscribe(function (res) {
            _this.animalsAddsOnes = res;
        });
    };
    AnimalsComponent.prototype.animalssareasAddsTwoa = function () {
        var _this = this;
        this.animals.animalsAddsTwoa(this.selectAreaId).subscribe(function (res) {
            _this.animalsAddsTwoa = res;
        });
    };
    AnimalsComponent.prototype.animalssareasAddsThreea = function () {
        var _this = this;
        this.animals.animalsAddsThreea(this.selectAreaId).subscribe(function (res) {
            _this.animalsAddsThreea = res;
        });
    };
    AnimalsComponent.prototype.animalssareasAddsFoura = function () {
        var _this = this;
        this.animals.animalsAddsFoura(this.selectAreaId).subscribe(function (res) {
            _this.animalsAddsFours = res;
        });
    };
    AnimalsComponent.prototype.animalssareasAddsOneLoc = function () {
        var _this = this;
        this.animals.animalsAddsOnel(this.its).subscribe(function (res) {
            _this.animalsAddsOnesl = res;
        });
    };
    AnimalsComponent.prototype.animalssareasAddsTwoLoc = function () {
        var _this = this;
        this.animals.animalsAddsTwol(this.its).subscribe(function (res) {
            _this.animalsAddsTwol = res;
        });
    };
    AnimalsComponent.prototype.animalssareasAddsThreeLoc = function () {
        var _this = this;
        this.animals.animalsAddsThreel(this.its).subscribe(function (res) {
            _this.animalsAddsThreel = res;
        });
    };
    AnimalsComponent.prototype.animalssareasAddsFourLoc = function () {
        var _this = this;
        this.animals.animalsAddsFour(this.its).subscribe(function (res) {
            _this.animalsAddsFourl = res;
        });
    };
    AnimalsComponent.prototype.animalssareasAddsFiveLoc = function () {
        var _this = this;
        this.animals.animalsAddsFivel(this.its).subscribe(function (res) {
            _this.animalsAddsFivesLoc = res;
        });
    };
    AnimalsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-animals',
            template: __webpack_require__("./src/app/animals/animals/animals.component.html"),
            styles: [__webpack_require__("./src/app/animals/animals/animals.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_2__animals_service__["a" /* AnimalsService */]])
    ], AnimalsComponent);
    return AnimalsComponent;
}());



/***/ }),

/***/ "./src/app/animals/animals/animals.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnimalsModule", function() { return AnimalsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_shared_module__ = __webpack_require__("./src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__animals_component__ = __webpack_require__("./src/app/animals/animals/animals.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__animalsdetails_animalsdetails_module__ = __webpack_require__("./src/app/animals/animalsdetails/animalsdetails.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__animals_service__ = __webpack_require__("./src/app/animals/animals.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var routes = [
    {
        path: '', component: __WEBPACK_IMPORTED_MODULE_5__animals_component__["a" /* AnimalsComponent */], children: [
            { path: 'animalsdetails/:_id/:name', loadChildren: 'app/animals/animalsdetails/animalsdetails.module#AnimalsdetailsModule' },
        ]
    }
];
var AnimalsModule = (function () {
    function AnimalsModule() {
    }
    AnimalsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["h" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* RouterModule */].forChild(routes),
                __WEBPACK_IMPORTED_MODULE_4__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_6__animalsdetails_animalsdetails_module__["AnimalsdetailsModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__animals_component__["a" /* AnimalsComponent */],
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_7__animals_service__["a" /* AnimalsService */]]
        })
    ], AnimalsModule);
    return AnimalsModule;
}());



/***/ })

});
//# sourceMappingURL=animals.module.chunk.js.map
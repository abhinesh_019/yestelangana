webpackJsonp(["main-module.module"],{

/***/ "./src/app/login/main-module/main-module.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/login/main-module/main-module.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n\n   \n<router-outlet></router-outlet>\n</div>\n"

/***/ }),

/***/ "./src/app/login/main-module/main-module.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainModuleComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MainModuleComponent = (function () {
    function MainModuleComponent() {
    }
    MainModuleComponent.prototype.ngOnInit = function () {
    };
    MainModuleComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-main-module',
            template: __webpack_require__("./src/app/login/main-module/main-module.component.html"),
            styles: [__webpack_require__("./src/app/login/main-module/main-module.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MainModuleComponent);
    return MainModuleComponent;
}());



/***/ }),

/***/ "./src/app/login/main-module/main-module.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainModuleModule", function() { return MainModuleModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__main_module_component__ = __webpack_require__("./src/app/login/main-module/main-module.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__login_login_module__ = __webpack_require__("./src/app/login/login/login.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__signup_signup_module__ = __webpack_require__("./src/app/login/signup/signup.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__shared_materials_module__ = __webpack_require__("./src/app/login/shared/materials.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__golbal_service__ = __webpack_require__("./src/app/login/golbal.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__forgetpassword_forgetpassword_module__ = __webpack_require__("./src/app/login/forgetpassword/forgetpassword.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__resetpassword_resetpassword_module__ = __webpack_require__("./src/app/login/resetpassword/resetpassword.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var routes = [
    {
        path: '', component: __WEBPACK_IMPORTED_MODULE_4__main_module_component__["a" /* MainModuleComponent */], children: [{ path: 'login', redirectTo: 'login', pathMatch: "full" },
            { path: 'login', loadChildren: 'app/login/login/login.module#LoginModule' },
            { path: 'signup', loadChildren: 'app/login/signup/signup.module#SignupModule' },
            { path: 'forgetpassword', loadChildren: 'app/login/forgetpassword/forgetpassword.module#ForgetpasswordModule' },
            { path: 'reset-password/:token', loadChildren: 'app/login/resetpassword/resetpassword.module#ResetpasswordModule' },
        ]
    }
];
var MainModuleModule = (function () {
    function MainModuleModule() {
    }
    MainModuleModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_forms__["h" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* RouterModule */].forChild(routes),
                __WEBPACK_IMPORTED_MODULE_6__login_login_module__["LoginModule"],
                __WEBPACK_IMPORTED_MODULE_7__signup_signup_module__["SignupModule"],
                __WEBPACK_IMPORTED_MODULE_8__shared_materials_module__["a" /* MaterialsModule */],
                __WEBPACK_IMPORTED_MODULE_10__forgetpassword_forgetpassword_module__["ForgetpasswordModule"],
                __WEBPACK_IMPORTED_MODULE_11__resetpassword_resetpassword_module__["ResetpasswordModule"],
                __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["b" /* HttpClientModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__main_module_component__["a" /* MainModuleComponent */],
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_9__golbal_service__["a" /* GolbalService */]],
        })
    ], MainModuleModule);
    return MainModuleModule;
}());



/***/ })

});
//# sourceMappingURL=main-module.module.chunk.js.map
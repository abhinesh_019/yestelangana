webpackJsonp(["fight.module"],{

/***/ "./src/app/fight/fight/fight.component.css":
/***/ (function(module, exports) {

module.exports = ".catstyle{text-align: center;\n    text-transform: capitalize;\n    font-weight: bold;\n    text-shadow: 2px 2px #000000c7;\n    font-size: 50px;}"

/***/ }),

/***/ "./src/app/fight/fight/fight.component.html":
/***/ (function(module, exports) {

module.exports = " <!-- ******************* nav bar ************************** -->\n\n <nav class=\"navbar navbar-default bg fixed\">\n        <div class=\"container-fluid\">\n          <div class=\"navbar-header\">\n            <a class=\"navbar-brand yesT\">  <img src=\"assets/images/logo.png\"    style=\"width:25%\">    </a>\n          </div>\n          \n             \n      <div class=\"col-sm-3 profilesv\">\n         \n        \n         <div class=\"col-sm-12\">\n                 <a [routerLink]= \"['/login/login']\" class=\"\">  Login  </a> /   <a [routerLink]= \"['/login/signup']\" class=\"\">  Sign Up  </a> \n         </div>\n        \n      </div>\n      \n        \n        </div>\n      </nav>\n      \n<div class=\"col-md-12 col-xs-12\">\n  <div class=\"col-sm-12\" *ngFor=\"let item of catList\">\n      <a [routerLink]=\"['/fight/fightusers',item._id,item.fightCategories]\">\n\n      <div class=\"panel panel-primary dff boxshaws\">\n           <div class=\"panel-body dffff\">\n          <div class=\"col-sm-6 col-sm-offset-3 imgs\">\n              <h2 class=\"catstyle\"> {{item.fightCategories}}</h2>\n              <img src=\"{{item.images}}\" alt=\"about us\" style=\"width:100%; height: 260px;\"> \n          </div>  \n        \n          </div>\n        </div>\n</a>\n      </div>\n\n</div> \n"

/***/ }),

/***/ "./src/app/fight/fight/fight.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FightComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__fight_service__ = __webpack_require__("./src/app/fight/fight/fight.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FightComponent = (function () {
    function FightComponent(fights) {
        this.fights = fights;
    }
    FightComponent.prototype.ngOnInit = function () {
        this.getcatagories();
    };
    FightComponent.prototype.getcatagories = function () {
        var _this = this;
        this.fights.catapigetdetails().subscribe(function (res) {
            _this.catList = res;
            console.log(_this.catList);
        });
    };
    FightComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-fight',
            template: __webpack_require__("./src/app/fight/fight/fight.component.html"),
            styles: [__webpack_require__("./src/app/fight/fight/fight.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__fight_service__["a" /* FightService */]])
    ], FightComponent);
    return FightComponent;
}());



/***/ }),

/***/ "./src/app/fight/fight/fight.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FightModule", function() { return FightModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_shared_module__ = __webpack_require__("./src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__fight_component__ = __webpack_require__("./src/app/fight/fight/fight.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__fight_service__ = __webpack_require__("./src/app/fight/fight/fight.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__fightusers_fightusers_module__ = __webpack_require__("./src/app/fight/fightusers/fightusers.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__fight_details_fight_deatils_module__ = __webpack_require__("./src/app/fight/fight-details/fight-deatils.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var routes = [
    {
        path: '', component: __WEBPACK_IMPORTED_MODULE_5__fight_component__["a" /* FightComponent */], children: [
            { path: 'fightusers/:_id/:name', loadChildren: 'app/fight/fightusers/fightusers.module#FightusersModule' },
            { path: 'fightsDetails/:_id/:name', loadChildren: 'app/fight/fight-details/fight-deatils.module#FightDeatilsModule' },
        ]
    }
];
var FightModule = (function () {
    function FightModule() {
    }
    FightModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["h" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* RouterModule */].forChild(routes),
                __WEBPACK_IMPORTED_MODULE_4__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_7__fightusers_fightusers_module__["FightusersModule"],
                __WEBPACK_IMPORTED_MODULE_8__fight_details_fight_deatils_module__["FightDeatilsModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__fight_component__["a" /* FightComponent */],
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_6__fight_service__["a" /* FightService */]]
        })
    ], FightModule);
    return FightModule;
}());



/***/ }),

/***/ "./src/app/fight/fight/fight.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FightService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment_prod__ = __webpack_require__("./src/environments/environment.prod.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FightService = (function () {
    function FightService(http) {
        this.http = http;
    }
    FightService.prototype.catapigetdetails = function () {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_3__environments_environment_prod__["a" /* environment */].apiUrl + "/fightsCategories");
    };
    FightService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], FightService);
    return FightService;
}());



/***/ })

});
//# sourceMappingURL=fight.module.chunk.js.map
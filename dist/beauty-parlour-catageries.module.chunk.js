webpackJsonp(["beauty-parlour-catageries.module"],{

/***/ "./src/app/beautyparlour/beauty-parlour-catageries/beauty-parlour-catageries.component.css":
/***/ (function(module, exports) {

module.exports = ".catstyle{text-align: center;\n    text-transform: capitalize;\n    font-weight: bold;\n    text-shadow: 2px 2px #000000c7;\n    font-size: 50px;}"

/***/ }),

/***/ "./src/app/beautyparlour/beauty-parlour-catageries/beauty-parlour-catageries.component.html":
/***/ (function(module, exports) {

module.exports = " <!-- ******************* nav bar ************************** -->\n\n <nav class=\"navbar navbar-default bg fixed\">\n        <div class=\"container-fluid\">\n          <div class=\"navbar-header\">\n            <a class=\"navbar-brand yesT\">  <img src=\"assets/images/logo.png\"    style=\"width:25%\">    </a>\n          </div>\n          \n             \n      <div class=\"col-sm-3 profilesv\">\n         \n        \n         <div class=\"col-sm-12\">\n                 <a [routerLink]= \"['/login/login']\" class=\"\">  Login  </a> /   <a [routerLink]= \"['/login/signup']\" class=\"\">  Sign Up  </a> \n         </div>\n        \n      </div>\n      \n        \n        </div>\n      </nav>\n     \n<div class=\"col-md-12 col-xs-12\">\n  <div class=\"col-sm-12\" *ngFor=\"let item of catList\">\n      <a [routerLink]=\"['/beautyparlour/parlourselect',item._id,item.categories]\">\n\n      <div class=\"panel panel-primary dff boxshaws\">\n           <div class=\"panel-body dffff\">\n          <div class=\"col-sm-6 col-sm-offset-3 imgs\">\n              <h2 class=\"catstyle\"> {{item.categories}}</h2>\n              <img src=\"{{item.images1}}\" alt=\"about us\" style=\"width:100%; height: 260px;\"> \n          </div>  \n        \n          </div>\n        </div>\n</a>\n      </div>\n\n</div> \n"

/***/ }),

/***/ "./src/app/beautyparlour/beauty-parlour-catageries/beauty-parlour-catageries.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BeautyParlourCatageriesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__beautyparlour_service__ = __webpack_require__("./src/app/beautyparlour/beautyparlour.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BeautyParlourCatageriesComponent = (function () {
    function BeautyParlourCatageriesComponent(parlour) {
        this.parlour = parlour;
    }
    BeautyParlourCatageriesComponent.prototype.ngOnInit = function () {
        this.getcatagories();
    };
    BeautyParlourCatageriesComponent.prototype.getcatagories = function () {
        var _this = this;
        this.parlour.catapigetdetails().subscribe(function (res) {
            _this.catList = res;
            console.log(_this.catList);
        });
    };
    BeautyParlourCatageriesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-beauty-parlour-catageries',
            template: __webpack_require__("./src/app/beautyparlour/beauty-parlour-catageries/beauty-parlour-catageries.component.html"),
            styles: [__webpack_require__("./src/app/beautyparlour/beauty-parlour-catageries/beauty-parlour-catageries.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__beautyparlour_service__["a" /* BeautyparlourService */]])
    ], BeautyParlourCatageriesComponent);
    return BeautyParlourCatageriesComponent;
}());



/***/ }),

/***/ "./src/app/beautyparlour/beauty-parlour-catageries/beauty-parlour-catageries.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BeautyParlourCatageriesModule", function() { return BeautyParlourCatageriesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_shared_module__ = __webpack_require__("./src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__parlourdetails_parlourdetails_module__ = __webpack_require__("./src/app/beautyparlour/parlourdetails/parlourdetails.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__beautyparlour_service__ = __webpack_require__("./src/app/beautyparlour/beautyparlour.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__beauty_parlour_catageries_component__ = __webpack_require__("./src/app/beautyparlour/beauty-parlour-catageries/beauty-parlour-catageries.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__beautyparlour_beautyparlour_module__ = __webpack_require__("./src/app/beautyparlour/beautyparlour/beautyparlour.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var routes = [
    {
        path: '', component: __WEBPACK_IMPORTED_MODULE_7__beauty_parlour_catageries_component__["a" /* BeautyParlourCatageriesComponent */], children: [
            { path: 'parlourdetails/:_id/:name', loadChildren: 'app/beautyparlour/parlourdetails/parlourdetails.module#ParlourdetailsModule' },
            { path: 'parlourselect/:_id/:name', loadChildren: 'app/beautyparlour/beautyparlour/beautyparlour.module#BeautyparlourModule' },
        ]
    }
];
var BeautyParlourCatageriesModule = (function () {
    function BeautyParlourCatageriesModule() {
    }
    BeautyParlourCatageriesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["h" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* RouterModule */].forChild(routes),
                __WEBPACK_IMPORTED_MODULE_4__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_8__beautyparlour_beautyparlour_module__["BeautyparlourModule"],
                __WEBPACK_IMPORTED_MODULE_5__parlourdetails_parlourdetails_module__["ParlourdetailsModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__beauty_parlour_catageries_component__["a" /* BeautyParlourCatageriesComponent */],
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_6__beautyparlour_service__["a" /* BeautyparlourService */]]
        })
    ], BeautyParlourCatageriesModule);
    return BeautyParlourCatageriesModule;
}());



/***/ })

});
//# sourceMappingURL=beauty-parlour-catageries.module.chunk.js.map